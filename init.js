function init (models) {
  models.Rol.findOrCreate({ where: { tipoRol: 'cliente' }, defaults: { nombre: 'Cliente' } })
    .then(([rol, created]) => {
      if (created) { console.log(rol.get({ plain: true })) }
      models.Rol.findOrCreate({ where: { tipoRol: 'jefe' }, defaults: { nombre: 'Jefe' } })
        .then(([rol, created]) => {
          if (created) { console.log(rol.get({ plain: true })) }
          models.Rol.findOrCreate({ where: { tipoRol: 'tecnico' }, defaults: { nombre: 'Tecnico' } })
            .then(([rol, created]) => {
              if (created) { console.log(rol.get({ plain: true })) }
            }).catch(err => { console.log(err) })
        }).catch(err => { console.log(err) })
    }).catch(err => { console.log(err) })

  models.TipoServicio
    .findOrCreate({ where: { nombre: 'reparacion' } })
    .then(([tipoServicio, created]) => {
      if (created) {
        console.log(tipoServicio.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.TipoServicio
    .findOrCreate({ where: { nombre: 'mantenimiento' } })
    .then(([tipoServicio, created]) => {
      if (created) {
        console.log(tipoServicio.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.TipoServicio
    .findOrCreate({ where: { nombre: 'mantenimiento y reparacion' } })
    .then(([tipoServicio, created]) => {
      if (created) {
        console.log(tipoServicio.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.Permiso
    .findOrCreate({ where: { nombre: 'crearEjemplo' } })
    .then(([permiso, created]) => {
      if (created) {
        console.log(permiso.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.Permiso
    .findOrCreate({ where: { nombre: 'editarEjemplo' } })
    .then(([permiso, created]) => {
      if (created) {
        console.log(permiso.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.Permiso
    .findOrCreate({ where: { nombre: 'borrarEjemplo' } })
    .then(([permiso, created]) => {
      if (created) {
        console.log(permiso.get({
          plain: true
        }))
      }
    })
    .catch(err => {
      console.log(err)
    })
  models.Empresa
    .findOrCreate({ where: { id: 1 } })
}
module.exports = {
  init
}
