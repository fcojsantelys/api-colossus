'use strict'

const secret = require('../config/env').secret
const ejwt = require('express-jwt')
const Usuario = require('../models').Usuario
const Rol = require('../models').Rol

function unauthorizedHandling (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.sendStatus(401)
  }
}

const expressjwt = ejwt({ secret: secret })

function checkUser (req, res, next) {
  const correo = req.user.correo
  if (correo) { // if the token has the attribute correo
    Usuario.findOne(
      { where: { correo },
        include: [Rol]
      }
    )
      .then(usuario => {
        if (usuario) {
          req.currentUser = usuario // set currentUser
          next()
        } else {
          res.sendStatus(401) // usuario not found
        }
      })
      .catch(err => {
        console.error(err)
      })
  } else {
    res.sendStatus(401) // not the right format on the jwt
  }
}

module.exports = {
  unauthorizedHandling,
  expressjwt,
  checkUser
}
