# API COLOSSUS
Conjunto de definiciones y protocolos que se utiliza para desarrollar e integrar el software de Colossus; permite que la comunicacion entre los distintos componentes.
Desarrollado por el Equipo Siri para la catedra de Sistemas de Informacion (10524) en la carrera Ingenieria en Informatica, UCLA
lapso 2019-1

## Table de contents

+ [Getting started](#Getting-Started)
      + [Live demo](#Live-demo)
      + [Local usage](#Using-locally)
            + [Installation](#Installation)
+ [Endpoints](#Endpoints)
      + [Bitacora](#bitacora)
            + [Get by Solicitud](#httplocalhost8900solicitudServicioIdBitacora-Get)
            + [Get ALL](#httplocalhost8900solicitudServicioBitacora-Get)
      + [Precio](#precio)
      + [Condicion](#condicion)
            + [Create condicion](#httplocalhost8900condicion-POST)
            + [GetOne condicion](#httplocalhost8900condicionid-GET)
            + [GetAll condicion](#httplocalhost8900condicion-GET)
            + [Update condicion](#httplocalhost8900condicionid-PUT)
            + [Delete condicion](#httplocalhost8900condicionid-DELETE)
+ [Authors](#Authors)

""""""""""""""""""""""""""""""""""""""""""""""""""

## Getting Started

Estas instrucciones te ayudaran a configurar el entorno para usar la API

### Live demo

### Using locally

#### Installation

""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""

## Endpoints

### bitacora

#### http://localhost:8900/solicitudServicio/:id/bitacora (GET)

description: get all Bitacora by SolicitudServicio, order by[fechaMovimiento, asc]

**REQUEST**  
Headers: `x-access-token`

**RESPONSE**
```json
{
    "status": true,
    "message": {},
    "data": [
        {
            "id": 44,
            "fechaMovimiento": "12-10-2019 12:05:38",
            "descripcion": "Solicitud Creada",
            "solicitudServicio": {
                "id": 47
            }
        },
        {
            "id": 49,
            "fechaMovimiento": "12-10-2019 12:19:14",
            "descripcion": "Solicitud Aceptada",
            "solicitudServicio": {
                "id": 47
            }
        },
        {
            "id": 50,
            "fechaMovimiento": "12-10-2019 12:19:14",
            "descripcion": "En espera de revision del equipo",
            "solicitudServicio": {
                "id": 47
            }
        },
        {
            "id": 54,
            "fechaMovimiento": "12-10-2019 12:38:41",
            "descripcion": "Equipo es reparable, pendiente presupuesto",
            "solicitudServicio": {
                "id": 47
            }
        },
        {
            "id": 58,
            "fechaMovimiento": "12-10-2019 12:53:18",
            "descripcion": "Presupuesto Generado",
            "solicitudServicio": {
                "id": 47
            }
        },
        {
            "id": 59,
            "fechaMovimiento": "12-10-2019 01:01:32",
            "descripcion": "Presupuesto Aceptado",
            "solicitudServicio": {
                "id": 47
            }
        }
    ]
}
```

#### http://localhost:8900/solicitudServicio/bitacora (GET)

description: get all Bitacora, orden by [[id, asc],[fechaMovimiento, asc]]

**REQUEST**  
Headers: `x-access-token`

**RESPONSE**
```json
{
    "status": true,
    "message": {},
    "data": [
        {
            "id": 42,
            "fechaMovimiento": "12-10-2019 12:05:22",
            "descripcion": "Solicitud Creada",
            "solicitudServicio": {
                "id": 45
            }
        },
        {
            "id": 45,
            "fechaMovimiento": "12-10-2019 12:06:34",
            "descripcion": "Solicitud Aceptada",
            "solicitudServicio": {
                "id": 45
            }
        },
        {
            "id": 46,
            "fechaMovimiento": "12-10-2019 12:06:34",
            "descripcion": "En espera de revision del equipo",
            "solicitudServicio": {
                "id": 45
            }
        },
        {
            "id": 43,
            "fechaMovimiento": "12-10-2019 12:05:32",
            "descripcion": "Solicitud Creada",
            "solicitudServicio": {
                "id": 46
            }
        },
        {
            "id": 47,
            "fechaMovimiento": "12-10-2019 12:06:28",
            "descripcion": "Solicitud Aceptada",
            "solicitudServicio": {
                "id": 46
            }
        },
        {
            "id": 48,
            "fechaMovimiento": "12-10-2019 12:06:34",
            "descripcion": "En espera de revision del equipo",
            "solicitudServicio": {
                "id": 46
            }
        }
    ]
}
```
-----------

### precio


-----------

### condicion

#### http://localhost:8900/condicion (POST)

Create a condicion.

**REQUEST**  
Headers: `x-access-token`
```json
{
	"descripcion": "descripcion de la condicion"
}
```

**RESPONSE**
```json
{
    "status": true,
    "message": {
        "code": "I-condicion-01",
        "text": "condicion creada satisfactoriamente"
    }
}
```

#### http://localhost:8900/condicion/:id (GET)

Get One condicion

**REQUEST**  
Headers: `x-access-token`

**RESPONSE**
```json
{
    "status": true,
    "message": {},
    "data": {
        "id": 2,
        "descripcion": "descripcion de la condicion"
    }
}
```

#### http://localhost:8900/condicion (GET)

Get All condicion

**REQUEST**  
Headers: `x-access-token`

**RESPONSE**
```json
{
    "status": true,
    "message": {},
    "data": [
        {
            "id": 1,
            "descripcion": "modificacion nueva2"
        },
        {
            "id": 3,
            "descripcion": "descripcion cosdfsdf2"
        }
    ]
}
```

#### http://localhost:8900/condicion/:id (PUT)

Update a condicion.

**REQUEST**  
Headers: `x-access-token`
```json
{
	"descripcion": "modificacion de la descripcion"
}
```

**RESPONSE**
```json
{
    "status": true,
    "message": {
        "code": "I-condicion-02",
        "text": "condicion actualizada satisfactoriamente"
    }
}
```

#### http://localhost:8900/condicion/:id (DELETE)

Delete a condicion.

**REQUEST**  
Headers: `x-access-token`

**RESPONSE**
```json
{
    "status": true,
    "message": {
        "code": "I-condicion-02",
        "text": "condicion borrada satisfactoriamente"
    }
}
```

### asdasds

-----------

""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""

## Authors


""""""""""""""""""""""""""""""""""""""""""""""""""

