const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const formidableMiddleware = require('express-formidable')
const staticRoot = require('./config/path.json').staticRoot
const path = require('path')
const app = express()
const unath = require('./middlewares/auth').unauthorizedHandling
const ejwt = require('./middlewares/auth').expressjwt
const checkUser = require('./middlewares/auth').checkUser

app.use(formidableMiddleware(
  {
    encoding: 'utf-8',
    multiples: true, // req.files to be arrays of files
    keepExtensions: true // req.files to be arrays of files
  }))

// app.use(logRequest)
app.use(cors())
app.use(morgan('short'))
app.get('/', (req, res) => {
  res.status(200).json({ status: true, message: 'Titan of endurance, strength and astronomy' })
})

app.use(`/${staticRoot}`, express.static(path.join(__dirname, staticRoot)))
require('./routes')(app)

app.get('/protected',
  ejwt,
  checkUser,
  function (req, res) {
    console.log(req.user)
    res.status(200).json(req.user.correo)
  })

app.use(unath)

app.use((req, res, next) => {
  res.sendStatus(404)
})

module.exports = app
