const sDuracionGarantia = require('../services/duracionGarantia')
const responses = require('../utils/responses')

async function createDuracionGarantia (req, res) {
  try {
    await sDuracionGarantia.createDuracionGarantia(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I-duracionGarantia-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllDuracionGarantia (req, res) {
  try {
    const duracionGarantia = await sDuracionGarantia.getAllDuracionGarantia()
    responses.makeResponseOk(res, { duracionGarantia }, 'duracionGarantia/getAllDuracionGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getDuracionGarantia (req, res) {
  try {
    const id = req.params.id
    const duracionGarantia = await sDuracionGarantia.getDuracionGarantia(id)
    responses.makeResponseOk(res, { duracionGarantia }, 'duracionGarantia/getDuracionGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateDuracionGarantia (req, res) {
  try {
    const duracionGarantia = await sDuracionGarantia.updateDuracionGarantia(req.params.id, req.fields)
    responses.makeResponseOk(res, { duracionGarantia }, 'duracionGarantia/getDuracionGarantia', 'I-duracionGarantia-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteDuracionGarantia (req, res) {
  try {
    await sDuracionGarantia.deleteDuracionGarantia(req.params.id)
    responses.makeResponseOkMessage(res, 'I-duracionGarantia-02')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createDuracionGarantia,
  getAllDuracionGarantia,
  getDuracionGarantia,
  updateDuracionGarantia,
  deleteDuracionGarantia }
