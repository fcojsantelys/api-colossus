'use strict'

const sActividad = require('../services/actividad')
const responses = require('../utils/responses')

async function createActividad (req, res) {
  try {
    await sActividad.createActividad(req.fields)
    responses.makeResponseOkMessage(res, 'I00601')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getActividad (req, res) {
  try {
    const id = req.params.id
    const actividad = await sActividad.getActividad(id)
    responses.makeResponseOk(res, { actividad }, 'actividad/getActividad')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllActividad (req, res) {
  try {
    const actividad = await sActividad.getAllActividad()
    responses.makeResponseOk(res, { actividad }, 'actividad/getAllActividad')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateActividad (req, res) {
  try {
    await sActividad.updateActividad(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I00602')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteActividad (req, res) {
  try {
    await sActividad.deleteActividad(req.params.id)
    responses.makeResponseOkMessage(res, 'I00603')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createActividad,
  getActividad,
  getAllActividad,
  updateActividad,
  deleteActividad
}
