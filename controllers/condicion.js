'use strict'
const sCondicion = require('../services/condicion')
const responses = require('../utils/responses')

async function createCondicion (req, res) {
  try {
    await sCondicion.createCondicion(req.fields)
    responses.makeResponseOkMessage(res, 'I-condicion-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getCondicion (req, res) {
  try {
    const id = req.params.id
    const condicion = await sCondicion.getCondicion(id)
    responses.makeResponseOk(res, { condicion }, 'condicion/getCondicion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllCondicion (req, res) {
  try {
    const condicion = await sCondicion.getAllCondicion()
    responses.makeResponseOk(res, { condicion }, 'condicion/getAllCondicion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCondicion (req, res) {
  try {
    await sCondicion.updateCondicion(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-condicion-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteCondicion (req, res) {
  try {
    await sCondicion.deleteCondicion(req.params.id)
    responses.makeResponseOkMessage(res, 'I-condicion-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}
module.exports = {
  createCondicion,
  getCondicion,
  getAllCondicion,
  updateCondicion,
  deleteCondicion
}
