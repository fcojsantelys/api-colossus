'use strict'
const sGarantia = require('../services/garantia')
const responses = require('../utils/responses')

async function createGarantia (req, res) {
  try {
    await sGarantia.createGarantia(req.fields)
    responses.makeResponseOkMessage(res, 'I-garantia-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getGarantia (req, res) {
  try {
    const id = req.params.id
    const garantia = await sGarantia.getGarantia(id)
    responses.makeResponseOk(res, { garantia }, 'garantia/getGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllGarantia (req, res) {
  try {
    const garantia = await sGarantia.getAllGarantia()
    responses.makeResponseOk(res, { garantia }, 'garantia/getAllGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = { createGarantia,
  getGarantia,
  getAllGarantia
}
