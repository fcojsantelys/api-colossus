const sModeloEquipo = require('../services/modeloEquipo')
const responses = require('../utils/responses')

async function createModeloEquipo (req, res) {
  try {
    await sModeloEquipo.createModeloEquipo(req.fields)
    responses.makeResponseOkMessage(res, 'I01101')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getModeloEquipo (req, res) {
  try {
    const modeloEquipo = await sModeloEquipo.getModeloEquipo(req.params.id)
    responses.makeResponseOk(res, { modeloEquipo }, 'modeloEquipo/getModeloEquipo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllModeloEquipo (req, res) {
  try {
    console.log(req.query)
    const modeloEquipo = await sModeloEquipo.getAllModeloEquipo(req.query)
    responses.makeResponseOk(res, { modeloEquipo }, 'modeloEquipo/getAllModeloEquipo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateModeloEquipo (req, res) {
  try {
    await sModeloEquipo.updateModeloEquipo(req.fields, req.params.id)
    responses.makeResponseOkMessage(res, 'I01102')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteModeloEquipo (req, res) {
  try {
    await sModeloEquipo.deleteModeloEquipo(req.params.id)
    responses.makeResponseOkMessage(res, 'I01103')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createModeloEquipo,
  getModeloEquipo,
  getAllModeloEquipo,
  updateModeloEquipo,
  deleteModeloEquipo
}
