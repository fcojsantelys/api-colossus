const sRol = require('../services/rol')
const responses = require('../utils/responses')

async function postRol (req, res) {
  try {
    await sRol.addRol(req.fields)
    responses.makeResponseOkMessage(res, 'I01701')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllRol (req, res) {
  try {
    const rol = await sRol.getAllRol(req.query)
    responses.makeResponseOk(res, { rol }, 'rol/getAllRol')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getRol (req, res) {
  try {
    const { id } = req.params
    const rol = await sRol.getRol(id)
    responses.makeResponseOk(res, { rol }, 'rol/getRol')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateRol (req, res) {
  try {
    const { id } = req.params
    await sRol.updateRol(id, req.fields)
    responses.makeResponseOkMessage(res, 'I01702')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function destroyRol (req, res) {
  try {
    const { id } = req.params
    await sRol.deleteRol(id)
    responses.makeResponseOkMessage(res, 'I01703')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPermisos (req, res) {
  try {
    const { id } = req.params
    const permiso = await sRol.getPermisos(id)
    responses.makeResponseOk(res, { permiso }, 'permiso/getAllPermisoIntra')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getUnaddedPermisos (req, res) {
  try {
    const { id } = req.params
    const permiso = await sRol.getUnaddedPermisos(id)
    responses.makeResponseOk(res, { permiso }, 'permiso/getAllPermiso')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function addPermisos (req, res) {
  try {
    await sRol.addPermisos(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I01704')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function removePermisos (req, res) {
  try {
    await sRol.removePermisos(req.params.id, req.params.permiso)
    responses.makeResponseOkMessage(res, 'I01705')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  postRol,
  getAllRol,
  getRol,
  updateRol,
  destroyRol,
  getPermisos,
  getUnaddedPermisos,
  addPermisos,
  removePermisos
}
