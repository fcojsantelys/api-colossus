'use strict'
const sRevision = require('../services/revision')
const responses = require('../utils/responses')

async function getRevision (req, res) {
  try {
    const id = req.params.id
    const revision = await sRevision.getRevision(id)
    responses.makeResponseOk(res, { revision }, 'revision/getRevision')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllRevision (req, res) {
  try {
    const revision = await sRevision.getAllRevision()
    responses.makeResponseOk(res, { revision }, 'revision/getAllRevision')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateRevision (req, res) {
  try {
    const revision = await sRevision.updateRevision(req.params.id, req.fields)
    responses.makeResponseOk(res, { revision }, 'revision/getRevision')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  getRevision,
  getAllRevision,
  updateRevision
}
