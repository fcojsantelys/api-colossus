const sMotivoRechazo = require('../services/motivoRechazo')
const responses = require('../utils/responses')

async function createMotivoRechazo (req, res) {
  try {
    await sMotivoRechazo.createMotivoRechazo(req.fields)
    responses.makeResponseOkMessage(res, 'I-motivoRechazo-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllMotivoRechazo (req, res) {
  try {
    const motivoRechazo = await sMotivoRechazo.getAllMotivoRechazo()
    console.log(motivoRechazo)
    responses.makeResponseOk(res, { motivoRechazo }, 'motivoRechazo/getAllMotivoRechazo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getOneMotivoRechazo (req, res) {
  try {
    const motivoRechazo = await sMotivoRechazo.getOneMotivoRechazo(req.params.id)
    console.log(motivoRechazo)
    responses.makeResponseOk(res, { motivoRechazo }, 'motivoRechazo/getOneMotivoRechazo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getByNameMotivoRechazo (req, res) {
  try {
    const motivoRechazo = await sMotivoRechazo.getByNameMotivoRechazo(req.params.tipo)
    console.log(motivoRechazo)
    responses.makeResponseOk(res, { motivoRechazo }, 'motivoRechazo/getByNameMotivoRechazo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSolicitudByMotivo (req, res) {
  try {
    const solicitudServicio = await sMotivoRechazo.getSolictudByMotivo(req.params.id)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getSolicitudByMotivo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function updateMotivoRechazo (req, res) {
  try {
    await sMotivoRechazo.updateMotivoRechazo(req.params.id, res.fields)
    responses.makeResponseOkMessage(res, 'I-motivoRechazo-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteMotivoRechazo (req, res) {
  try {
    await sMotivoRechazo.deleteMotivoRechazo(req.params.id)
    responses.makeResponseOkMessage(res, 'I-motivoRechazo-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createMotivoRechazo,
  getAllMotivoRechazo,
  getByNameMotivoRechazo,
  getOneMotivoRechazo,
  updateMotivoRechazo,
  deleteMotivoRechazo,
  getSolicitudByMotivo
}
