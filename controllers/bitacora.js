'use strict'
const sBitacora = require('../services/bitacora')
const responses = require('../utils/responses')

async function createBitacora (req, res) {
  try {
    await sBitacora.createBitacora(req.fields)
    responses.makeResponseOkMessage(res, 'I-bitacora-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getBitacora (req, res) {
  try {
    const idSolicitudServicio = req.params.id
    const bitacora = await sBitacora.getBitacora(idSolicitudServicio)
    responses.makeResponseOk(res, { bitacora }, 'bitacora/getBitacora')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllBitacora (req, res) {
  try {
    const bitacora = await sBitacora.getAllBitacora()
    responses.makeResponseOk(res, { bitacora }, 'bitacora/getAllBitacora')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createBitacora,
  getBitacora,
  getAllBitacora
}
