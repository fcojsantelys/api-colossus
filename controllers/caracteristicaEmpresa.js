const sCaracteristicaEmpresa = require('../services/caracteristicaEmpresa')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function createCaracteristicaEmpresa (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sCaracteristicaEmpresa.createCaracteristicaEmpresa(req.fields, img)
    responses.makeResponseOkMessage(res, 'I03101')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}

async function updateCaracteristicaEmpresa (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    const caracteristicaEmpresa = await sCaracteristicaEmpresa.updateCaracteristicaEmpresa(req.params.id, req.fields, img)
    responses.makeResponseOk(res, { caracteristicaEmpresa }, 'caracteristicaEmpresa/getCaracteristicaEmpresa')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}
async function getCaracteristicaEmpresa (req, res) {
  try {
    const id = req.params.id
    const caracteristicaEmpresa = await sCaracteristicaEmpresa.getCaracteristicaEmpresa(id)
    responses.makeResponseOk(res, { caracteristicaEmpresa }, 'caracteristicaEmpresa/getCaracteristicaEmpresa')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllCaracteristicaEmpresa (req, res) {
  try {
    const caracteristicaEmpresa = await sCaracteristicaEmpresa.getAllCaracteristicaEmpresa()
    responses.makeResponseOk(res, { caracteristicaEmpresa }, 'caracteristicaEmpresa/getAllCaracteristicaEmpresa')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createCaracteristicaEmpresa,
  updateCaracteristicaEmpresa,
  getCaracteristicaEmpresa,
  getAllCaracteristicaEmpresa

}
