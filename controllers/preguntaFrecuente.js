const sPreguntaFrecuente = require('../services/preguntaFrecuente')
const responses = require('../utils/responses')

async function createPreguntaFrecuente (req, res) {
  try {
    await sPreguntaFrecuente.createPreguntaFrecuente(req.fields)
    responses.makeResponseOkMessage(res, 'I-preguntaFrecuente-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllPreguntaFrecuente (req, res) {
  try {
    const preguntaFrecuente = await sPreguntaFrecuente.getAllPreguntaFrecuente()
    responses.makeResponseOk(res, { preguntaFrecuente }, 'preguntaFrecuente/getAllPreguntaFrecuente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPreguntaFrecuente (req, res) {
  try {
    const id = req.params.id
    const preguntaFrecuente = await sPreguntaFrecuente.getPreguntaFrecuente(id)
    responses.makeResponseOk(res, { preguntaFrecuente }, 'preguntaFrecuente/getPreguntaFrecuente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updatePreguntaFrecuente (req, res) {
  try {
    const preguntaFrecuente = await sPreguntaFrecuente.updatePreguntaFrecuente(req.params.id, req.fields)
    responses.makeResponseOk(res, { preguntaFrecuente }, 'preguntaFrecuente/getPreguntaFrecuente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deletePreguntaFrecuente (req, res) {
  try {
    await sPreguntaFrecuente.deletePreguntaFrecuente(req.params.id)
    responses.makeResponseOkMessage(res, 'I-preguntaFrecuente-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createPreguntaFrecuente,
  getPreguntaFrecuente,
  getAllPreguntaFrecuente,
  updatePreguntaFrecuente,
  deletePreguntaFrecuente }
