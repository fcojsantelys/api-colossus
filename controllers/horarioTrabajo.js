const sHorarioTrabajo = require('../services/horarioTrabajo')
const responses = require('../utils/responses')

async function getHorarioTrabajo (req, res) {
  try {
    const horarioTrabajo = await sHorarioTrabajo.getHorarioTrabajo()
    responses.makeResponseOk(res, { horarioTrabajo }, 'horarioTrabajo/getHorarioTrabajo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateHorarioTrabajo (req, res) {
  try {
    await sHorarioTrabajo.updateHorarioTrabajo(req.fields)
    responses.makeResponseOkMessage(res, 'I-horarioTrabajo-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
module.exports = {
  getHorarioTrabajo,
  updateHorarioTrabajo
}
