
const sUsuario = require('../services/usuario')
const sIncidencia = require('../services/incidencia')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getCliente (req, res) {
  try {
    const id = req.params.id
    const usuario = await sUsuario.getCliente(id)
    responses.makeResponseOk(res, { usuario }, 'usuario/getCliente')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createCliente (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.foto
    // set the image
    await sUsuario.createCliente(req.fields, img)
    responses.makeResponseOkMessage(res, 'I-cliente-01')
  } catch (err) {
    console.log(err)
    responses.makeResponseException(res, err)
  }
}

async function getAllCliente (req, res) {
  try {
    const usuario = await sUsuario.getAllCliente()
    responses.makeResponseOk(res, { usuario }, 'usuario/getAllCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCliente (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.foto
    console.log(req.files)
    await sUsuario.updateCliente(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I-cliente-02')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteCliente (req, res) {
  try {
    await sUsuario.deleteCliente(req.params.id)
    responses.makeResponseOkMessage(res, 'I-cliente-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getEmpleado (req, res) {
  try {
    const id = req.params.id
    const usuario = await sUsuario.getEmpleado(id)
    responses.makeResponseOk(res, { usuario }, 'usuario/getEmpleado')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createEmpleado (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.foto
    // set the image
    await sUsuario.createEmpleado(req.fields, img)
    responses.makeResponseOkMessage(res, 'I-cliente-11')
  } catch (err) {
    console.log(err)
    responses.makeResponseException(res, err)
  }
}

async function getAllEmpleado (req, res) {
  try {
    const usuario = await sUsuario.getAllEmpleado()
    responses.makeResponseOk(res, { usuario }, 'usuario/getAllEmpleado')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateEmpleado (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.foto
    console.log(img)
    await sUsuario.updateEmpleado(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I-cliente-12')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteEmpleado (req, res) {
  try {
    await sUsuario.deleteEmpleado(req.params.id)
    responses.makeResponseOkMessage(res, 'I-cliente-13')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function addCaracteristicasCliente (req, res) {
  try {
    await sUsuario.addCaracteristicasCliente(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-cliente-04')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function setCaracteristicasCliente (req, res) {
  try {
    await sUsuario.setCaracteristicasCliente(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-cliente-08')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function removeCaracteristicasCliente (req, res) {
  try {
    await sUsuario.removeCaracteristicasCliente(req.params.id, req.params.caracteristicaCliente)
    responses.makeResponseOkMessage(res, 'I-cliente-05')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getCaracteristicasCliente (req, res) {
  try {
    const id = req.params.id
    const caracteristicaCliente = await sUsuario.getCaracteristicasCliente(id, req.query)
    responses.makeResponseOk(res, { caracteristicaCliente }, 'caracteristicaCliente/getAllCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getUnaddedCaracteristicasCliente (req, res) {
  try {
    const id = req.params.id
    const caracteristicaCliente = await sUsuario.getUnaddedCaracteristicasCliente(id)
    responses.makeResponseOk(res, { caracteristicaCliente }, 'caracteristicaCliente/getAllCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function addEspecialidad (req, res) {
  try {
    await sUsuario.addEspecialidad(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-cliente-14')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function setEspecialidad (req, res) {
  try {
    await sUsuario.setEspecialidad(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-cliente-16')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function removeEspecialidad (req, res) {
  try {
    await sUsuario.removeEspecialidad(req.params.id, req.params.catalogoServicio)
    responses.makeResponseOkMessage(res, 'I-cliente-15')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getEspecialidad (req, res) {
  try {
    const id = req.params.id
    const catalogoServicio = await sUsuario.getEspecialidad(id)
    responses.makeResponseOk(res, { catalogoServicio }, 'catalogoServicio/getAllCatalogoServicioShort')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getUnaddedEspecialidad (req, res) {
  try {
    const id = req.params.id
    const catalogoServicio = await sUsuario.getUnaddedEspecialidad(id)
    responses.makeResponseOk(res, { catalogoServicio }, 'catalogoServicio/getAllCatalogoServicioShort')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function login (req, res) {
  try {
    const { usuario, token } = await sUsuario.verifyUser(req.fields)
    responses.makeResponseOkHeader(res, { usuario, token }, 'usuario/getUserToken', { key: 'token', value: token })
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function forgotPassword (req, res) {
  try {
    await sUsuario.generatePasswordResetToken(req.query.correo)
    responses.makeResponseOkMessage(res, 'I-cliente-06')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function resetPassword (req, res) {
  try {
    await sUsuario.usePasswordResetToken(req.params.token)
    responses.makeResponseOkMessage(res, 'I-cliente-07')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function unsubscribeEmail (req, res) {
  try {
    await sUsuario.unsubscribeEmail(req.params.id)
    responses.makeResponseOkMessage(res, 'I-cliente-09')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getIncidenciasByTecnico (req, res) {
  try {
    const id = req.currentUser.id
    const agenda = await sIncidencia.getIncidenciasByTecnico(id)
    responses.makeResponseOk(res, { agenda }, 'usuario/getIncidenciasByTecnico')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  getAllCliente,
  getAllEmpleado,
  getCliente,
  getEmpleado,
  createCliente,
  createEmpleado,
  updateCliente,
  updateEmpleado,
  deleteCliente,
  deleteEmpleado,
  addCaracteristicasCliente,
  setCaracteristicasCliente,
  removeCaracteristicasCliente,
  getCaracteristicasCliente,
  getUnaddedCaracteristicasCliente,
  addEspecialidad,
  setEspecialidad,
  removeEspecialidad,
  getEspecialidad,
  getUnaddedEspecialidad,
  login,
  forgotPassword,
  resetPassword,
  unsubscribeEmail,
  getIncidenciasByTecnico
}
