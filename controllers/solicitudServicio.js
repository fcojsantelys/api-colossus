'use strict'

const sSolicitudServicio = require('../services/solicitudServicio')
const responses = require('../utils/responses')

async function createSolicitudServicio (req, res) {
  try {
    await sSolicitudServicio.createSolicitudServicio(req.fields)
    responses.makeResponseOkMessage(res, 'I01801')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSolicitudServicio (req, res) {
  try {
    const id = req.params.id
    const solicitudServicio = await sSolicitudServicio.getSolicitudServicio(id)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getSolicitudServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllSolicitudServicio (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getAllSolicitudServicio(req.query, req.currentUser)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllSolicitudServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllDebug (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getAllDebug()
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllSolicitudServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function denySolicitudServicio (req, res) {
  try {
    await sSolicitudServicio.denySolicitudServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I01802')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function acceptSolicitudServicio (req, res) {
  try {
    await sSolicitudServicio.acceptSolicitudServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I01803')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function finishRevision (req, res) {
  try {
    await sSolicitudServicio.finishRevision(req.params.id, req.fields, req.currentUser)
    responses.makeResponseOkMessage(res, 'I03303')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function generateBudget (req, res) {
  try {
    await sSolicitudServicio.generateBudget(req.params.id, req.fields, req.currentUser)
    responses.makeResponseOkMessage(res, 'I01804')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function acceptBudget (req, res) {
  try {
    await sSolicitudServicio.acceptBudget(req.params.id, req.currentUser)
    responses.makeResponseOkMessage(res, 'I01805')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function rejectBudget (req, res) {
  try {
    await sSolicitudServicio.rejectBudget(req.params.id, req.currentUser, req.fields)
    responses.makeResponseOkMessage(res, 'I01806')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAvailableEmpleadosForAcceptance (req, res) {
  try {
    const usuario = await sSolicitudServicio.getAvailableEmpleadosForAcceptance(req.params.id)
    responses.makeResponseOk(res, { usuario }, 'usuario/getAllEmpleado')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllSolicitudServicioCliente (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getAllSolicitudServicioCliente(req.currentUser)
    console.log(solicitudServicio)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllSolicitudServicioCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllPresupuestoCliente (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getAllPresupuestoCliente(req.currentUser)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllPresupuestoCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSolicitudGarantiaVigente (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getSolicitudGarantiaVigente(req.currentUser.id)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllSolicitudServicioConGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getSolicitudSinGarantia (req, res) {
  try {
    const solicitudServicio = await sSolicitudServicio.getSolicitudSinGarantia(req.currentUser.id)
    responses.makeResponseOk(res, { solicitudServicio }, 'solicitudServicio/getAllSolicitudServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createSolicitudServicio,
  getSolicitudServicio,
  getAllSolicitudServicio,
  denySolicitudServicio,
  acceptSolicitudServicio,
  finishRevision,
  getAvailableEmpleadosForAcceptance,
  generateBudget,
  acceptBudget,
  rejectBudget,
  getAllSolicitudServicioCliente,
  getAllPresupuestoCliente,
  getAllDebug,
  getSolicitudGarantiaVigente,
  getSolicitudSinGarantia
}
