const sTipoComentario = require('../services/tipoComentario')
const responses = require('../utils/responses')

async function createTipoComentario (req, res) {
  try {
    await sTipoComentario.createTipoComentario(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I-tipoComentario-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllTipoComentario (req, res) {
  try {
    const tipoComentario = await sTipoComentario.getAllTipoComentario()
    responses.makeResponseOk(res, { tipoComentario }, 'tipoComentario/getAllTipoComentario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getTipoComentario (req, res) {
  try {
    const id = req.params.id
    const tipoComentario = await sTipoComentario.getTipoComentario(id)
    responses.makeResponseOk(res, { tipoComentario }, 'tipoComentario/getTipoComentario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoComentario (req, res) {
  try {
    await sTipoComentario.updateTipoComentario(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-tipoComentario-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoComentario (req, res) {
  try {
    await sTipoComentario.deleteTipoComentario(req.params.id)
    responses.makeResponseOkMessage(res, 'I-tipoComentario-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createTipoComentario,
  getAllTipoComentario,
  getTipoComentario,
  updateTipoComentario,
  deleteTipoComentario }
