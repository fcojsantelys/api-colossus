const sTipoReclamo = require('../services/tipoReclamo')
const responses = require('../utils/responses')

async function createTipoReclamo (req, res) {
  try {
    await sTipoReclamo.createTipoReclamo(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I01501')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllTipoReclamo (req, res) {
  try {
    let tipoReclamo = await sTipoReclamo.getAllTipoReclamo()
    responses.makeResponseOk(res, { tipoReclamo }, 'tipoReclamo/getAllTipoReclamo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getTipoReclamo (req, res) {
  try {
    let id = req.params.id
    let tipoReclamo = await sTipoReclamo.getTipoReclamo(id)
    responses.makeResponseOk(res, { tipoReclamo }, 'tipoReclamo/getTipoReclamo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoReclamo (req, res) {
  try {
    let tipoReclamo = await sTipoReclamo.updateTipoReclamo(req.params.id, req.fields)
    responses.makeResponseOk(res, { tipoReclamo }, 'tipoReclamo/getTipoReclamo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoReclamo (req, res) {
  try {
    await sTipoReclamo.deleteTipoReclamo(req.params.id)
    responses.makeResponseOkMessage(res, 'I01503')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createTipoReclamo,
  getAllTipoReclamo,
  getTipoReclamo,
  updateTipoReclamo,
  deleteTipoReclamo }
