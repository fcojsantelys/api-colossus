'use strict'

const sPlantillaNotificacion = require('../services/plantillaNotificacion')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function updatePlantillaNotificacion (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    const plantillaNotificacion = await sPlantillaNotificacion.updatePlantillaNotificacion(req.params.id, req.fields, img)
    responses.makeResponseOk(res, { plantillaNotificacion }, 'plantillaNotificacion/getPlantillaNotificacion')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}

async function getPlantillaNotificacion (req, res) {
  try {
    const id = req.params.id
    const plantillaNotificacion = await sPlantillaNotificacion.getPlantillaNotificacion(id)
    responses.makeResponseOk(res, { plantillaNotificacion }, 'plantillaNotificacion/getPlantillaNotificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllPlantillaNotificacion (req, res) {
  try {
    const plantillaNotificacion = await sPlantillaNotificacion.getAllPlantillaNotificacion()
    responses.makeResponseOk(res, { plantillaNotificacion }, 'plantillaNotificacion/getAllPlantillaNotificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  updatePlantillaNotificacion,
  getPlantillaNotificacion,
  getAllPlantillaNotificacion
}
