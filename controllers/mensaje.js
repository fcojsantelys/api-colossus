const sMensaje = require('../services/mensaje')
const responses = require('../utils/responses')

async function getMensaje (req, res) {
  try {
    const id = req.params.id
    const mensaje = await sMensaje.getMensaje(id)
    responses.makeResponseOk(res, { mensaje }, 'mensaje/getMensaje')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function getAllMensaje (req, res) {
  try {
    const mensaje = await sMensaje.getAllMensaje()
    responses.makeResponseOk(res, { mensaje }, 'mensaje/getAllMensaje')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateMensaje (req, res) {
  try {
    await sMensaje.updateMensaje(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I00102')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getMensaje,
  getAllMensaje,
  updateMensaje
}
