
const sCarrusel = require('../services/carrusel')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getCarrusel (req, res) {
  try {
    const id = req.params.id
    const carrusel = await sCarrusel.getCarrusel(id)
    responses.makeResponseOk(res, { carrusel }, 'carrusel/getCarrusel')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createCarrusel (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    // set the image
    await sCarrusel.createCarrusel(req.fields, img)
    responses.makeResponseOkMessage(res, 'I02301')
  } catch (err) {
    console.log(err)
    responses.makeResponseException(res, err)
  }
}

async function getAllCarrusel (req, res) {
  try {
    const carrusel = await sCarrusel.getAllCarrusel()
    responses.makeResponseOk(res, { carrusel }, 'carrusel/getAllCarrusel')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCarrusel (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    console.log(img)
    await sCarrusel.updateCarrusel(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I02302')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteCarrusel (req, res) {
  try {
    await sCarrusel.deleteCarrusel(req.params.id)
    responses.makeResponseOkMessage(res, 'I02303')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getCarrusel,
  createCarrusel,
  getAllCarrusel,
  updateCarrusel,
  deleteCarrusel
}
