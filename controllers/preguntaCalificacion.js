const sPreguntaCalificacion = require('../services/preguntaCalificacion')
const responses = require('../utils/responses')

async function createPreguntaCalificacion (req, res) {
  try {
    await sPreguntaCalificacion.createPreguntaCalificacion(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I-preguntaCalificacion-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllPreguntaCalificacion (req, res) {
  try {
    const preguntaCalificacion = await sPreguntaCalificacion.getAllPreguntaCalificacion()
    responses.makeResponseOk(res, { preguntaCalificacion }, 'preguntaCalificacion/getAllPreguntaCalificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPreguntaCalificacion (req, res) {
  try {
    const id = req.params.id
    const preguntaCalificacion = await sPreguntaCalificacion.getPreguntaCalificacion(id)
    responses.makeResponseOk(res, { preguntaCalificacion }, 'preguntaCalificacion/getPreguntaCalificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updatePreguntaCalificacion (req, res) {
  try {
    await sPreguntaCalificacion.updatePreguntaCalificacion(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-preguntaCalificacion-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deletePreguntaCalificacion (req, res) {
  try {
    await sPreguntaCalificacion.deletePreguntaCalificacion(req.params.id)
    responses.makeResponseOkMessage(res, 'I-preguntaCalificacion-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createPreguntaCalificacion,
  getAllPreguntaCalificacion,
  getPreguntaCalificacion,
  updatePreguntaCalificacion,
  deletePreguntaCalificacion }
