const sBloqueHorario = require('../services/bloqueHorario')
const responses = require('../utils/responses')

async function createBloqueHorario (req, res) {
  try {
    await sBloqueHorario.createBloqueHorario(req.fields)
    responses.makeResponseOkMessage(res, 'I04201')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllBloqueHorario (req, res) {
  try {
    const bloqueHorario = await sBloqueHorario.getAllBloqueHorario()
    responses.makeResponseOk(res, { bloqueHorario }, 'bloqueHorario/getAllBloqueHorario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getBloqueHorario (req, res) {
  try {
    const id = req.params.id
    const bloqueHorario = await sBloqueHorario.getBloqueHorario(id)
    responses.makeResponseOk(res, { bloqueHorario }, 'bloqueHorario/getBloqueHorario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateBloqueHorario (req, res) {
  try {
    const bloqueHorario = await sBloqueHorario.updateBloqueHorario(req.params.id, req.fields)
    responses.makeResponseOk(res, { bloqueHorario }, 'bloqueHorario/getBloqueHorario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteBloqueHorario (req, res) {
  try {
    await sBloqueHorario.deleteBloqueHorario(req.params.id)
    responses.makeResponseOkMessage(res, 'I04203')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  createBloqueHorario,
  getBloqueHorario,
  getAllBloqueHorario,
  updateBloqueHorario,
  deleteBloqueHorario }
