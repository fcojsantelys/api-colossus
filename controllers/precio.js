const sPrecio = require('../services/precio')
const responses = require('../utils/responses')

async function getPrecio (req, res) {
  try {
    const precio = await sPrecio.getPrecio()
    responses.makeResponseOk(res, { precio }, 'precio/getPrecio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updatePrecio (req, res) {
  try {
    const precio = await sPrecio.updatePrecio(req.fields)
    responses.makeResponseOk(res, { precio }, 'precio/getPrecio', 'I-precio-01')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getPrecio,
  updatePrecio
}
