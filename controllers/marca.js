// const sRole = require("../services/sRole"),
//  { makeResponseOk, makeResponseException, makeResponseOkMessage } = require("../global/response");
// secret_key = require("../config").secret_key;
const sMarca = require('../services/marca')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getMarca (req, res) {
  try {
    const id = req.params.id
    const marca = await sMarca.getMarca(id)
    responses.makeResponseOk(res, { marca }, 'marca/getMarca')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createMarca (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    // set the image
    await sMarca.createMarca(req.fields, img)
    responses.makeResponseOkMessage(res, 'I00101')
  } catch (err) {
    console.log(err)
    responses.makeResponseException(res, err)
  }
}

async function getAllMarca (req, res) {
  try {
    const marca = await sMarca.getAllMarca()
    responses.makeResponseOk(res, { marca }, 'marca/getAllMarca')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateMarca (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    console.log(img)
    await sMarca.updateMarca(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I00102')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteMarca (req, res) {
  try {
    await sMarca.deleteMarca(req.params.id)
    responses.makeResponseOkMessage(res, 'I00103')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getMarca,
  createMarca,
  getAllMarca,
  updateMarca,
  deleteMarca
}
