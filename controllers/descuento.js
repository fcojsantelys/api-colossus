const sDescuento = require('../services/descuento')
const responses = require('../utils/responses')

async function createDescuento (req, res) {
  try {
    await sDescuento.createDescuento(req.fields)
    responses.makeResponseOkMessage(res, 'I-descuento-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getDescuento (req, res) {
  try {
    const descuento = await sDescuento.getDescuento(req.params.id)
    responses.makeResponseOk(res, { descuento }, 'descuento/getDescuento')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllDescuento (req, res) {
  try {
    const descuento = await sDescuento.getAllDescuento()
    responses.makeResponseOk(res, { descuento }, 'descuento/getAllDescuento')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateDescuento (req, res) {
  try {
    await sDescuento.updateDescuento(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-descuento-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteDescuento (req, res) {
  try {
    await sDescuento.deleteDescuento(req.params.id)
    responses.makeResponseOkMessage(res, 'I-descuento-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createDescuento,
  getAllDescuento,
  getDescuento,
  updateDescuento,
  deleteDescuento
}
