'use strict'

const sCatalogoServicio = require('../services/catalogoServicio')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getCatalogoServicio (req, res) {
  try {
    const id = req.params.id
    const catalogoServicio = await sCatalogoServicio.getCatalogoServicio(id)
    responses.makeResponseOk(res, { catalogoServicio }, 'catalogoServicio/getCatalogoServicio')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createCatalogoServicio (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sCatalogoServicio.createCatalogoServicio(req.fields, img)
    responses.makeResponseOkMessage(res, 'I00501')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function getAllCatalogoServicio (req, res) {
  try {
    const catalogoServicio = await sCatalogoServicio.getAllCatalogoServicio()
    responses.makeResponseOk(res, { catalogoServicio }, 'catalogoServicio/getAllCatalogoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCatalogoServicio (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sCatalogoServicio.updateCatalogoServicio(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I00502')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteCatalogoServicio (req, res) {
  try {
    await sCatalogoServicio.deleteCatalogoServicio(req.params.id)
    responses.makeResponseOkMessage(res, 'I00503')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getRevisionActivity (req, res) {
  try {
    const actividad = await sCatalogoServicio.getRevisionActivity(req.params.id)
    responses.makeResponseOk(res, { actividad }, 'actividad/getActividad')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getCatalogoServicio,
  createCatalogoServicio,
  getAllCatalogoServicio,
  updateCatalogoServicio,
  deleteCatalogoServicio,
  getRevisionActivity
}
