const sReporte = require('../services/reporte')
const responses = require('../utils/responses')

async function reporteSolicitud (req, res) {
  try {
    let solicitudServicio = await sReporte.reporteSolicitud(req.query)
    responses.makeResponseOk(res, { solicitudServicio }, 'reporte/getAllReporteSolicitud')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteGarantia (req, res) {
  try {
    const solicitudServicio = await sReporte.reporteGarantia(req.query)
    responses.makeResponseOk(res, { solicitudServicio }, 'reporte/getAllReporteGarantia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteCliente (req, res) {
  try {
    let usuario = await sReporte.reporteCliente(req.query)
    responses.makeResponseOk(res, { usuario }, 'reporte/getAllReporteCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteEstadisticoSolicitud (req, res) {
  try {
    const catalogoServicio = await sReporte.reporteEstadisticoSolicitud(req.query)
    responses.makeResponseOk(res, { catalogoServicio }, 'reporte/getAllReporteEstadisticoSolicitud')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteRevision (req, res) {
  try {
    let solicitudServicio = await sReporte.reporteRevision(req.query)
    responses.makeResponseOk(res, { solicitudServicio }, 'reporte/getAllReporteRevision')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteIncidencia (req, res) {
  try {
    let solicitudServicio = await sReporte.reporteIncidencia(req.query)
    responses.makeResponseOk(res, { solicitudServicio }, 'reporte/getAllReporteIncidencia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteEstadisticoTecnico (req, res) {
  try {
    let objeto = await sReporte.reporteEstadisticoTecnico(req.query)
    responses.makeResponseOk(res, { objeto }, 'reporte/getAllReporteEstadisticoTecnico')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteEstadisticoTecnico2 (req, res) {
  try {
    let objeto = await sReporte.reporteEstadisticoTecnico2(req.query)
    responses.makeResponseOk(res, { objeto }, 'reporte/getAllReporteEstadisticoTecnico2')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteEstadisticoAnual (req, res) {
  try {
    let objeto = await sReporte.reporteEstadisticoAnual(req.query)
    responses.makeResponseOk(res, { objeto }, 'reporte/getAllReporteEstadisticoAnual')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function reporteEstadisticoTiempoProm (req, res) {
  try {
    let objeto = await sReporte.reporteEstadisticoTiempoProm(req.query)
    responses.makeResponseOk(res, { objeto }, 'reporte/getAllReporteEstadisticoTiempo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
module.exports = {
  reporteSolicitud,
  reporteGarantia,
  reporteEstadisticoSolicitud,
  reporteRevision,
  reporteIncidencia,
  reporteEstadisticoTecnico,
  reporteEstadisticoTecnico2,
  reporteEstadisticoAnual,
  reporteCliente,
  reporteEstadisticoTiempoProm
}
