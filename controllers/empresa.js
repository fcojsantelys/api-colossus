const sEmpresa = require('../services/empresa')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function createEmpresa (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.logo
    console.log(img)
    await sEmpresa.createEmpresa(req.fields, img)
    responses.makeResponseOkMessage(res, 'I01001')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}
async function getEmpresa (req, res) {
  try {
    const empresa = await sEmpresa.getEmpresa()
    responses.makeResponseOk(res, { empresa }, 'empresa/getEmpresa')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function updateEmpresa (req, res) {
  try {
    let img, imgMision, imgVision, imgObjetivoGeneral, imgFavIco
    if (!isEmpty(req.files)) {
      img = req.files.logo
      imgMision = req.files.imagenMision
      imgVision = req.files.imagenVision
      imgObjetivoGeneral = req.files.imagenObjetivoGeneral
      imgFavIco = req.files.imagenFavIco
    }
    await sEmpresa.updateEmpresa(req.fields, img, imgMision, imgVision, imgObjetivoGeneral, imgFavIco)
    responses.makeResponseOkMessage(res, 'I01002')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createEmpresa,
  getEmpresa,
  updateEmpresa }
