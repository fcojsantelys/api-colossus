'use strict'

const sOrdenServicio = require('../services/ordenServicio')
const responses = require('../utils/responses')

async function getAllOrdenServicio (req, res) {
  try {
    const ordenServicio = await sOrdenServicio.getAllOrdenServicio(req.query)
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getAllOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllOrdenServicioRepairing (req, res) {
  try {
    const ordenServicio = await sOrdenServicio.getAllOrdenServicioRepairing(req.currentUser)
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getAllOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getOrdenServicio (req, res) {
  try {
    const { id } = req.params
    const ordenServicio = await sOrdenServicio.getOrdenServicioExcludingRevisionActivities(id)
    console.log(ordenServicio.agendas)
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSchedulableTasks (req, res) {
  try {
    const { id } = req.params
    const actividad = await sOrdenServicio.getSchedulableTasks(id)
    responses.makeResponseOk(res, { actividad }, 'actividad/getAllActividad')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function completeRepair (req, res) {
  try {
    const { id } = req.params
    const actividad = await sOrdenServicio.completeRepair(id)
    responses.makeResponseOk(res, { actividad }, 'actividad/getAllActividad', 'I-ordenServicio-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getUncompletedAgendas (req, res) {
  try {
    const { id } = req.params
    const agenda = await sOrdenServicio.getUncompletedAgendas(req.currentUser, id)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAllAgenda')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getGarantiaByOrdenServicio (req, res) {
  try {
    const id = req.params.id
    const garantia = await sOrdenServicio.getGarantiaByOrdenServicio(id)
    responses.makeResponseOk(res, { garantia }, 'garantia/getGarantiaByOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function setAsDelivered (req, res) {
  try {
    await sOrdenServicio.setAsDelivered(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-ordenServicio-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function createCalificacionServicio (req, res) {
  try {
    await sOrdenServicio.createCalificacionServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-calificacionServicio-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllCalificacionServicio (req, res) {
  try {
    const ordenServicio = await sOrdenServicio.getAllCalificacionServicio()
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getAllOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getCalificacionServicio (req, res) {
  try {
    const id = req.params.id
    const ordenServicio = await sOrdenServicio.getCalificacionServicio(id)
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getOrdenServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateOrdenEntregarSinGarantia (req, res) {
  try {
    const id = req.params.id
    await sOrdenServicio.updateOrdenEntregarSinGarantia(id)
    responses.makeResponseOkMessage(res, 'I-ordenServicio-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getSinCalificarServicioCliente (req, res) {
  try {
    const ordenServicio = await sOrdenServicio.getSinCalificarServicioCliente(req.currentUser.id)
    responses.makeResponseOk(res, { ordenServicio }, 'ordenServicio/getAllOrdenServicioSinCalificar')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  getAllOrdenServicio,
  getAllOrdenServicioRepairing,
  getOrdenServicio,
  getSchedulableTasks,
  getUncompletedAgendas,
  completeRepair,
  getGarantiaByOrdenServicio,
  setAsDelivered,
  createCalificacionServicio,
  getCalificacionServicio,
  getAllCalificacionServicio,
  updateOrdenEntregarSinGarantia,
  getSinCalificarServicioCliente
}
