const sAgenda = require('../services/agenda')
const responses = require('../utils/responses')

async function createAgenda (req, res) {
  try {
    await sAgenda.scheduleActivity(req.fields)
    responses.makeResponseOkMessage(res, 'I-agenda-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllAgenda (req, res) {
  try {
    const agenda = await sAgenda.getAllAgenda(req.query)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAllAgenda')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAgenda (req, res) {
  try {
    const id = req.params.id
    const agenda = await sAgenda.getAgenda(id)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAgenda')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateAgenda (req, res) {
  try {
    const agenda = await sAgenda.updateAgenda(req.params.id, req.fields)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAgenda')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteAgenda (req, res) {
  try {
    await sAgenda.deleteAgenda(req.params.id)
    responses.makeResponseOkMessage(res, 'I-agenda-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function completeAgenda (req, res) {
  try {
    const agenda = await sAgenda.setAsCompleted(req.params.id)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAgenda', 'I-agenda-04')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function rescheduleAgenda (req, res) {
  try {
    const agenda = await sAgenda.rescheduleAgenda(req.params.id, req.fields)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAgenda', 'I-agenda-05')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getAvailableEmpleados (req, res) {
  try {
    const usuario = await sAgenda.getAvailablesEmpleados(req.query)
    responses.makeResponseOk(res, { usuario }, 'usuario/getAllEmpleadoShort')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAgendasForUser (req, res) {
  try {
    const agenda = await sAgenda.getAgendasForUser(req.currentUser)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAllAgendaGeneral')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPendingAgendasForUser (req, res) {
  try {
    const agenda = await sAgenda.getPendingAgendasForUser(req.currentUser, req.query)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAllAgendaGeneral')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createAgenda,
  getAgenda,
  getAllAgenda,
  updateAgenda,
  deleteAgenda,
  completeAgenda,
  getAvailableEmpleados,
  rescheduleAgenda,
  getAgendasForUser,
  getPendingAgendasForUser
}
