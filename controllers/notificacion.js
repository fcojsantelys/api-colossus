const sNotificacion = require('../services/notificacion')
const responses = require('../utils/responses')

async function getNotificacion (req, res) {
  try {
    const notificacion = await sNotificacion.getNotificacion(req.params.id)
    responses.makeResponseOk(res, { notificacion }, 'notificacion/getNotificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllNotificacion (req, res) {
  try {
    console.log(req.query)
    const notificacion = await sNotificacion.getAllNotificacion(req.currentUser)
    responses.makeResponseOk(res, { notificacion }, 'notificacion/getAllNotificacion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function markAsRead (req, res) {
  try {
    await sNotificacion.markAsRead(req.currentUser)
    responses.makeResponseOkMessage(res, 'I01102')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getNotificacionCount (req, res) {
  try {
    const notificacion = await sNotificacion.getNotificacionCount(req.currentUser)
    console.log(notificacion)
    responses.makeResponseOk(res, { notificacion }, 'notificacion/getCount')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  getNotificacion,
  getAllNotificacion,
  getNotificacionCount,
  markAsRead
}
