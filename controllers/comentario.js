'use strict'
const sComentario = require('../services/comentario')
const responses = require('../utils/responses')

async function createComentario (req, res) {
  try {
    await sComentario.createComentario(req.fields, req.currentUser)
    responses.makeResponseOkMessage(res, 'I-comentario-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function createComentarioNoRegistado (req, res) {
  try {
    await sComentario.createComentarioNoRegistado(req.fields)
    responses.makeResponseOkMessage(res, 'I-comentario-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateComentarioRespuesta (req, res) {
  try {
    await sComentario.updateComentarioRespuesta(req.params.id, req.fields, req.currentUser)
    responses.makeResponseOkMessage(res, 'I-comentario-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllComentario (req, res) {
  try {
    const comentario = await sComentario.getAllComentario()
    responses.makeResponseOk(res, { comentario }, 'comentario/getAllComentario')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getComentarioSinResponder (req, res) {
  try {
    const comentario = await sComentario.getComentarioSinResponder()
    responses.makeResponseOk(res, { comentario }, 'comentario/getComentarioSinResponder')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function deleteComentario (req, res) {
  try {
    await sComentario.deleteComentario(req.params.id, req.currentUser)
    responses.makeResponseOkMessage(res, 'I-comentario-03')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}
async function getComentarioByUser (req, res) {
  try {
    const comentario = await sComentario.getComentarioByUser(req.currentUser)
    responses.makeResponseOk(res, { comentario }, 'comentario/getComentarioByUser')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = { createComentario,
  updateComentarioRespuesta,
  getAllComentario,
  getComentarioSinResponder,
  createComentarioNoRegistado,
  deleteComentario,
  getComentarioByUser }
