module.exports = {
  development: {
    username: process.env.COLOSSUS_DEV_USERNAME || 'postgres',
    password: process.env.COLOSSUS_DEV_PASSWORD || 'postgres',
    database: process.env.COLOSSUS_DEV_DATABASE || 'colossi_development',
    host: process.env.host || '127.0.0.1',
    dialect: 'postgres',
    define: {
      paranoid: true
    },
    logging: console.log,
    timezone: '-04:00'
  },
  test: {
    username: process.env.COLOSSUS_TEST_USERNAME || 'postgres',
    password: process.env.COLOSSUS_TEST_PASSWORD || 'postgres',
    database: process.env.COLOSSUS_TEST_DATABASE || 'colossi_test',
    host: process.env.host || '127.0.0.1',
    dialect: 'postgres'
  },
  production: {
    username: process.env.COLOSSUS_PROD_USERNAME || 'postgres',
    password: process.env.COLOSSUS_PROD_PASSWORD || 'postgres',
    database: process.env.COLOSSUS_PROD_DATABASE || 'colossi_production',
    host: process.env.host || '127.0.0.1',
    dialect: 'postgres'
  }
}
