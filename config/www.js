'use strict'

module.exports = {
  port: process.env.COLOSSUS_PORT || 8900,
  url: process.env.COLOSSUS_URL || 'localhost'
}
