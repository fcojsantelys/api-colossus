'use strict'

module.exports = (sequelize, DataTypes) => {
  const PlantillaNotificacion = sequelize.define('PlantillaNotificacion', {
    descripcion: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    titulo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una titulo vacío.'
        }
      }
    },
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener un nombre vacío.'
        }
      }
    },
    entidad: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una entidad vacía.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tableName: 'plantillaNotificacion',
    name: {
      plural: 'plantillasNotificacion',
      singular: 'plantillaNotificacion'
    }
  })
  PlantillaNotificacion.associate = function (models) {
    PlantillaNotificacion.hasMany(models.Notificacion, { foreignKey: 'idPlantillaNotificacion' })
  }
  return PlantillaNotificacion
}
