'use strict'

module.exports = (sequelize, DataTypes) => {
  const CaracteristicaEmpresa = sequelize.define('CaracteristicaEmpresa', {
    titulo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener título vacío.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripción vacía.'
        }
      }
    },
    icono: DataTypes.STRING,
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tableName: 'caracteristicaEmpresa',
    name: {
      singular: 'caracteristicaEmpresa',
      plural: 'caracteristicasEmpresa'
    }
  })
  return CaracteristicaEmpresa
}
