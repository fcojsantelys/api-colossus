'use strict'

module.exports = (sequelize, DataTypes) => {
  const MotivoRechazo = sequelize.define('MotivoRechazo', {
    tipoRechazo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener un tipo de rechazo vacío.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    }
  },
  {
    tableName: 'motivoRechazo',
    name: {
      plural: 'motivosRechazo',
      singular: 'motivoRechazo'
    }
  })
  MotivoRechazo.associate = function (models) {
    MotivoRechazo.hasMany(models.SolicitudServicio, { foreignKey: 'idMotivoRechazo' })
    MotivoRechazo.hasMany(models.ReclamoServicio, { foreignKey: 'idMotivoRechazo' })
  }
  return MotivoRechazo
}
