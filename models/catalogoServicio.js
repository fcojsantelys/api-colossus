'use strict'
module.exports = (sequelize, DataTypes) => {
  const CatalogoServicio = sequelize.define('CatalogoServicio', {
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener descripción vacía.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  // opciones
  {
    tableName: 'catalogoServicio',
    name: {
      singular: 'catalogoServicio',
      plural: 'catalogosServicio'
    }
  })
  CatalogoServicio.associate = function (models) {
    CatalogoServicio.belongsTo(models.TipoEquipo, { foreignKey: 'idTipoEquipo' })
    CatalogoServicio.belongsTo(models.TipoServicio, { foreignKey: 'idTipoServicio' })
    CatalogoServicio.hasMany(models.Actividad, { foreignKey: 'idCatalogoServicio' })
    CatalogoServicio.hasMany(models.SolicitudServicio, { foreignKey: 'idCatalogoServicio' })
    CatalogoServicio.belongsToMany(models.Usuario, { through: 'especialidad' })
    CatalogoServicio.hasMany(models.Promocion, { foreignKey: 'idCatalogoServicio' })
    CatalogoServicio.hasMany(models.ServicioExhibicion, { foreignKey: 'idCatalogoServicio' })
  }
  return CatalogoServicio
}
