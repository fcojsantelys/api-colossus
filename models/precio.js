'use strict'
module.exports = (sequelize, DataTypes) => {
  const Precio = sequelize.define('Precio', {
    precioMinimoRevision: {
      type: DataTypes.FLOAT,
      validate: {
        min: 0
      }
    },
    precioMaximoRevision: {
      type: DataTypes.FLOAT,
      validate: {
        min: 0
      }
    }
  },
  {
    tableName: 'precio',
    name: {
      singular: 'precio',
      plural: 'precios'
    }

  })
  Precio.associate = (models) => {
  }

  return Precio
}
