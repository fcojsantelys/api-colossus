'use strict'
module.exports = (sequelize, DataTypes) => {
  const BloqueHorario = sequelize.define('BloqueHorario', {
    horaInicio: {
      type: DataTypes.TIME,
      validate: {
        notEmpty: {
          msg: 'Debe tener hora inicio.'
        }
      }
    },
    horaFinal: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Debe tener hora final.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Debe tener una descripcion.'
        }
      }
    }
  },
  {
    tableName: 'bloqueHorario',
    name: {
      singular: 'bloqueHorario',
      plural: 'bloquesHorario'
    }

  })
  BloqueHorario.associate = (models) => {
    BloqueHorario.hasMany(models.Agenda, { foreignKey: 'idBloqueHorario' })
  }

  return BloqueHorario
}
