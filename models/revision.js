'use strict'
module.exports = (sequelize, DataTypes) => {
  const Revision = sequelize.define('Revision', {
    costo: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null
    },
    diagnostico: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    estatusEquipo: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'E'
    }
  },
  {
    tableName: 'revision',
    name: {
      singular: 'revision',
      plural: 'revisiones'
    }
  })
  Revision.associate = (models) => {
    Revision.belongsTo(models.SolicitudServicio, { foreignKey: 'idSolicitudServicio' })
  }
  return Revision
}
