'use strict'
module.exports = (sequelize, DataTypes) => {
  const Condicion = sequelize.define('Condicion', {
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripcion vacía.'
        }
      }
    }
  },
  {
    tableName: 'condicion',
    name: {
      singular: 'condicion',
      plural: 'condiciones'
    }
  })
  Condicion.associate = (models) => {
    Condicion.belongsToMany(models.Garantia, { through: 'condicionGarantia' })
  }
  return Condicion
}
