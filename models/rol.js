'use strict'

module.exports = (sequelize, DataTypes) => {
  const Rol = sequelize.define('Rol', {
    nombre: DataTypes.STRING,
    tipoRol: DataTypes.STRING,
    esJefe: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    tableName: 'rol',
    name: {
      singular: 'rol',
      plural: 'roles'
    }
  })
  Rol.associate = function (models) {
    Rol.hasMany(models.Usuario, { foreignKey: 'idRol' })
    Rol.belongsToMany(models.Permiso, { through: 'permisoRol' }) // this is the jointable's name
  }
  return Rol
}
