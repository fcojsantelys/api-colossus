'use strict'

module.exports = (sequelize, Datatypes) => {
  const SliderMovil = sequelize.define('SliderMovil', {
    titulo: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener el titulo vacio.'
        }
      }
    },
    descripcion: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripcion vacia.'
        }
      }
    },
    icono: Datatypes.STRING
  },
  {
    tableName: 'sliderMovil',
    name: {
      singular: 'SliderMovil',
      plural: 'SlidersMovil'
    }
  })
  return SliderMovil
}
