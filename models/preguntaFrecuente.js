'use strict'
module.exports = (sequelize, DataTypes) => {
  const PreguntaFrecuente = sequelize.define('PreguntaFrecuente', {
    pregunta: {
      type: DataTypes.STRING(500),
      validate: {
        notEmpty: {
          msg: 'No puede tener la pregunta vacia.'
        }
      }
    },
    respuesta: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener una respuesta vacia.'
        }
      }
    }
  },
  {
    tableName: 'preguntaFrecuente',
    name: {
      singular: 'preguntaFrecuente',
      plural: 'preguntasFrecuente'
    }

  })
  PreguntaFrecuente.associate = (models) => {

  }

  return PreguntaFrecuente
}
