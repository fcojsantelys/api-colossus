'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoIncidencia = sequelize.define('TipoIncidencia', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    }
  },
  {
    tableName: 'tipoIncidencia',
    name: {
      singular: 'tipoIncidencia',
      plural: 'tiposIncidencia'
    }

  })
  TipoIncidencia.associate = (models) => {
    TipoIncidencia.hasMany(models.Incidencia, { foreignKey: 'idTipoIncidencia' })
  }

  return TipoIncidencia
}
