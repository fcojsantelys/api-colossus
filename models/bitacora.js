'use strict'
const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  const Bitacora = sequelize.define('Bitacora', {
    fechaMovimiento: {
      type: DataTypes.DATE,
      defaultValue: null,
      get: function () {
        if (this.getDataValue('fechaMovimiento') === null) {
          return null
        }
        return moment(this.getDataValue('fechaMovimiento')).format('DD-MM-YYYY hh:mm:ss')
      }
    },
    descripcion: DataTypes.STRING
  },
  {
    tableName: 'bitacora',
    name: {
      singular: 'bitacora',
      plural: 'bitacoras'
    }
  })

  Bitacora.associate = (models) => {
    Bitacora.belongsTo(models.SolicitudServicio, { foreignKey: 'idSolicitudServicio' })
  }
  return Bitacora
}
