'use strict'
module.exports = (sequelize, DataTypes) => {
  const CaracteristicaCliente = sequelize.define('CaracteristicaCliente', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    }
  },
  // opciones
  {
    tableName: 'caracteristicaCliente',
    name: {
      singular: 'caracteristicaCliente',
      plural: 'caracteristicasCliente'
    }
  })
  CaracteristicaCliente.associate = function (models) {
    CaracteristicaCliente.belongsTo(models.TipoCaracteristicaCliente, { foreignKey: 'idTipoCaracteristicaCliente' })
    CaracteristicaCliente.belongsToMany(models.Usuario, { through: 'perfil' }) // this is the jointable's name
  }
  return CaracteristicaCliente
}
