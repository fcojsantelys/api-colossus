'use strict'
var bcrypt = require('bcryptjs')
var moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const Usuario = sequelize.define('Usuario', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    apellido: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener apellido vacío.'
        }
      }
    },
    correo: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'El correo ya existe'
      },
      validate: {
        notEmpty: {
          msg: 'No puede tener correo vacío.'
        }
      }
    },
    direccion: DataTypes.TEXT,
    telefono: DataTypes.STRING,
    mobilePlayerId: DataTypes.STRING,
    desktopPlayerId: DataTypes.STRING,
    suscritoEmail: DataTypes.BOOLEAN,
    suscritoPushMobile: DataTypes.BOOLEAN,
    suscritoPushDesktop: DataTypes.BOOLEAN,

    fechaNacimiento: {
      type: DataTypes.DATEONLY,
      get: function () {
        return moment(this.getDataValue('fechaNacimiento')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaNacimiento', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      }
    },
    contrasena: DataTypes.STRING,
    resetToken: DataTypes.STRING,
    sexo: {
      type: DataTypes.STRING(1)
    },
    urlFoto: DataTypes.STRING(2000),
    documentoIdentidad: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'El documento de identidad ya existe'
      }
    }
  },
  // options
  {
    tableName: 'usuario',
    name: {
      singular: 'usuario',
      plural: 'usuarios'
    },
    getterMethods: {
      nombreCompleto: function () {
        return `${this.getDataValue('nombre')} ${this.getDataValue('apellido')}`
      }
    }
  })
  Usuario.associate = function (models) {
    Usuario.hasMany(models.Agenda, { foreignKey: 'idUsuario' })
    Usuario.belongsTo(models.Rol, { foreignKey: 'idRol' })
    Usuario.belongsToMany(models.CaracteristicaCliente, { through: 'perfil' })
    Usuario.belongsToMany(models.CatalogoServicio, { through: 'especialidad' })
    Usuario.hasMany(models.SolicitudServicio, { foreignKey: 'idUsuario' })
    Usuario.hasMany(models.OrdenServicio, { foreignKey: 'idUsuario' })
    Usuario.hasMany(models.Comentario, { foreignKey: 'idUsuario' })
    Usuario.hasMany(models.Notificacion, { foreignKey: 'idUsuario' })
    Usuario.hasOne(models.TecnicoExhibicion, { foreignKey: 'idUsuario' })
    Usuario.hasMany(models.ReclamoServicio, { foreignKey: 'idUsuario' })
  }

  Usuario.addHook('beforeCreate', async (usuario, options) => {
    const salt = await bcrypt.genSalt(10)
    usuario.contrasena = await bcrypt.hash(usuario.contrasena, salt)
  })

  Usuario.addHook('beforeUpdate', async (usuario, options) => {
    if (usuario.changed('contrasena')) {
      const salt = await bcrypt.genSalt(10)
      usuario.contrasena = await bcrypt.hash(usuario.contrasena, salt)
    }
  })

  Usuario.prototype.validPassword = async function (password) {
    return bcrypt.compare(password, this.contrasena)
  }
  return Usuario
}
