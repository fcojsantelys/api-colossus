'use strict'

module.exports = (sequelize, DataTypes) => {
  const Notificacion = sequelize.define('Notificacion', {
    descripcion: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    titulo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    entidad: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    idEntidad: DataTypes.INTEGER,
    leida: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tableName: 'notificacion',
    name: {
      plural: 'notificaciones',
      singular: 'notificacion'
    }
  })
  Notificacion.associate = function (models) {
    Notificacion.belongsTo(models.SolicitudServicio, { foreignKey: 'idSolicitudServicio' })
    Notificacion.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
    Notificacion.belongsTo(models.PlantillaNotificacion, { foreignKey: 'idPlantillaNotificacion' })
  }
  return Notificacion
}
