'use strict'

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const OrdenServicio = sequelize.define('OrdenServicio', {
    fechaRespuestaCliente: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaRespuestaCliente') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRespuestaCliente')).format('DD-MM-YYYY')
      }
    },
    fechaFinalizacion: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaFinalizacion') === null) {
          return null
        }
        return moment(this.getDataValue('fechaFinalizacion')).format('DD-MM-YYYY')
      }
    },
    fechaEntrega: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaEntrega') === null) {
          return null
        }
        return moment(this.getDataValue('fechaEntrega')).format('DD-MM-YYYY')
      }
    },
    fechaRespuestaReclamo: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaRespuestaReclamo') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRespuestaReclamo')).format('DD-MM-YYYY')
      }
    },
    fechaFinalizacionReclamo: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaFinalizacionReclamo') === null) {
          return null
        }
        return moment(this.getDataValue('fechaFinalizacionReclamo')).format('DD-MM-YYYY')
      }
    },
    fechaEntregaReclamo: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaEntregaReclamo') === null) {
          return null
        }
        return moment(this.getDataValue('fechaEntregaReclamo')).format('DD-MM-YYYY')
      }
    },
    estatus: {
      type: DataTypes.STRING,
      defaultValue: 'Invisible'
    },
    estatusReclamo: DataTypes.STRING,
    puntaje: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    comentario: {
      type: DataTypes.STRING,
      defaultValue: null
    }
  },
  {
    tableName: 'ordenServicio',
    name: {
      singular: 'ordenServicio',
      plural: 'ordenesServicio'
    },
    getterMethods: {
      fechaCreacion () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY hh:mm:ss')
      }
    }
  })
  OrdenServicio.associate = (models) => {
    OrdenServicio.belongsTo(models.SolicitudServicio, { foreignKey: 'idSolicitudServicio' })
    OrdenServicio.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
    OrdenServicio.belongsToMany(models.Actividad, { through: 'tarea' })
    OrdenServicio.hasOne(models.Garantia, { foreignKey: 'idOrdenServicio' })
    OrdenServicio.hasMany(models.ReclamoServicio, { foreignKey: 'idOrdenServicio' })
    OrdenServicio.hasMany(models.CalificacionServicio, { foreignKey: 'idOrdenServicio' })
    OrdenServicio.hasMany(models.Agenda, { foreignKey: 'idOrdenServicio' })
    OrdenServicio.hasMany(models.CalificacionServicio, { foreignKey: 'idOrdenServicio' })
  }

  return OrdenServicio
}
