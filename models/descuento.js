'use strict'

module.exports = (sequelize, DataTypes) => {
  const Descuento = sequelize.define('Descuento', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    porcentaje: {
      type: DataTypes.FLOAT
    }
  },
  {
    tableName: 'descuento',
    name: {
      plural: 'descuentos',
      singular: 'descuento'
    }
  })
  Descuento.associate = function (models) {
    Descuento.hasMany(models.Promocion, { foreignKey: 'idDescuento' })
  }

  return Descuento
}
