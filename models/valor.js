'use strict'

module.exports = (sequelize, DataTypes) => {
  const Valor = sequelize.define('Valor', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripción vacía.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tablename: 'valor',
    name: {
      plural: 'valores',
      singular: 'valor'
    }
  })

  return Valor
}
