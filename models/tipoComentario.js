'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoComentario = sequelize.define('TipoComentario', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    }
  },
  {
    tableName: 'tipoComentario',
    name: {
      singular: 'tipoComentario',
      plural: 'tiposComentario'
    }
  })
  TipoComentario.associate = (models) => {
    TipoComentario.hasMany(models.Comentario, { foreignKey: 'idTipoComentario' })
  }
  return TipoComentario
}
