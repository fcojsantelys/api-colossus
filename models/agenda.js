'use strict'
const moment = require('moment')

module.exports = (sequelize, Datatypes) => {
  const Agenda = sequelize.define('Agenda', {
    descripcion: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener estatus vacio.'
        }
      }
    },
    estatus: {
      type: Datatypes.STRING,
      defaultValue: 'E',
      validate: {
        notEmpty: {
          msg: 'No puede tener estatus vacio.'
        }
      }
    },
    fechaActividad: {
      type: Datatypes.DATEONLY,
      get: function () {
        return moment(this.getDataValue('fechaActividad')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaActividad', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      },
      allowNull: false
    },
    porReclamo: {
      type: Datatypes.BOOLEAN,
      defaultValue: false
    }
  },
  {
    tableName: 'agenda',
    name: {
      singular: 'agenda',
      plural: 'agendas'
    },
    getterMethods: {
      fechaCreacion () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY')
      }
    },
    scopes: {
      revision: {
        include: [{
          model: sequelize.models.Actividad,
          where: {
            tipo: 'revision'
          },
          required: false
        }]
      },
      actividad: {
        include: [{
          model: sequelize.models.Actividad,
          where: {
            tipo: 'actividad'
          },
          required: true
        }]
      }
    }
  })
  Agenda.associate = (models) => {
    Agenda.belongsTo(models.BloqueHorario, { foreignKey: 'idBloqueHorario' })
    Agenda.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })

    Agenda.belongsTo(models.Actividad, { foreignKey: 'idActividad' })

    Agenda.belongsTo(models.OrdenServicio, { foreignKey: 'idOrdenServicio' })
    Agenda.hasOne(models.Incidencia, { foreignKey: 'idAgenda' })
  }

  return Agenda
}
