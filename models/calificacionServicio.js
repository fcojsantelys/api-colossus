'use strict'
module.exports = (sequelize, DataTypes) => {
  const CalificacionServicio = sequelize.define('CalificacionServicio', {
    respuesta: DataTypes.INTEGER,
    idPreguntaCalificacion: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    idOrdenServicio: {
      type: DataTypes.INTEGER,
      primaryKey: true
    }
  },
  {
    tableName: 'calificacionServicio',
    name: {
      singular: 'calificacionServicio',
      plural: 'calificacionesServicio'
    }
  })
  CalificacionServicio.associate = (models) => {
    CalificacionServicio.belongsTo(models.PreguntaCalificacion, { foreignKey: 'idPreguntaCalificacion' })
    CalificacionServicio.belongsTo(models.OrdenServicio, { foreignKey: 'idOrdenServicio' })
  }

  return CalificacionServicio
}
