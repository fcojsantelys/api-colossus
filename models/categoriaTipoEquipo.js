'use strict'

module.exports = (sequelize, DataTypes) => {
  const CategoriaTipoEquipo = sequelize.define('CategoriaTipoEquipo', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    }
  },
  // options
  {
    tableName: 'categoriaTipoEquipo',
    name: {
      singular: 'categoriaTipoEquipo',
      plural: 'categoriasTipoEquipo'
    }
  })
  CategoriaTipoEquipo.associate = function (models) {
    CategoriaTipoEquipo.hasMany(models.TipoEquipo, { foreignKey: 'idCategoriaTipoEquipo' })
  }
  return CategoriaTipoEquipo
}
