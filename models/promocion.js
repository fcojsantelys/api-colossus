'use strict'

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const Promocion = sequelize.define('Promocion', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    fechaInicio: {
      type: DataTypes.DATEONLY,
      get: function () {
        return moment(this.getDataValue('fechaInicio')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaInicio', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      },
      allowNull: false
    },
    fechaExpiracion: {
      type: DataTypes.DATEONLY,
      get: function () {
        return moment(this.getDataValue('fechaExpiracion')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaExpiracion', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      },
      allowNull: false
    },
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tableName: 'promocion',
    name: {
      plural: 'promociones',
      singular: 'promocion'
    }
  })
  Promocion.associate = function (models) {
    Promocion.belongsTo(models.Descuento, { foreignKey: 'idDescuento' })
    Promocion.hasMany(models.SolicitudServicio, { foreignKey: 'idPromocion' })
    Promocion.belongsTo(models.CatalogoServicio, { foreignKey: 'idCatalogoServicio' })
  }

  return Promocion
}
