'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoReclamo = sequelize.define('TipoReclamo', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    }
  },
  {
    tableName: 'tipoReclamo',
    name: {
      singular: 'tipoReclamo',
      plural: 'tiposReclamo'
    }

  })
  TipoReclamo.associate = (models) => {
    TipoReclamo.hasMany(models.ReclamoServicio, { foreignKey: 'idTipoReclamo' })
  }

  return TipoReclamo
}
