'use strict'

module.exports = (sequelize, DataTypes) => {
  const Carrusel = sequelize.define('Carrusel', {

    urlImagen: DataTypes.STRING(2000)
  },
  // options
  {
    tableName: 'carrusel',
    name: {
      singular: 'carrusel',
      plural: 'carruseles'
    }
  })

  return Carrusel
}
