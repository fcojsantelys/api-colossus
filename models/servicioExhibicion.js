'use strict'

module.exports = (sequelize, DataTypes) => {
  const ServicioExhibicion = sequelize.define('ServicioExhibicion', {
    descripcion: DataTypes.STRING
  }, {
    tableName: 'servicioExhibicion',
    name: {
      singular: 'servicioExhibicion',
      plural: 'serviciosExhibicion'
    }
  })
  ServicioExhibicion.associate = (models) => {
    ServicioExhibicion.belongsTo(models.CatalogoServicio, { foreignKey: 'idCatalogoServicio' })
  }
  return ServicioExhibicion
}
