'use strict'

module.exports = (sequelize, Datatypes) => {
  const Actividad = sequelize.define('Actividad', {
    nombre: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    },
    descripcion: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripcion vacia.'
        }
      }
    },
    tipo: {
      type: Datatypes.STRING,
      defaultValue: 'actividad',
      validate: {
        notEmpty: {
          msg: 'Debe tener un tipo de actividad'
        }
      }
    },
    costo: {
      type: Datatypes.INTEGER,
      defaultValue: 0,
      validate: {
        notEmpty: {
          msg: 'el costo no puede ser 0'
        }
      }
    }
  },
  {
    tableName: 'actividad',
    name: {
      singular: 'actividad',
      plural: 'actividades'
    },
    defaultScope: {
      where: {
        tipo: 'actividad'
      },
      required: false
      // required is set to false so it does not hide catalogos with no activities created yet
    },
    scopes: {
      revision: {
        where: {
          tipo: 'revision'
        },
        required: false
      }
    }
  })

  Actividad.associate = (models) => {
    Actividad.belongsTo(models.CatalogoServicio, { foreignKey: 'idCatalogoServicio' })
    Actividad.belongsToMany(models.ReclamoServicio, { through: 'tareaReclamo' })
    Actividad.hasMany(models.Agenda, { foreignKey: 'idActividad' })
    Actividad.belongsToMany(models.OrdenServicio, { through: 'tarea' })
  }

  return Actividad
}
