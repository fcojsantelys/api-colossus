'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoServicio = sequelize.define('TipoServicio', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    }
  },
  {
    tableName: 'tipoServicio',
    name: {
      singular: 'tipoServicio',
      plural: 'tiposServicio'
    }
  })
  TipoServicio.associate = (models) => {
    TipoServicio.hasMany(models.CatalogoServicio, { foreignKey: 'idTipoServicio' })
  }

  return TipoServicio
}
