'use strict'
module.exports = {
  getSolicitudServicio,
  createSolicitudServicio,
  getAllSolicitudServicio,
  denySolicitudServicio,
  acceptSolicitudServicio,
  finishRevision,
  getAvailableEmpleadosForAcceptance,
  generateBudget,
  acceptBudget,
  rejectBudget,
  getAllSolicitudServicioCliente,
  getAllPresupuestoCliente,
  getAllDebug,
  getSolicitudGarantiaVigente,
  getSolicitudSinGarantia
}

const sequelize = require('../models').sequelize
const SolicitudServicio = require('../models').SolicitudServicio
const Usuario = require('../models').Usuario
const CatalogoServicio = require('../models').CatalogoServicio
const ModeloEquipo = require('../models').ModeloEquipo
const Agenda = require('../models').Agenda
const Actividad = require('../models').Actividad
const MotivoRechazo = require('../models').MotivoRechazo
const Marca = require('../models').Marca
const Garantia = require('../models').Garantia
const DuracionGarantia = require('../models').DuracionGarantia

const Condicion = require('../models').Condicion
const Promocion = require('../models').Promocion
const Descuento = require('../models').Descuento
// const TipoServicio = require('../models').TipoServicio

const Revision = require('../models').Revision
const BloqueHorario = require('../models').BloqueHorario
const Op = require('../models').Sequelize.Op
// const PromocionCatalogoServicio = require('../models').PromocionCatalogoServicio
const OrdenServicio = require('../models').OrdenServicio
const sUsuario = require('./usuario')
const sPromocion = require('./promocion')
const sCatalogoServicio = require('./catalogoServicio')
const sModeloEquipo = require('./modeloEquipo')
const sNotificacion = require('./notificacion')
// const sPromocionCatalogoServicio = require('../services/promocionCatalogoServicio')
const sRevision = require('./revision')
const sAgenda = require('./agenda')
const sOrdenServicio = require('./ordenServicio')
const sBitacora = require('./bitacora')
const sMotivoRechazo = require('./motivoRechazo')
const moment = require('moment')
const throwException = require('../utils/helpers').throwException

async function getSolicitudServicio (id) {
  const solicitudServicio = await SolicitudServicio.findOne({
    where: { id },
    include: [CatalogoServicio, Usuario, Revision, MotivoRechazo,
      {
        model: OrdenServicio,
        include: [Usuario, Actividad, {
          model: Garantia,
          include: [DuracionGarantia, Condicion]
        }]
      }, {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      }],

    order: [['id', 'asc']]
  })
  if (!solicitudServicio) {
    throwException('E-solicitudServicio-01')
  }
  solicitudServicio.agenda = await getLastAgenda(id)

  return solicitudServicio
}

async function createSolicitudServicio (data) {
  if (!data.idCatalogoServicio) {
    throwException('E-solicitudServicio-02')
  }
  await sCatalogoServicio.getCatalogoServicio(data.idCatalogoServicio)

  if (!data.idUsuario) {
    throwException('E-solicitudServicio-05')
  }
  const usuario = await sUsuario.getCliente(data.idUsuario)

  if (!data.idTecnico) {
    throwException('E-solicitudServicio-09')
  }
  await sUsuario.getEmpleado(data.idTecnico)

  if (!data.idModeloEquipo) {
    throwException('E-solicitudServicio-03')
  }

  if (!data.descripcion) {
    throwException('E-solicitudServicio-07')
  }
  const modeloEquipo = await sModeloEquipo.getModeloEquipo(data.idModeloEquipo)

  if (data.idPromocion) {
    const promocion = await sPromocion.getPromocion(data.idPromocion)
    if (promocion.catalogoServicio.id !== parseInt(data.idCatalogoServicio)) {
      throwException('E-solicitudServicio-20') // not available for this service
    }
    const inicio = moment(promocion.fechaInicio, 'DD-MM-YYYY')
    const final = moment(promocion.fechaExpiracion, 'DD-MM-YYYY')
    if (!moment().isBetween(inicio, final)) {
      throwException('E-solicitudServicio-21') // not avaliable on this date
    }
  }

  if (moment().startOf('day').isAfter(moment(data.fechaActividad, 'DD-MM-YYYY'))) {
    throwException('E-solicitudServicio-22')
  }

  await sequelize.transaction(async transaction => {
    const solicitudServicio = await SolicitudServicio.create(data, { transaction: transaction })
    const revision = await sCatalogoServicio.getRevisionActivity(data.idCatalogoServicio)

    const ordenObj = {
      idUsuario: data.idTecnico,
      idSolicitudServicio: solicitudServicio.id
    }

    const orden = await sOrdenServicio.createOrdenServicio(ordenObj, transaction)
    const agendaObj = {
      idUsuario: data.idTecnico,
      idBloqueHorario: data.idBloqueHorario,
      fechaActividad: data.fechaActividad,
      idActividad: revision.id,
      descripcion: `Revision de ${modeloEquipo.nombre} de ${usuario.nombreCompleto}`,
      idOrdenServicio: orden.id
    }

    await sAgenda.scheduleRevision(agendaObj, transaction)

    const bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Solicitud Creada'
    }
    await sBitacora.createBitacora(bitacoraObj, transaction)
  })
}

async function getAllSolicitudServicio (data, currentUser) {
  console.log(data)
  let conditions = {}
  let revisionConditions // it has no revisions so unless we do this, it will bring nothing
  let ordenConditions

  console.log(currentUser.id)

  if (data.estatus) {
    if (data.estatus === 'NE') { // no evaluada
      conditions.estatus = 'E'
    }
    if (data.estatus === 'NR') { // aprobada con revison en espera
      conditions.estatus = 'A'
      revisionConditions = { estatusEquipo: 'E' }
      ordenConditions = { idUsuario: currentUser.id }
    }
    if (data.estatus === 'SP') { // sin presupuesto, aprobada y revisada
      conditions.estatusPresupuesto = null
      conditions.estatus = 'A'
      revisionConditions = { estatusEquipo: 'A' }
      ordenConditions = { idUsuario: currentUser.id }
    }
    console.log(data.estatus)
  }

  return SolicitudServicio.findAll({
    include: [CatalogoServicio, Usuario, MotivoRechazo,
      {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      },
      {
        model: Revision,
        where: revisionConditions
      },
      {
        model: OrdenServicio,
        where: ordenConditions,
        include: [Usuario, Actividad]
      }],
    where: conditions,
    // include: [CatalogoServicio, Usuario, PromocionCatalogoServicio, ModeloEquipo, Revision],
    order: [['id', 'asc']]
  })
}

async function getAllDebug () {
  return SolicitudServicio.findAll({
    include: [CatalogoServicio, Usuario, Revision, MotivoRechazo,
      {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      },
      {
        model: OrdenServicio,
        include: [Usuario, Actividad]
      }]
  })
}

async function denySolicitudServicio (id, data) {
  let solicitudServicio = await getSolicitudServicio(id)

  if (solicitudServicio.estatus !== 'E') {
    throwException('E-solicitudServicio-06')
  }

  if (!data.idMotivoRechazo) {
    throwException('E-solicitudServicio-17')
  }
  await sMotivoRechazo.getOneMotivoRechazo(data.idMotivoRechazo)

  await sequelize.transaction(async t => {
    await solicitudServicio.update(
      { estatus: 'R',
        fechaRespuesta: sequelize.fn('NOW'),
        idMotivoRechazo: data.idMotivoRechazo },
      { where: { id }, transaction: t })
    await Agenda.destroy({
      where: { idOrdenServicio: solicitudServicio.ordenServicio.id },
      transaction: t })
    const bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Solicitud Rechazada'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('solicitud-rechazada', solicitudServicio.usuario, solicitudServicio.id, solicitudServicio)
}

async function acceptSolicitudServicio (id, data) {
  let solicitudServicio = await getSolicitudServicio(id)

  if (solicitudServicio.estatus !== 'E') {
    throwException('E-solicitudServicio-06')
  }
  if (!data.idUsuario) {
    throwException('E-solicitudServicio-05')
  }
  const usuario = await sUsuario.getEmpleado(data.idUsuario)
  const ordenServicio = await solicitudServicio.getOrdenServicio()
  const agenda = await getPendingAgenda(id)

  await sequelize.transaction(async t => {
    await sRevision.createRevision(id, t)

    await agenda.update(
      { idUsuario: usuario.id },
      { transaction: t })
    await ordenServicio.update(
      { idUsuario: usuario.id },
      { transaction: t })
    let bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Solicitud Aceptada'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
    bitacoraObj.descripcion = 'En espera de revision del equipo'
    await sBitacora.createBitacora(bitacoraObj, t)
    await solicitudServicio.update(
      { estatus: 'A', fechaRespuesta: sequelize.fn('NOW') },
      { transaction: t })
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('solicitud-aprobada', solicitudServicio.usuario, solicitudServicio.id, solicitudServicio)
  return solicitudServicio
}

async function finishRevision (id, data, currentUser) {
  let solicitudServicio = await getSolicitudServicio(id)
  const agenda = await getPendingAgenda(id)

  if (solicitudServicio.estatus !== 'A') {
    throwException('E03308')
  }

  if (solicitudServicio.revision.estatusEquipo !== 'E') {
    throwException('E03307')
  }

  if (solicitudServicio.ordenServicio.idUsuario !== currentUser.id) {
    throwException('E03309')
  }

  await sequelize.transaction(async t => {
    await sOrdenServicio.setAsAwaitingBudget(id, t)
    await agenda.update(
      { estatus: 'L' },
      { transaction: t })
    await sRevision.updateRevision(id, data, t)
    await solicitudServicio.update(
      { fechaRevisado: sequelize.fn('NOW') },
      { transaction: t }
    )
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('revision-realizada', solicitudServicio.usuario, solicitudServicio.id, solicitudServicio)
  if (solicitudServicio.revision.estatusEquipo === 'R') { // if not repairable
    await sNotificacion.createNotificacion('retirar-equipo', solicitudServicio.usuario, solicitudServicio.ordenServicio.id, solicitudServicio)
  }
}

async function generateBudget (id, data, currentUser) {
  let solicitudServicio = await getSolicitudServicio(id)
  const ordenServicio = await solicitudServicio.getOrdenServicio()

  if (solicitudServicio.estatus !== 'A') {
    throwException('E-solicitudServicio-12')
  }
  if (solicitudServicio.revision.estatusEquipo !== 'A') {
    throwException('E-solicitudServicio-12')
  }
  if (solicitudServicio.estatusPresupuesto) { // different from null
    throwException('E-solicitudServicio-13')
  }
  /*
  if (!data.costoMateriales) {
    throwException('E-solicitudServicio-10')
  }
  */
  if (!data.actividades) {
    throwException('E-solicitudServicio-11')
  }

  if (solicitudServicio.ordenServicio.idUsuario !== currentUser.id) {
    throwException('E-solicitudServicio-14')
  }

  let costos = await Actividad.findAll({
    where: { id: { [Op.in]: data.actividades } }
  }).map(act => act.get('costo'))

  let presupuesto = costos.reduce((pre, act) => pre + act, 0)
  // what this does is use pre as the accumulator of previous values, and add every one of the activity costs
  console.log(presupuesto)
  // presupuesto += data.costoMateriales
  console.log(`presupuesto con mat= ${presupuesto + data.costoMateriales}`)
  await sequelize.transaction(async t => {
    await ordenServicio.addActividades(data.actividades)
    await sOrdenServicio.setAsAwaitingClient(id, t)
    await solicitudServicio.update(
      { estatusPresupuesto: 'E', fechaGeneracionPresupuesto: sequelize.fn('NOW'), presupuesto: presupuesto },
      { transaction: t })
    const bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Presupuesto Generado'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('presupuesto-generado', solicitudServicio.usuario, solicitudServicio.id, solicitudServicio)
}

async function acceptBudget (id, currentUser) {
  let solicitudServicio = await getSolicitudServicio(id)
  if (solicitudServicio.estatusPresupuesto !== 'E') { // budget not waiting for client response
    throwException('E-solicitudServicio-15')
  }
  if (solicitudServicio.idUsuario !== currentUser.id) {
    throwException('E-solicitudServicio-16')
  }

  await sequelize.transaction(async t => {
    await sOrdenServicio.setAsSchedulingTasks(id)
    await solicitudServicio.ordenServicio.update(
      { fechaRespuestaCliente: sequelize.fn('NOW') },
      { transaction: t })
    let bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Presupuesto Aceptado'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
    bitacoraObj = {
      descripcion: 'Agendando Tarea(s)'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
    await solicitudServicio.update(
      { estatusPresupuesto: 'A' },
      { transaction: t })
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('servicio-asignado', solicitudServicio.ordenServicio.usuario, solicitudServicio.ordenServicio.id, solicitudServicio)
}

async function rejectBudget (id, currentUser, data) {
  let solicitudServicio = await getSolicitudServicio(id)
  if (solicitudServicio.estatusPresupuesto !== 'E') { // budget not waiting for client response
    throwException('E-solicitudServicio-15')
  }
  if (solicitudServicio.idUsuario !== currentUser.id) {
    throwException('E-solicitudServicio-16')
  }

  if (!data.idMotivoRechazo) {
    throwException('E-solicitudServicio-17')
  }
  await sMotivoRechazo.getOneMotivoRechazo(data.idMotivoRechazo)

  await sequelize.transaction(async t => {
    await solicitudServicio.ordenServicio.update(
      { fechaRespuestaCliente: sequelize.fn('NOW') },
      { transaction: t })
    await solicitudServicio.update(
      { estatusPresupuesto: 'R',
        idMotivoRechazo: data.idMotivoRechazo
      },
      { transaction: t })
    await sOrdenServicio.setAsDelivery(id, t)
    const bitacoraObj = {
      idSolicitudServicio: solicitudServicio.id,
      descripcion: 'Presupuesto Rechazado'
    }
    await sBitacora.createBitacora(bitacoraObj, t)
  })
  solicitudServicio = await getSolicitudServicio(id)
  await sNotificacion.createNotificacion('retirar-equipo', solicitudServicio.usuario, solicitudServicio.ordenServicio.id, solicitudServicio)
}

async function getAvailableEmpleadosForAcceptance (id) {
  const solicitudServicio = await getSolicitudServicio(id)
  console.log(solicitudServicio.id)
  const agenda = await getLastAgenda(id)
  const data = {
    idBloqueHorario: agenda.idBloqueHorario,
    fecha: agenda.fechaActividad,
    idCatalogoServicio: solicitudServicio.idCatalogoServicio
  }
  const usuario = await sAgenda.getAvailablesEmpleados(data)
  usuario.push(agenda.usuario)
  return usuario
}

async function getAllSolicitudServicioCliente (currentUser) {
  console.log(currentUser.id)

  return SolicitudServicio.findAll({
    include: [CatalogoServicio, Usuario, Revision, MotivoRechazo,
      {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      },
      {
        model: OrdenServicio,
        include: [Usuario,
          {
            model: Agenda.scope('revision'),
            include: [BloqueHorario, Usuario]
          }]
      }
    ],
    where: { idUsuario: currentUser.id },
    order: [['id', 'asc']]
  })
}

async function getAllPresupuestoCliente (currentUser) {
  console.log(currentUser.id)

  return SolicitudServicio.findAll({
    include: [Usuario, Revision, CatalogoServicio, MotivoRechazo,
      {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      },
      {
        model: OrdenServicio,
        include: [Usuario, Actividad]
      }],
    where: { idUsuario: currentUser.id,
      presupuesto: {
        [Op.ne]: null
      } },
    order: [['id', 'asc']]
  })
}

async function getSolicitudGarantiaVigente (idUsuario) {
  return SolicitudServicio.findAll({
    where: {
      idUsuario: idUsuario
    },
    include: [{
      model: OrdenServicio,
      required: true,
      include: [Usuario, {
        model: Garantia,
        include: [Condicion],
        where: {
          fechaExpiracion: {
            [Op.gt]: moment().format('YYYY-MM-DD')
          }
        }
      }]
    },
    {
      model: ModeloEquipo,
      include: [Marca]
    }, CatalogoServicio]
  })
}
async function getSolicitudSinGarantia (idUsuario) {
  return SolicitudServicio.findAll({
    where: {
      idUsuario: idUsuario
    },
    include: [Usuario, CatalogoServicio, MotivoRechazo, {
      model: Revision,
      where: { estatusEquipo: 'R' }
    }, {
      model: OrdenServicio,
      include: [Usuario],
      where: { estatus: 'Esperando Entrega' }
    }, {
      model: ModeloEquipo,
      include: [Marca]
    }]
  })
}

async function getLastAgenda (id) {
  return Agenda.findOne({
    where: {
      estatus: { [Op.in]: ['E', 'L'] } // en espera, lograda
    },
    include: [Usuario, BloqueHorario, Actividad.scope('revision'),
      {
        model: OrdenServicio,
        where: {
          idSolicitudServicio: id
        }
      }
    ],
    order: [['createdAt', 'desc']] // last created I guess
  })
}

async function getPendingAgenda (id) {
  return Agenda.findOne({
    where: {
      estatus: 'E' // en espera
    },
    include: [Usuario, BloqueHorario, Actividad.scope('revision'),
      // get only activities that are of type 'revision'
      // this should ammount to only one per order, but it is hidden unless scoped
      {
        model: OrdenServicio,
        where: {
          idSolicitudServicio: id
        }
      }
    ],
    order: [['createdAt', 'desc']] // last created I guess
  })
}
