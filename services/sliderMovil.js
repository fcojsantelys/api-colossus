'use strict'

const SliderMovil = require('../models').SliderMovil
const throwException = require('../utils/helpers').throwException

async function createSliderMovil (data) {
  console.log(data)
  await SliderMovil.create(data)
}

async function getSliderMovil (id) {
  const sliderMovil = await SliderMovil.findOne({
    where: { id }
  })
  if (!sliderMovil) {
    throwException('E04401')
  }
  return sliderMovil
}

async function getAllSliderMovil () {
  return SliderMovil.findAll({
    order: [['id', 'asc']]

  })
}

async function updateSliderMovil (id, data) {
  const sliderMovil = await getSliderMovil(id)
  if (!sliderMovil) {
    throwException('E04401')
  }
  return sliderMovil.update(data)
}

async function deleteSliderMovil (id) {
  await getSliderMovil(id)
  return SliderMovil.destroy({
    where: { id }
  })
}

module.exports = { createSliderMovil,
  getSliderMovil,
  getAllSliderMovil,
  updateSliderMovil,
  deleteSliderMovil }
