'use strict'
const Empresa = require('../models').Empresa
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

async function getEmpresa () {
  const empresa = await Empresa.findOne({
    where: { cargada: true }
  })
  if (!empresa) {
    throwException('E01001')
  }
  return empresa
}

async function createEmpresa (data, img) {
  const empresa = await Empresa.findOne() // get "unconfigured" company
  if (empresa.cargada) {
    throwException('E01002') // cant configure if it has already been configured
  }

  data.cargada = true // set is as configured
  console.log(data)
  console.log(img)

  if (!data.nombre || !data.correo || !img) {
    throwException('E01003')
  }

  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlLogo = handleFile(imgPath, 'empresa')
    } else {
      data.urlLogo = new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href
    }
    await empresa.update(data, { t })
  })
}

async function updateEmpresa (data, img, imgMision, imgVision, imgObjetivoGeneral, imgFavIco) {
  await sequelize.transaction(async t => {
    const empresa = await getEmpresa()
    if (img) {
      const imgPath = img.path
      data.urlLogo = handleFile(imgPath, 'empresa')
    }
    if (imgMision) {
      const imgPath = imgMision.path
      data.urlImagenMision = handleFile(imgPath, 'empresa')
    }
    if (imgVision) {
      const imgPath = imgVision.path
      data.urlImagenVision = handleFile(imgPath, 'empresa')
    }
    if (imgObjetivoGeneral) {
      const imgPath = imgObjetivoGeneral.path
      data.urlImagenObjetivoGeneral = handleFile(imgPath, 'empresa')
    }
    if (imgFavIco) {
      const imgPath = imgFavIco.path
      data.urlFavIco = handleFile(imgPath, 'empresa')
    }

    return empresa.update(data, { t })
  })
}

module.exports = { createEmpresa,
  updateEmpresa,
  getEmpresa
}
