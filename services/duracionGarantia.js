'use strict'

const DuracionGarantia = require('../models').DuracionGarantia
const throwException = require('../utils/helpers').throwException

async function createDuracionGarantia (data) {
  console.log(data)
  await DuracionGarantia.create(data)
}

async function getDuracionGarantia (id) {
  const duracionGarantia = await DuracionGarantia.findOne({
    where: { id }
  })
  if (!duracionGarantia) {
    throwException('E-duracionGarantia-01')
  }
  return duracionGarantia
}

async function getAllDuracionGarantia () {
  return DuracionGarantia.findAll({
    order: [['dia', 'asc']]

  })
}

async function updateDuracionGarantia (id, data) {
  const duracionGarantia = await getDuracionGarantia(id)
  return duracionGarantia.update(data)
}

async function deleteDuracionGarantia (id) {
  await getDuracionGarantia(id)
  return DuracionGarantia.destroy({
    where: { id }
  })
}

module.exports = { createDuracionGarantia,
  getDuracionGarantia,
  getAllDuracionGarantia,
  updateDuracionGarantia,
  deleteDuracionGarantia }
