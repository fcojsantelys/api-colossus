'use strict'
const TipoEquipo = require('../models').TipoEquipo
const CategoriaTipoEquipo = require('../models').CategoriaTipoEquipo
const ModeloEquipo = require('../models').ModeloEquipo
const Marca = require('../models').Marca
const CatalogoServicio = require('../models').CatalogoServicio
const Actividad = require('../models').Actividad
const Promocion = require('../models').Promocion

const TipoServicio = require('../models').TipoServicio

const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const sCategoriaTipoEquipo = require('./categoriaTipoEquipo')
// const messages = require('../utils/messages')

async function addTipoEquipo (data, img) {
  if (!data.idCategoriaTipoEquipo) {
    throwException('E00302') // needs the association
  }
  await sCategoriaTipoEquipo.getCategoriaTipoEquipo(data.idCategoriaTipoEquipo)

  console.log(data)
  if (!img) throwException('E00303')
  const imgPath = img.path
  await sequelize.transaction(async t => {
    data.urlImagen = handleFile(imgPath, 'tipoEquipo')
    await TipoEquipo.create(data, { t })
  })
}

async function getAllTipoEquipo () {
  const findall = await TipoEquipo.findAll({
    include: [CategoriaTipoEquipo, ModeloEquipo],
    order: [['id', 'asc']]
  })
  return findall
}

async function getTipoEquipo (id) {
  const tipoEquipo = await TipoEquipo.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [CategoriaTipoEquipo, ModeloEquipo, {
      model: CatalogoServicio,
      include: [TipoServicio, Actividad, Promocion]
    }]
  })

  if (!tipoEquipo) {
    throwException('E00301')
  }
  console.log(tipoEquipo.get({ plain: true }))
  return tipoEquipo
}

async function updateTipoEquipo (id, data, img) {
  await sequelize.transaction(async t => {
    const tipoEquipo = await getTipoEquipo(id)

    if (data.idCategoriaTipoEquipo) {
      await sCategoriaTipoEquipo.getCategoriaTipoEquipo(data.idCategoriaTipoEquipo)
    }

    if (!tipoEquipo) {
      throwException('E00301')
    }

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'tipoEquipo')
    }

    return tipoEquipo.update(data, { t })
  })
}

async function deleteTipoEquipo (id) {
  await getTipoEquipo(id)
  return TipoEquipo.destroy({
    where: {
      id: id
    }
  })
}

async function getMarcasTipoEquipo (id) {
  await getTipoEquipo(id)
  return Marca.findAll({
    include: [{
      model: ModeloEquipo,
      where: { idTipoEquipo: id }
    }]
  })
}

async function getCatalogosTipoEquipo (id) {
  await getTipoEquipo(id)
  return CatalogoServicio.findAll({
    include: [Actividad, Promocion, TipoServicio, {
      model: TipoEquipo,
      where: { id: id }
    }]
  })
}

module.exports = {
  addTipoEquipo,
  getTipoEquipo,
  getAllTipoEquipo,
  updateTipoEquipo,
  deleteTipoEquipo,
  getMarcasTipoEquipo,
  getCatalogosTipoEquipo
}
