'use strict'

const Descuento = require('../models').Descuento
const throwException = require('../utils/helpers').throwException

async function createDescuento (data) {
  await Descuento.create(data)
}

async function getAllDescuento () {
  const descuento = await Descuento.findAll({
    order: [['id', 'asc']]
  })

  if (!descuento) {
    throwException('E-descuento-02')
  }
  return descuento
}

async function getDescuento (id) {
  const descuento = await Descuento.findOne({
    where: { id },
    order: [['id', 'asc']]
  })
  if (!descuento) {
    throwException('E-descuento-01')
  }
  return descuento
}

async function updateDescuento (id, data) {
  const descuento = await getDescuento(id)
  return descuento.update(data)
}

async function deleteDescuento (id) {
  await getDescuento(id)
  return Descuento.destroy({
    where: { id }
  })
}

module.exports = {
  createDescuento,
  getDescuento,
  getAllDescuento,
  updateDescuento,
  deleteDescuento
}
