'use strict'
const Precio = require('../models').Precio
const throwException = require('../utils/helpers').throwException

async function getPrecio () {
  const precio = await Precio.findOne()
  if (!precio) {
    throwException('E-precio-01') // this should never happen if migrations are set the right way
  }
  return precio
}

async function updatePrecio (data) {
  const precio = await getPrecio()
  if (data.precioMinimoRevision > data.precioMaximoRevision) {
    throwException('E-precio-02')
  }
  return precio.update(data)
}

module.exports = {
  updatePrecio,
  getPrecio
}
