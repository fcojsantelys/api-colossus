'use strict'

const ReclamoServicio = require('../models').ReclamoServicio
const OrdenServicio = require('../models').OrdenServicio
const TipoReclamo = require('../models').TipoReclamo
const MotivoRechazo = require('../models').MotivoRechazo
const Usuario = require('../models').Usuario
const Revision = require('../models').Revision
const CatalogoServicio = require('../models').CatalogoServicio
const ModeloEquipo = require('../models').ModeloEquipo
const SolicitudServicio = require('../models').SolicitudServicio
const Marca = require('../models').Marca
const Actividad = require('../models').Actividad
const Agenda = require('../models').Agenda
const BloqueHorario = require('../models').BloqueHorario
const Incidencia = require('../models').Incidencia

const sAgenda = require('../services/agenda')
const sOrdenServicio = require('../services/ordenServicio')
const sTipoReclamo = require('../services/tipoReclamo')
const sUsuario = require('../services/usuario')
const sequelize = require('../models').sequelize
const moment = require('moment')
const Op = require('../models').Sequelize.Op

const throwException = require('../utils/helpers').throwException

const ESTATUS = {
  awaiting: 'En espera',
  accepted: 'Aceptado',
  rejected: 'Rechazado',
  awaitRev: 'Esperando revision',
  finalized: 'Finalizado',
  awaitClaim: 'Esperando entrega reclamo'
}

async function createReclamoServicio (data) {
  if (!data.idOrdenServicio) {
    throwException('E-reclamoServicio-01')
  }

  if (!data.idTipoReclamo) {
    throwException('E-reclamoServicio-02')
  }

  if (!data.idUsuario) {
    throwException('E-reclamoServicio-07')
  }

  const ordenServicio = await sOrdenServicio.getOrdenServicioExcludingRevisionActivities(data.idOrdenServicio)
  const garantia = await sOrdenServicio.getGarantiaByOrdenServicio(data.idOrdenServicio)
  const fActual = moment().startOf('day')
  const fExpiracion = moment(garantia.fechaExpiracion, 'DD-MM-YYYY')

  if (fExpiracion.isBefore(fActual)) {
    throwException('E-reclamoServicio-14')
  }

  const actividad = await Actividad.findOne({
    where: {
      tipo: 'revisionReclamo'
    },
    include: [{
      model: CatalogoServicio,
      where: { id: ordenServicio.solicitudServicio.catalogoServicio.id }
    }]
  })
  await sTipoReclamo.getTipoReclamo(data.idTipoReclamo)
  await sUsuario.getEmpleado(data.idUsuario)
  const agendaObj = {
    idUsuario: data.idUsuario,
    idBloqueHorario: data.idBloqueHorario,
    fechaActividad: data.fechaActividad,
    idActividad: actividad.id,
    descripcion: `Revision de ${ordenServicio.solicitudServicio.modeloEquipo.nombre} de ${ordenServicio.solicitudServicio.usuario.nombreCompleto}`,
    idOrdenServicio: ordenServicio.id,
    porReclamo: true
  }

  if (ordenServicio.estatus === sOrdenServicio.ESTATUS.delivered &&
  !ordenServicio.solicitudServicio.motivoRechazo) { // entregado y no fue rechazado
    await sequelize.transaction(async transaction => {
      console.log(data.idOrdenServicio)
      await OrdenServicio.update({
        estatusReclamo: ESTATUS.awaiting
      }, { where: { id: data.idOrdenServicio },
        transaction })

      await sAgenda.scheduleRevision(agendaObj, transaction)

      await ReclamoServicio.create(data, { transaction })
    })
  } else {
    console.log(ordenServicio.solicitudServicio.motivoRechazo)
    throwException('E-reclamoServicio-08')
    // debe estar entregado el equipo
  }
}

async function getAllReclamoServicio (currentUser, data) {
  console.log(data)
  let conditions = {}
  if (data.estatus) {
    conditions.estatus = data.estatus
  }

  return ReclamoServicio.findAll({
    where: conditions,
    include: [TipoReclamo, MotivoRechazo, {
      required: true,
      model: OrdenServicio,
      include: [Usuario, {
        model: SolicitudServicio,
        required: true,
        include: [Revision, CatalogoServicio, {
          model: ModeloEquipo,
          include: [Marca]
        }, {
          model: Usuario,
          where: { id: currentUser.id }
        }]
      }]
    }],
    order: [['id', 'asc']]
  })
}

async function getReclamoServicio (id) {
  const reclamoServicio = await ReclamoServicio.findOne({
    where: { id },
    include: [TipoReclamo, MotivoRechazo, {
      model: OrdenServicio,
      include: [Usuario, {
        model: SolicitudServicio,
        include: [Revision, CatalogoServicio, Usuario, {
          model: ModeloEquipo,
          include: [Marca]
        }]
      }]
    }]
  })
  if (!reclamoServicio) {
    throwException('E-reclamoServicio-03')
  }
  return reclamoServicio
}

async function acceptReclamoServicio (id) {
  const fActual = moment().startOf('day')
  const reclamoServicio = await getReclamoServicio(id)
  if (reclamoServicio.estatus !== ESTATUS.awaiting) {
    throwException('E-reclamoServicio-03')
  }
  await sequelize.transaction(async transaction => {
    await OrdenServicio.update({
      estatusReclamo: ESTATUS.awaitRev
    }, { where: { id: reclamoServicio.idOrdenServicio },
      transaction })
    await reclamoServicio.update({
      fechaRespuesta: fActual,
      estatus: ESTATUS.accepted,
      estatusRevision: 'E'
    }, { transaction })
  })
}

async function rejectReclamoServicio (id, data) {
  const fActual = moment().startOf('day')
  const reclamoServicio = await getReclamoServicio(id)
  if (reclamoServicio !== ESTATUS.awaiting) {
    throwException('E-reclamoServicio-03')
  }
  if (!data.idMotizoRechazo) {
    throwException('E-reclamoServicio-09')
  }
  const agendaObj = {
    idOrdenServicio: reclamoServicio.idOrdenServicio,
    porReclamo: true
  }

  const agenda = await sAgenda.getAgendaForReclamo(agendaObj)

  await sequelize.transaction(async transaction => {
    await OrdenServicio.update({
      estatusReclamo: ESTATUS.finalized
    }, { where: { id: reclamoServicio.idOrdenServicio },
      transaction })
    await reclamoServicio.update({
      fechaRespuesta: fActual,
      estatus: ESTATUS.rejected,
      estatusRevision: 'R',
      idMotizoRechazo: data.idMotizoRechazo
    }, { transaction })
    await sAgenda.deleteAgenda(agenda.id, transaction)
  })
}

async function rejectRevisionByReclamoServicio (id, data) {
  const fActual = moment().startOf('day')
  const reclamoServicio = await getReclamoServicio(id)

  if (reclamoServicio.estatus !== ESTATUS.accepted) {
    throwException('E-reclamoServicio-04')
  }

  if (reclamoServicio.estatusRevision !== 'E') {
    throwException('E-reclamoServicio-05')
  }

  if (!data.idMotizoRechazo) {
    throwException('E-reclamoServicio-09')
  }
  const agendaObj = {
    idOrdenServicio: reclamoServicio.idOrdenServicio,
    porReclamo: true
  }

  const agenda = sAgenda.getAgendaForReclamo(agendaObj)

  // TODO OPCIONAL:  PONER EL ESTATUS DE RECLAMO ORDEN: "Esperando entrega reclamo" - Listo
  // TODO IMPORTANTE!!! MODIFICAR LOS GETS DE ENTREGA SIN GARANTIA - creo que esta listo
  await sequelize.transaction(async transaction => {
    await OrdenServicio.update({
      estatus: sOrdenServicio.ESTATUS.awaitingDelivery,
      estatusReclamo: ESTATUS.awaitClaim
    }, { where: { id: reclamoServicio.idOrdenServicio },
      transaction })
    await reclamoServicio.update({
      fechaRevision: fActual,
      estatusRevision: 'R',
      diagnostico: data.diagnostico,
      idMotizoRechazo: data.idMotizoRechazo
    }, { transaction })
    await sOrdenServicio.updateOrdenEntregarSinGarantia(reclamoServicio.idOrdenServicio,
      { transaction })
    await Agenda.update({
      where: { id: agenda.id },
      estatus: 'L'
    })
  })
}

async function acceptRevisionByReclamoServicio (id, data) {
  const fActual = moment().startOf('day')

  const reclamoServicio = await getReclamoServicio(id)

  if (reclamoServicio.estatus !== ESTATUS.accepted) {
    throwException('E-reclamoServicio-04')
  }

  if (reclamoServicio.estatusRevision !== 'E') {
    throwException('E-reclamoServicio-05')
  }

  if (!data.diagnostico) { // must have diagnostic
    throwException('E-reclamoServicio-06')
  }

  if (!data.actividades) {
    throwException('E-solicitudServicio-11')
  }

  const ordenServicio = await sOrdenServicio.getOrdenServicioExcludingRevisionActivities(reclamoServicio.idOrdenServicio)
  if (ordenServicio.estatusReclamo !== ESTATUS.awaitRev) {
    throwException('E-reclamoServicio-13')
  }

  await sequelize.transaction(async transaction => {
    await reclamoServicio.update({
      fechaRevision: fActual,
      estatusRevision: 'A',
      diagnostico: data.diagnostico
    }, { transaction })
    await ordenServicio.update({
      estatusReclamo: 'En reparacion'
    }, { transaction })
    await reclamoServicio.addActividades(data.actividades, { transaction })
  })
}

async function getSchedulableReclamoTasks (id) {
  let scheduled = await Actividad.findAll({
    include: [{
      model: Agenda,
      where: { porReclamo: true },
      include: [{
        model: OrdenServicio,
        include: [{
          model: ReclamoServicio,
          where: { id }
        }]
      }]
    }]
  }).map(schdl => schdl.get('id'))
  // basically a subquery workaround, it returns the array of activity ids
  console.log(`actividades agendadas ${scheduled}`)

  return Actividad.findAll({
    where: {
      id: {
        [Op.notIn]: scheduled
      }
    },
    include: [{
      model: ReclamoServicio,
      where: { id: id }
    }]
  })
}

async function getUncompletedAgendas (currentUser, id) {
  return Agenda.findAll({
    where: { estatus: 'E', porReclamo: true },
    include: [ BloqueHorario, Incidencia,
      {
        model: Usuario,
        where: { id: currentUser.id } },
      {
        model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
        include: [CatalogoServicio]
      }, {
        model: OrdenServicio,
        include: [{
          model: ReclamoServicio,
          where: { id: id }
        }]
      }],
    order: [['fechaActividad', 'asc'], [BloqueHorario, 'horaInicio', 'asc']]
  })
}

async function completeRepair (id) {
  const agendaCount = await Agenda.count({
    where: {
      estatus: 'L',
      porReclamo: true
    },
    include: [{
      model: OrdenServicio,
      include: [{
        model: ReclamoServicio,
        where: { id: id }
      }]
    }, {
      model: Actividad,
      where: { tipo: 'actividad' },
      required: true
    }]
  })

  const actividadCount = await Actividad.count({
    include: [{
      model: ReclamoServicio,
      where: { id: id }
    }]
  })
  console.log(`actividades listas ${actividadCount}/${agendaCount}`)

  if (actividadCount !== 0 && actividadCount === agendaCount) {
    let reclamoServicio = await getReclamoServicio(id)
    let ordenServicio = await sOrdenServicio.getOrdenServicioExcludingRevisionActivities(reclamoServicio.ordenServicio.id)
    if (ordenServicio.estatusReclamo === 'En reparacion') {
      await reclamoServicio.update({ estatus: ESTATUS.awaitClaim })
      ordenServicio = await ordenServicio.update({ estatusReclamo: ESTATUS.awaitClaim, fechaFinalizacionReclamo: moment() })
    } else {
      throwException('E-reclamoServicio-13')
    }
    /*
    let ordenServicio = await getOrdenServicioBySolicitud(id)
    if (ordenServicio.estatus === ESTATUS.repairing) {
      await sequelize.transaction(async transaction => {
        let bitacoraObj = {
          idSolicitudServicio: id,
          descripcion: 'Tareas Completadas'
        }
        await sBitacora.createBitacora(bitacoraObj, transaction)
        bitacoraObj.descripcion = ESTATUS.awaitingDelivery
        await sBitacora.createBitacora(bitacoraObj, transaction)
        ordenServicio = await ordenServicio.update({ estatus: ESTATUS.awaitingDelivery, fechaFinalizacion: moment() })
      })
      const solicitudServicio = await getSolicitudByOrdenId(id)
      await sNotificacion.createNotificacion('retirar-equipo', solicitudServicio.usuario, solicitudServicio.ordenServicio.id, solicitudServicio)
      return ordenServicio
    } */
  } else {
    throwException('E-reclamoServicio-12')
  }
}

async function entregarReclamo (id) {
  let reclamoServicio = await getReclamoServicio(id)

  const ordenServicio = await OrdenServicio.findOne({
    where: { estatusReclamo: ESTATUS.awaitClaim },
    include: [{
      model: ReclamoServicio,
      where: { id: reclamoServicio.id }
    }]
  })
  if (!ordenServicio) {
    throwException('E-ordenServicio-07')
  }
  await reclamoServicio.update({ estatus: ESTATUS.finalized })
  return ordenServicio.update({ estatusReclamo: ESTATUS.finalized, fechaEntregaReclamo: moment() })
}

module.exports = {
  createReclamoServicio,
  getAllReclamoServicio,
  getReclamoServicio,
  acceptReclamoServicio,
  rejectReclamoServicio,
  completeRepair,
  entregarReclamo,
  acceptRevisionByReclamoServicio,
  rejectRevisionByReclamoServicio,
  getSchedulableReclamoTasks,
  getUncompletedAgendas
}
