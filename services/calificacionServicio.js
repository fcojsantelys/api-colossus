'use strict'
const OrdenServicio = require('../models').OrdenServicio
const Revision = require('../models').Revision
const SolicitudServicio = require('../models').SolicitudServicio
const PreguntaCalificacion = require('../models').PreguntaCalificacion
const CalificacionServicio = require('../models').CalificacionServicio
const Promocion = require('../models').Promocion
const Usuario = require('../models').Usuario
const CatalogoServicio = require('../models').CatalogoServicio
const ModeloEquipo = require('../models').ModeloEquipo
const Marca = require('../models').Marca
const Descuento = require('../models').Descuento
const throwException = require('../utils/helpers').throwException

async function createCalificacionServicio (idOrdenServicio, data) {
  const ordenServicio = await OrdenServicio.findOne({
    where: { id: idOrdenServicio }
  })
  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  if (ordenServicio.puntaje !== null) {
    throwException('E-calificacionServicio-02')
  }
  if (ordenServicio.estatus === 'Entregado') {
    const calificacionObj = {
      puntaje: data.puntaje,
      comentario: data.comentario
    }
    await ordenServicio.update(calificacionObj)

    data.preguntas.map(pregunta => {
      pregunta.idOrdenServicio = idOrdenServicio
    })
    console.log(data.preguntas)
    return CalificacionServicio.bulkCreate(data.preguntas)
  }
  throwException('E-calificacionServicio-01')
}

async function getCalificacionServicio (idOrdenServicio) {
  const ordenServicio = await OrdenServicio.findOne({
    where: { id: idOrdenServicio },
    include: [{
      model: CalificacionServicio,
      include: [PreguntaCalificacion]
    }]
  })
  console.log(ordenServicio)
  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  if (ordenServicio.estatus === 'Entregado') {
    return ordenServicio
  }
}

async function getAllCalificacionServicio () {
  return OrdenServicio.findAll({
    include: [Usuario, {
      model: CalificacionServicio,
      required: true,
      include: [PreguntaCalificacion]
    }, {
      model: SolicitudServicio,
      include: [Revision, CatalogoServicio, Usuario,
        {
          model: ModeloEquipo,
          include: [Marca]
        }, {
          model: Promocion,
          include: [Descuento]
        }]
    }]
  })
}

module.exports = {
  createCalificacionServicio,
  getCalificacionServicio,
  getAllCalificacionServicio
}
