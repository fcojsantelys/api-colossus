'use strict'

const PreguntaFrecuente = require('../models').PreguntaFrecuente
const throwException = require('../utils/helpers').throwException

async function createPreguntaFrecuente (data) {
  console.log(data)
  await PreguntaFrecuente.create(data)
}

async function getPreguntaFrecuente (id) {
  const preguntaFrecuente = await PreguntaFrecuente.findOne({
    where: { id }
    // include: [Pregunta]
  })
  if (!preguntaFrecuente) {
    throwException('E-preguntaFrecuente-01')
  }
  return preguntaFrecuente
}

async function getAllPreguntaFrecuente () {
  return PreguntaFrecuente.findAll({
    order: [['id', 'asc']]
    // include: [Pregunta]
  })
}

async function updatePreguntaFrecuente (id, data) {
  const preguntaFrecuente = await getPreguntaFrecuente(id)
  return preguntaFrecuente.update(data)
}

async function deletePreguntaFrecuente (id) {
  await getPreguntaFrecuente(id)
  return PreguntaFrecuente.destroy({
    where: { id }
  })
}

module.exports = { createPreguntaFrecuente,
  getPreguntaFrecuente,
  getAllPreguntaFrecuente,
  updatePreguntaFrecuente,
  deletePreguntaFrecuente }
