'use strict'
// ENTITY 005
const sequelize = require('../models').sequelize
const moment = require('moment')
const Op = require('../models').Sequelize.Op
const CatalogoServicio = require('../models').CatalogoServicio
const Actividad = require('../models').Actividad
const Promocion = require('../models').Promocion
const Descuento = require('../models').Descuento

const TipoServicio = require('../models').TipoServicio
const TipoEquipo = require('../models').TipoEquipo
const sTipoEquipo = require('../services/tipoEquipo')
const sTipoServicio = require('../services/tipoServicio')

const handleFile = require('../utils/fileHandler').handleFile
const throwException = require('../utils/helpers').throwException

async function getCatalogoServicio (id) {
  const fActual = moment().format('YYYY-MM-DD')
  const catalogoServicio = await CatalogoServicio.findOne({
    where: { id },
    include: [TipoServicio, TipoEquipo, Actividad, {
      model: Promocion,
      include: Descuento,
      required: false,
      where: {
        fechaInicio: { [Op.lte]: fActual },
        fechaExpiracion: { [Op.gte]: fActual }
      }
    }],
    order: [['id', 'asc']]
  })
  if (!catalogoServicio) {
    throwException('E-catalogoServicio-01')
  }
  return catalogoServicio
}

async function updateCatalogoServicio (id, data, img) {
  if (data.idTipoEquipo) {
    await sTipoEquipo.getTipoEquipo(data.idTipoEquipo)
  }
  if (data.idTipoServicio) {
    await sTipoServicio.getTipoServicio(data.idTipoServicio)
  }

  if (img) {
    const imgPath = img.path
    data.urlImagen = handleFile(imgPath, 'catalogoServicio')
  }

  const catalogoServicio = await getCatalogoServicio(id)
  return catalogoServicio.update(data)
}

async function createCatalogoServicio (data, img) {
  if (!data.idTipoEquipo) {
    throwException('E-catalogoServicio-02') // needs the association
  }
  await sTipoEquipo.getTipoEquipo(data.idTipoEquipo)
  if (!img) {
    throwException('E-catalogoServicio-04')
  }
  const imgPath = img.path

  if (!data.idTipoServicio) {
    throwException('E-catalogoServicio-03') // needs the association here too
  }
  await sTipoServicio.getTipoServicio(data.idTipoServicio)
  // manually verifying that they exist

  console.log(data)

  await sequelize.transaction(async t => {
    data.urlImagen = handleFile(imgPath, 'catalogoServicio')
    const catalogo = await CatalogoServicio.create(data, { transaction: t })
    await Actividad.create({
      nombre: `Revision`,
      descripcion: `Revision de ${data.descripcion}`,
      tipo: 'revision',
      idCatalogoServicio: catalogo.id
    }, { transaction: t })
    await Actividad.create({
      nombre: `Revision por reclamo`,
      descripcion: `Revision por reclamo de ${data.descripcion}`,
      tipo: 'revisionReclamo',
      idCatalogoServicio: catalogo.id
    }, { transaction: t })
  })
}

async function getAllCatalogoServicio () {
  const fActual = moment().format('YYYY-MM-DD')
  return CatalogoServicio.findAll({
    include: [TipoServicio, TipoEquipo, Actividad, {
      model: Promocion,
      include: Descuento,
      required: false,
      where: {
        fechaInicio: { [Op.lte]: fActual },
        fechaExpiracion: { [Op.gte]: fActual }
      }
    }],
    order: [['id', 'asc']]
  })
}
// TODO VERIFICAR SI ESTO ESTA BIEN, NO ESTA MUY CARGADO EL JSON?
// consider to drop Actividad so we can show them on individual searches

async function deleteCatalogoServicio (id) {
  await getCatalogoServicio(id)

  return CatalogoServicio.destroy({
    where: {
      id: id
    }
  })
}

async function getRevisionActivity (id) {
  return Actividad.scope('revision').findOne({
    where: {
      idCatalogoServicio: id
    },
    include: [CatalogoServicio]
  })
}

module.exports = {
  getCatalogoServicio,
  createCatalogoServicio,
  getAllCatalogoServicio,
  updateCatalogoServicio,
  deleteCatalogoServicio,
  getRevisionActivity
}
