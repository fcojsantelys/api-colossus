'use strict'
const TipoCaracteristicaCliente = require('../models').TipoCaracteristicaCliente
const CaracteristicaCliente = require('../models').CaracteristicaCliente
const throwException = require('../utils/helpers').throwException
const Op = require('../models').Sequelize.Op

async function addTipoCaracteristicaCliente (data) {
  console.log(data)
  return TipoCaracteristicaCliente.create(data)
}

async function getAllTipoCaracteristicaCliente () {
  const findall = await TipoCaracteristicaCliente.findAll({
    where: { nombre: { [Op.not]: 'autoperfilado' } },
    order: [['id', 'asc']],
    include: [CaracteristicaCliente]
  })
  return findall
}

async function getTipoCaracteristicaCliente (id) {
  const tipoCaracteristicaCliente = await TipoCaracteristicaCliente.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [CaracteristicaCliente]
  })

  if (!tipoCaracteristicaCliente) {
    throwException('E02401')
  }
  console.log(tipoCaracteristicaCliente.get({ plain: true }))
  return tipoCaracteristicaCliente
}

async function updateTipoCaracteristicaCliente (id, data) {
  const tipoCaracteristicaCliente = await getTipoCaracteristicaCliente(id)
  if (!tipoCaracteristicaCliente) {
    throwException('E02401')
  }
  return tipoCaracteristicaCliente.update(data)
}

async function deleteTipoCaracteristicaCliente (id) {
  await getTipoCaracteristicaCliente(id)
  return TipoCaracteristicaCliente.destroy({
    where: {
      id: id
    }
  })
}

module.exports = {
  addTipoCaracteristicaCliente,
  getTipoCaracteristicaCliente,
  getAllTipoCaracteristicaCliente,
  updateTipoCaracteristicaCliente,
  deleteTipoCaracteristicaCliente
}
