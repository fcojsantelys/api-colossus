'use strict'

const HorarioTrabajo = require('../models').HorarioTrabajo
const throwException = require('../utils/helpers').throwException

async function getHorarioTrabajo () {
  const horarioTrabajo = await HorarioTrabajo.findOne({
  })
  if (!horarioTrabajo) {
    throwException('E-horarioTrabajo-01')
  }
  return horarioTrabajo
}

async function updateHorarioTrabajo (data) {
  const horarioTrabajo = await getHorarioTrabajo()
  if (data.horaInicio > data.horaFinal) {
    throwException('E-horarioTrabajo-02')
  }

  if (data.horaInicioFeriado > data.horaFinalFeriado) {
    throwException('E-horarioTrabajo-03')
  }

  return horarioTrabajo.update(data)
}

module.exports = {
  getHorarioTrabajo,
  updateHorarioTrabajo
}
