'use strict'

const PlantillaNotificacion = require('../models').PlantillaNotificacion
const Notificacion = require('../models').Notificacion

const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize

async function getPlantillaNotificacion (id) {
  const plantillaNotificacion = await PlantillaNotificacion.findOne({
    where: { id }
  })
  if (!plantillaNotificacion) {
    throwException('E-planillaNotificacion-01')
  }
  return plantillaNotificacion
}

async function getPlantillaNotificacionByName (name) {
  const plantillaNotificacion = await PlantillaNotificacion.findOne({
    where: { nombre: name }
  })
  if (!plantillaNotificacion) {
    throwException('E-planillaNotificacion-01')
  }
  return plantillaNotificacion
}
async function updatePlantillaNotificacion (id, data, img) {
  await sequelize.transaction(async t => {
    const plantillaNotificacion = await getPlantillaNotificacion(id)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'plantillaNotificacion')
    }
    await Notificacion.update({ urlImagen: data.urlImagen }, { where: { idPlantillaNotificacion: id } })
    return plantillaNotificacion.update(data, { transaction: t })
  })
}

async function getAllPlantillaNotificacion () {
  const plantillaNotificacion = await PlantillaNotificacion.findAll({
    order: [['id', 'asc']]
  })
  return plantillaNotificacion
}
module.exports = {
  updatePlantillaNotificacion,
  getAllPlantillaNotificacion,
  getPlantillaNotificacionByName,
  getPlantillaNotificacion
}
