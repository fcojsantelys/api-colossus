'use strict'

const CaracteristicaEmpresa = require('../models').CaracteristicaEmpresa
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
async function createCaracteristicaEmpresa (data, img) {
  if (!img) {
    throwException('E-catalogoServicio-04')
  }
  const imgPath = img.path

  await sequelize.transaction(async t => {
    data.urlImagen = handleFile(imgPath, 'caracteristicaEmpresa')
    await CaracteristicaEmpresa.create(data, { transaction: t })
  })
  // IMAGEN AQUI
}

async function getCaracteristicaEmpresa (id) {
  const caracteristicaEmpresa = await CaracteristicaEmpresa.findOne({
    where: { id }
  })
  if (!caracteristicaEmpresa) {
    throwException('E03101')
  }
  return caracteristicaEmpresa
}
async function updateCaracteristicaEmpresa (id, data, img) {
  await sequelize.transaction(async t => {
    const caracteristicaEmpresa = await getCaracteristicaEmpresa(id)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'caracteristicaEmpresa')
    }

    return caracteristicaEmpresa.update(data, { transaction: t })
  })
}

async function getAllCaracteristicaEmpresa () {
  const caracteristicaEmpresa = await CaracteristicaEmpresa.findAll({
    order: [['id', 'asc']]
  })
  if (!caracteristicaEmpresa) {
    throwException('E03102')
  }
  return caracteristicaEmpresa
}
module.exports = {
  createCaracteristicaEmpresa,
  updateCaracteristicaEmpresa,
  getAllCaracteristicaEmpresa,
  getCaracteristicaEmpresa

}
