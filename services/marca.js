'use strict'
const Marca = require('../models').Marca
const ModeloEquipo = require('../models').ModeloEquipo
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage
// const messages = require('../utils/messages')

async function getMarca (id) {
  const marca = await Marca.findOne({
    where: { id },
    include: [ModeloEquipo],
    order: [['id', 'asc']]
  })

  if (!marca) {
    throwException('E-marca-01')
  }
  return marca
}

async function createMarca (data, img) {
  console.log(data)
  console.log(img)

  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'marca')
    } else {
      data.urlImagen = new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href
    }
    await Marca.create(data, { t })
  })
}

async function getAllMarca () {
  return Marca.findAll({
    order: [['id', 'asc']]
  })
}

async function updateMarca (id, data, img) {
  await sequelize.transaction(async t => {
    const marca = await getMarca(id)
    console.log(img)
    console.log(data)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'marca')
    }

    return marca.update(data, { t })
  })
}

async function deleteMarca (id) {
  await getMarca(id)
  // using this in order to throw an exception if
  // it does not find the object that's been requested to be deleted
  return Marca.destroy({
    where: {
      id: id
    }
  })
}

module.exports = {
  getMarca,
  createMarca,
  getAllMarca,
  updateMarca,
  deleteMarca
}
