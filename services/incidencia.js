'use strict'

module.exports = {
  createIncidencia,
  getIncidencia,
  getAllIncidencia,
  getIncidenciasByTecnico
}

const Incidencia = require('../models').Incidencia
const Agenda = require('../models').Agenda
const TipoIncidencia = require('../models').TipoIncidencia
const OrdenServicio = require('../models').OrdenServicio
const SolicitudServicio = require('../models').SolicitudServicio
const ModeloEquipo = require('../models').ModeloEquipo
const Marca = require('../models').Marca
const Usuario = require('../models').Usuario
const sAgenda = require('./agenda')
const sTipoIncidencia = require('./tipoIncidencia')

const throwException = require('../utils/helpers').throwException

async function createIncidencia (data, transaction) {
  if (!data.idAgenda) {
    throwException('E-incidencia-02')
  }
  await sAgenda.getAgenda(data.idAgenda, transaction)

  if (!data.idTipoIncidencia) {
    throwException('E-incidencia-03')
  }
  await sTipoIncidencia.getTipoIncidencia(data.idTipoIncidencia)
  return Incidencia.create(data, { transaction: transaction })
}

async function getIncidencia (id) {
  const incidencia = await Incidencia.findOne({
    where: { id },
    include: [Agenda, TipoIncidencia],
    order: [['id', 'asc']]
  })
  if (!incidencia) {
    throwException('E-incidencia-01')
  }
  return incidencia
}

async function getAllIncidencia () {
  return Incidencia.findAll({
    include: [Agenda, TipoIncidencia],
    order: [['id', 'asc']]
  })
}

async function getIncidenciasByTecnico (idUsuario) {
  const agenda = await Agenda.findAll({
    include: [{
      required: true,
      model: Incidencia,
      include: [TipoIncidencia]
    },
    {
      model: OrdenServicio,
      where: { idUsuario },
      include: [
        {
          model: SolicitudServicio,
          include: [{
            model: ModeloEquipo,
            include: [Marca]
          }, Usuario]
        }]
    }]
  })
  return agenda
}
