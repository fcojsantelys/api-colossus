'use strict'

const MotivoRechazo = require('../models').MotivoRechazo
const SolicitudServicio = require('../models').SolicitudServicio
const Marca = require('../models').Marca
const Revision = require('../models').Revision
const Usuario = require('../models').Usuario
const CatalogoServicio = require('../models').CatalogoServicio
const ModeloEquipo = require('../models').ModeloEquipo
const OrdenServicio = require('../models').OrdenServicio
const Actividad = require('../models').Actividad
const throwException = require('../utils/helpers').throwException

async function createMotivoRechazo (data) {
  if (!data.idMotivoRechazo) {
    throwException('E-motivoRechazo-04')
  }
  await MotivoRechazo.create(data)
}

async function getAllMotivoRechazo () {
  const motivoRechazo = await MotivoRechazo.findAll({
    order: [['id', 'asc']]
  })
  if (!motivoRechazo) {
    throwException('E-motivoRechazo-01')
  }

  return motivoRechazo
}

async function getOneMotivoRechazo (id) {
  const motivoRechazo = await MotivoRechazo.findOne({
    where: { id }
  })
  if (!motivoRechazo) {
    throwException('E-motivoRechazo-02')
  }
  return motivoRechazo
}

async function getByNameMotivoRechazo (tipo) {
  const motivoRechazo = await MotivoRechazo.findAll({
    where: {
      tipoRechazo: [tipo]
    }
  })

  if (!motivoRechazo) {
    throwException('E-motivoRechazo-03')
  }
  return motivoRechazo
}

async function getSolictudByMotivo (id) {
  const solicitud = await SolicitudServicio.findAll({
    where: {
      idMotivoRechazo: id
    },
    include: [CatalogoServicio, Usuario, Revision,
      {
        model: OrdenServicio,
        include: [Usuario, Actividad]
      }, {
        model: ModeloEquipo,
        include: [Marca]
      }, MotivoRechazo]
  })
  if (!solicitud) {
    throwException('E-motivoRechazo-03')
  }
  return solicitud
}

async function updateMotivoRechazo (id, data) {
  const motivoRechazo = await getOneMotivoRechazo(id)
  return motivoRechazo.update(data)
}

async function deleteMotivoRechazo (id) {
  await getOneMotivoRechazo(id)
  return MotivoRechazo.destroy({
    where: { id }
  })
}

module.exports = {
  createMotivoRechazo,
  getAllMotivoRechazo,
  getByNameMotivoRechazo,
  getOneMotivoRechazo,
  updateMotivoRechazo,
  deleteMotivoRechazo,
  getSolictudByMotivo
}
