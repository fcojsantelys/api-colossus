'use strict'
const Comentario = require('../models').Comentario
const throwException = require('../utils/helpers').throwException
const sUsuario = require('../services/usuario')
const sNotificacion = require('../services/notificacion')

const Usuario = require('../models').Usuario
const TipoComentario = require('../models').TipoComentario
const sequelize = require('../models').sequelize

async function createComentario (data, currentUser) {
  await sUsuario.getCliente(currentUser.id)
  data.idUsuario = currentUser.id
  console.log(data)
  await Comentario.create(data)
}
async function createComentarioNoRegistado (data) {
  if (!data.correo || !data.descripcion || !data.idTipoComentario) {
    throwException('E-comentario-02')
  }
  const usuario = await Usuario.findOne({
    where: { correo: data.correo }
  })

  if (usuario) {
    data.idUsuario = usuario.id
    data.correo = undefined
  }
  console.log(data)
  await Comentario.create(data)
}

async function updateComentarioRespuesta (id, data, currentUser) {
  await sUsuario.getEmpleado(currentUser.id)
  const comentario = await getComentario(id)
  data.fechaRespuesta = sequelize.fn('NOW')
  console.log(data)
  const coment = await comentario.update(data)
  const usuario = comentario.usuario ? comentario.usuario : undefined
  const correo = comentario.usuario ? comentario.usuario.correo : comentario.correo
  await sNotificacion.createRespondedComentarioNotification(coment, correo, usuario)
}
async function getAllComentario () {
  return Comentario.findAll({
    include: [Usuario, TipoComentario],
    order: [['createdAt', 'desc']]
  })
}
async function getComentario (id) {
  const comentario = await Comentario.findOne({
    where: { id },
    include: [Usuario, TipoComentario]
  })
  if (!comentario) {
    throwException('E-comentario-01')
  }
  return comentario
}
async function getComentarioSinResponder () {
  return Comentario.findAll({
    where: { respuesta: null },
    include: [Usuario, TipoComentario]
  })
}

async function deleteComentario (id, currentUser) {
  await getComentario(id)
  await sUsuario.getEmpleado(currentUser.id)
  return Comentario.destroy({
    where: { id }
  })
}

async function getComentarioByUser (currentUser) {
  await sUsuario.getCliente(currentUser.id)
  return Comentario.findAll({
    where: { idUsuario: currentUser.id },
    include: [TipoComentario],
    order: [['createdAt', 'desc']]
  })
}

module.exports = { createComentario,
  updateComentarioRespuesta,
  getAllComentario,
  getComentario,
  getComentarioSinResponder,
  createComentarioNoRegistado,
  deleteComentario,
  getComentarioByUser
}
