'use strict'
const Mensaje = require('../models').Mensaje
const throwException = require('../utils/helpers').throwException

async function getMensaje (id) {
  const mensaje = await Mensaje.findOne({
    where: { id },
    order: [['id', 'asc']]
  })

  if (!mensaje) {
    throwException('E-mensaje-01')
  }
  return mensaje
}

async function getAllMensaje () {
  return Mensaje.findAll({
    order: [['id', 'asc']]
  })
}

async function updateMensaje (id, data) {
  const mensaje = await getMensaje(id)
  return mensaje.update(data)
}

module.exports = {
  getMensaje,
  getAllMensaje,
  updateMensaje
}
