'use strict'
const ESTATUS = {
  Invisible: 'Invisible',
  awaitingClient: 'En espera del cliente',
  awaitingBudget: 'En espera de presupuesto',
  scheduling: 'Agendando tareas',
  repairing: 'En reparacion',
  awaitingDelivery: 'Esperando entrega',
  delivered: 'Entregado'
}

module.exports = {
  getOrdenServicioBySolicitud,
  getAgendableOrdenServicio,
  getReschedulableOrdenServicio,
  createOrdenServicio,
  getAllOrdenServicio,
  getAllOrdenServicioRepairing,
  setAsDelivery,
  setAsRepairing,
  setAsAwaitingClient,
  setAsAwaitingBudget,
  setAsSchedulingTasks,
  getSchedulableTasks,
  verifyIfFullyScheduled,
  completeRepair,
  getOrdenServicioExcludingRevisionActivities,
  getUncompletedAgendas,
  getGarantiaByOrdenServicio,
  ESTATUS,
  setAsDelivered,
  createCalificacionServicio,
  getCalificacionServicio,
  getAllCalificacionServicio,
  updateOrdenEntregarSinGarantia,
  getSinCalificarServicioCliente
}
const getSolicitudServicio = require('./solicitudServicio').getSolicitudServicio
const OrdenServicio = require('../models').OrdenServicio
const Actividad = require('../models').Actividad
const BloqueHorario = require('../models').BloqueHorario
const SolicitudServicio = require('../models').SolicitudServicio
const Revision = require('../models').Revision
const ModeloEquipo = require('../models').ModeloEquipo
const Marca = require('../models').Marca
const PreguntaCalificacion = require('../models').PreguntaCalificacion
const CalificacionServicio = require('../models').CalificacionServicio
const CatalogoServicio = require('../models').CatalogoServicio
const Usuario = require('../models').Usuario
const Agenda = require('../models').Agenda
const Incidencia = require('../models').Incidencia
const MotivoRechazo = require('../models').MotivoRechazo
const Promocion = require('../models').Promocion
const Descuento = require('../models').Descuento
const moment = require('moment')
const sGarantia = require('./garantia')
const sNotificacion = require('./notificacion')
const sBitacora = require('./bitacora')
const sequelize = require('../models').sequelize
const Op = require('../models').Sequelize.Op

const throwException = require('../utils/helpers').throwException

async function getOrdenServicioBySolicitud (idSolicitud) {
  const ordenServicio = await OrdenServicio.findOne({
    include: [Usuario, Actividad.unscoped(), {
      model: SolicitudServicio,
      where: { id: idSolicitud },
      include: [Revision, CatalogoServicio, Usuario,
        {
          model: ModeloEquipo,
          include: [Marca]
        }]
    }],
    order: [['id', 'asc']]
  })

  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  return ordenServicio
}

async function getSolicitudByOrdenId (id) {
  const solicitudServicio = await SolicitudServicio.findOne({
    where: { id },
    include: [CatalogoServicio, Usuario, Revision, MotivoRechazo,
      {
        model: OrdenServicio,
        include: [Usuario, Actividad]
      }, {
        model: ModeloEquipo,
        include: [Marca]
      },
      {
        model: Promocion,
        include: [Descuento]
      }],

    order: [['id', 'asc']]
  })
  if (!solicitudServicio) {
    throwException('E-solicitudServicio-01')
  }
  return solicitudServicio
}

async function getOrdenServicioExcludingRevisionActivities (id) {
  const ordenServicio = await OrdenServicio.findOne({
    include: [Usuario, Actividad.unscoped(),
      {
        model: SolicitudServicio,
        where: { id: id },
        include: [Revision, CatalogoServicio, Usuario,
          {
            model: ModeloEquipo,
            include: [Marca]
          }, {
            model: Promocion,
            include: [Descuento]
          }]
      },
      {
        model: Agenda,
        separate: true,
        include: [Usuario, BloqueHorario, {
          model: Actividad,
          required: true,
          include: [CatalogoServicio]
        }]
      }],
    order: [['id', 'asc']]
  })

  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  return ordenServicio
}

async function getAllOrdenServicioRepairing (currentUser) {
  let technicianConditions
  let clientConditions
  console.log(currentUser)

  if (currentUser.rol.tipoRol === 'cliente') {
    clientConditions = { id: currentUser.id }
  } else { // is an employee
    technicianConditions = { id: currentUser.id }
  }
  console.log('cliente', clientConditions)
  console.log('tec', technicianConditions)

  return OrdenServicio.findAll({
    where: {
      estatus: { [Op.in]: [ESTATUS.repairing, ESTATUS.scheduling] }
    },
    include: [
      {
        model: Usuario,
        where: technicianConditions
      },
      {
        model: SolicitudServicio,
        required: true,
        include: [Revision, CatalogoServicio,
          {
            model: Usuario,
            where: clientConditions
          },
          {
            model: ModeloEquipo,
            include: [Marca]
          }, {
            model: Promocion,
            include: [Descuento]
          }]
      }],
    order: [['id', 'desc']]
  })
}

async function getAllOrdenServicio (query) {
  let conditions
  let condRev
  if (query.estatus) {
    conditions = { estatus: query.estatus }
  }

  return OrdenServicio.findAll({
    where: conditions,
    include: [Usuario, Actividad.unscoped(), Agenda,
      {
        model: SolicitudServicio,
        include: [CatalogoServicio, Usuario, {
          model: ModeloEquipo,
          include: [Marca]
        }, {
          model: Promocion,
          include: [Descuento]
        }, {
          model: Revision,
          where: condRev
        }]
      }],
    order: [['id', 'asc']]
  })
}

async function createOrdenServicio (data, transaction) {
  return OrdenServicio.create(data, { transaction: transaction })
}

async function setAsDelivery (idSolicitud, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  if (ordenServicio.estatus === 'Invisible' || ordenServicio.estatus === ESTATUS.awaitingClient) {
    return ordenServicio.update({ estatus: ESTATUS.awaitingDelivery }, { transaction: transaction })
  }
  throwException('E-ordenServicio-02')
}

async function setAsRepairing (idSolicitud, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  if (ordenServicio.estatus === ESTATUS.scheduling) {
    const bitacoraObj = {
      idSolicitudServicio: idSolicitud,
      descripcion: ESTATUS.repairing
    }
    await sBitacora.createBitacora(bitacoraObj, transaction)
    return ordenServicio.update({ estatus: ESTATUS.repairing }, { transaction: transaction })
  }
  throwException('E-ordenServicio-03')
}

async function setAsAwaitingClient (idSolicitud, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  if (ordenServicio.estatus === ESTATUS.awaitingBudget) {
    return ordenServicio.update({ estatus: ESTATUS.awaitingClient }, { transaction: transaction })
  }
  throwException('E-ordenServicio-03')
}

async function setAsAwaitingBudget (idSolicitud, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  if (ordenServicio.estatus === 'Invisible') {
    return ordenServicio.update({ estatus: ESTATUS.awaitingBudget }, { transaction: transaction })
  }
  throwException('E-ordenServicio-03')
}

async function setAsDelivered (idSolicitud, data, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  if (ordenServicio.estatus === ESTATUS.awaitingDelivery) {
    if (data.condiciones) {
      await sequelize.transaction(async transaction => {
        await sGarantia.createGarantia(idSolicitud, data, transaction)
        return ordenServicio.update({ estatus: ESTATUS.delivered, fechaEntregado: moment() }, { transaction })
      })
      const garantia = await sGarantia.getGarantiaByOrdenServicio(idSolicitud)
      const solicitud = await getSolicitudServicio(idSolicitud)
      return sNotificacion.createNotificacion('garantia-entregada', solicitud.usuario, garantia.id, solicitud)
    }
    throwException('E-garantia-06')
  }
  throwException('E-ordenServicio-07')
}

async function setAsSchedulingTasks (idSolicitud, transaction) {
  const ordenServicio = await getOrdenServicioBySolicitud(idSolicitud)
  console.log(ordenServicio.get({ plain: true }))
  if (ordenServicio.estatus === ESTATUS.awaitingClient) {
    return ordenServicio.update({ estatus: ESTATUS.scheduling }, { transaction: transaction })
  }
  throwException('E-ordenServicio-04')
}

async function getAgendableOrdenServicio (id) {
  const ordenServicio = await OrdenServicio.findOne({
    where: {
      id: id,
      estatus: ESTATUS.scheduling
    },
    order: [['id', 'asc']]
  })
  console.log(ordenServicio)
  if (!ordenServicio) {
    throwException('E-ordenServicio-06')
  }
  return ordenServicio
}

async function getReschedulableOrdenServicio (id) {
  const ordenServicio = await OrdenServicio.findOne({
    where: {
      id: id,
      estatus: { [Op.in]: [ESTATUS.scheduling, ESTATUS.repairing] }
    },
    order: [['id', 'asc']]
  })
  console.log(ordenServicio)
  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  return ordenServicio
}

async function getSchedulableTasks (id) {
  let scheduled = await Actividad.findAll({
    include: [{
      model: Agenda,
      required: true,
      include: [{
        model: OrdenServicio,
        where: { id: id }
      }]
    }]
  }).map(schdl => schdl.get('id'))
  // basically a subquery workaround, it returns the array of activity ids
  console.log(`actividades agednadas ${scheduled}`)

  return Actividad.findAll({
    where: {
      id: {
        [Op.notIn]: scheduled
      }
    },
    include: [{
      model: OrdenServicio,
      where: { id: id }
    }]
  })
}

async function verifyIfFullyScheduled (id) {
  const agendaCount = await Agenda.count({
    where: {
      estatus: {
        [Op.ne]: 'R' // finished or scheduled
      }
    },
    include: [{
      model: OrdenServicio,
      where: { id: id }
    }, {
      model: Actividad,
      where: { tipo: 'actividad' },
      required: true
    }]
  })

  const actividadCount = await Actividad.count({
    include: [{
      model: OrdenServicio,
      where: { id: id }
    }]
  })
  console.log(`act ${actividadCount}`)
  console.log(`age ${agendaCount}`)

  if (actividadCount === agendaCount) {
    const ordenServicio = await getAgendableOrdenServicio(id)
    // guaranteed to get the ESTATUS.scheduling estatus as a prerequisite
    return ordenServicio.update({ estatus: ESTATUS.repairing })
  }
}

async function completeRepair (id) {
  const agendaCount = await Agenda.count({
    where: {
      estatus: 'L'
    },
    include: [{
      model: OrdenServicio,
      where: { id: id }
    }, {
      model: Actividad,
      where: { tipo: 'actividad' },
      required: true
    }]
  })

  const actividadCount = await Actividad.count({
    include: [{
      model: OrdenServicio,
      where: { id: id }
    }]
  })
  console.log(`actividades listas ${actividadCount}/${agendaCount}`)

  if (actividadCount !== 0 && actividadCount === agendaCount) {
    let ordenServicio = await getOrdenServicioBySolicitud(id)
    if (ordenServicio.estatus === ESTATUS.repairing) {
      await sequelize.transaction(async transaction => {
        let bitacoraObj = {
          idSolicitudServicio: id,
          descripcion: 'Tareas Completadas'
        }
        await sBitacora.createBitacora(bitacoraObj, transaction)
        bitacoraObj.descripcion = ESTATUS.awaitingDelivery
        await sBitacora.createBitacora(bitacoraObj, transaction)
        ordenServicio = await ordenServicio.update({ estatus: ESTATUS.awaitingDelivery, fechaFinalizacion: moment() })
      })
      const solicitudServicio = await getSolicitudByOrdenId(id)
      await sNotificacion.createNotificacion('retirar-equipo', solicitudServicio.usuario, solicitudServicio.ordenServicio.id, solicitudServicio)
      return ordenServicio
    }
  }
  throwException('E-ordenServicio-05')
}

async function getUncompletedAgendas (currentUser, id) {
  return Agenda.findAll({
    where: { estatus: 'E' },
    include: [ BloqueHorario, Incidencia,
      {
        model: Usuario,
        where: { id: currentUser.id } },
      {
        model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
        include: [CatalogoServicio]
      }, {
        model: OrdenServicio,
        include: [Usuario],
        where: { id: id }
      }],
    order: [['fechaActividad', 'asc'], [BloqueHorario, 'horaInicio', 'desc']]
  })
}

async function getGarantiaByOrdenServicio (id) {
  return sGarantia.getGarantiaByOrdenServicio(id)
}

async function createCalificacionServicio (idOrdenServicio, data) {
  const ordenServicio = await OrdenServicio.findOne({
    where: { id: idOrdenServicio }
  })
  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  if (ordenServicio.puntaje !== null) {
    throwException('E-calificacionServicio-02')
  }
  if (ordenServicio.estatus === 'Entregado') {
    const calificacionObj = {
      puntaje: data.puntaje,
      comentario: data.comentario
    }
    await ordenServicio.update(calificacionObj)

    data.preguntas.map(pregunta => {
      pregunta.idOrdenServicio = idOrdenServicio
    })
    console.log(data.preguntas)
    return CalificacionServicio.bulkCreate(data.preguntas)
  }
  throwException('E-calificacionServicio-01')
}

async function getCalificacionServicio (idOrdenServicio) {
  const ordenServicio = await OrdenServicio.findOne({
    where: { id: idOrdenServicio },
    include: [{
      model: CalificacionServicio,
      required: true,
      include: [PreguntaCalificacion]
    }, {
      model: SolicitudServicio,
      include: [Revision, CatalogoServicio, Usuario,
        {
          model: ModeloEquipo,
          include: [Marca]
        }, {
          model: Promocion,
          include: [Descuento]
        }]
    }]
  })
  console.log(ordenServicio)
  if (!ordenServicio) {
    throwException('E-ordenServicio-01')
  }
  if (ordenServicio.estatus === 'Entregado') {
    return ordenServicio
  }
}

async function getAllCalificacionServicio () {
  return OrdenServicio.findAll({
    include: [Usuario, {
      model: CalificacionServicio,
      required: true,
      include: [PreguntaCalificacion]
    }, {
      model: SolicitudServicio,
      include: [Revision, CatalogoServicio, Usuario,
        {
          model: ModeloEquipo,
          include: [Marca]
        }, {
          model: Promocion,
          include: [Descuento]
        }]
    }]
  })
}

async function updateOrdenEntregarSinGarantia (idOrdenServicio) {
  const ordenServicio = await OrdenServicio.findOne({
    where: { id: idOrdenServicio, estatus: ESTATUS.awaitingDelivery },
    include: [{
      model: SolicitudServicio,
      include: [{
        model: Revision,
        where: { estatusEquipo: 'R' }
      }]
    }]
  })
  if (!ordenServicio) {
    throwException('E-ordenServicio-07')
  }
  const bitacoraObj = {
    idSolicitudServicio: ordenServicio.solicitudServicio.id,
    descripcion: 'Equipo Entregado sin garantia'
  }
  await sequelize.transaction(async transaction => {
    await sBitacora.createBitacora(bitacoraObj, transaction)
    return ordenServicio.update({ estatus: ESTATUS.delivered, fechaEntregado: moment() }, { transaction })
  })
}

async function getSinCalificarServicioCliente (idUsuario) {
  return OrdenServicio.findAll({
    where: { puntaje: null, estatus: 'Entregado' },
    include: [{
      model: SolicitudServicio,
      where: { idUsuario },
      include: [CatalogoServicio, Usuario, {
        model: Revision,
        where: { estatusEquipo: 'A' }
      }, {
        model: ModeloEquipo,
        include: [Marca]
      }, {
        model: Promocion,
        include: [Descuento]
      }]
    }]
  })
}
