'use strict'

const Actividad = require('../models').Actividad
const Agenda = require('../models').Agenda
const sequelize = require('../models').sequelize
const CatalogoServicio = require('../models').CatalogoServicio
const sCatalogoServicio = require('../services/catalogoServicio')
const throwException = require('../utils/helpers').throwException

async function createActividad (data) {
  if (!data.idCatalogoServicio) {
    throwException('E00602')
  }
  await sCatalogoServicio.getCatalogoServicio(data.idCatalogoServicio)
  console.log(data)
  await Actividad.create(data)
}

async function getActividad (id) {
  const actividad = await Actividad.findOne({
    where: { id },
    include: [CatalogoServicio],
    order: [['id', 'asc']]
  })
  if (!actividad) {
    throwException('E00601')
  }
  return actividad
}

async function getAllActividad () {
  return Actividad.findAll({
    include: [CatalogoServicio],
    order: [['id', 'asc']]
  })
}

async function updateActividad (id, data) {
  const actividad = await getActividad(id)

  if (data.idCatalogoServicio) {
    await sCatalogoServicio.getCatalogoServicio(data.idCatalogoServicio)
  }

  await sequelize.transaction(async t => {
    await actividad.update(data, { t })

    if (data.nombre) { // if the name is changed on the activity, change it up on all related agenda events
      const agendaChanges = {
        descripcion: data.nombre
      }

      await Agenda.update(agendaChanges, {
        where: {
          idActividad: id
        },
        transaction: t
      })
    }
    return actividad
  })
}

async function deleteActividad (id) {
  await getActividad(id)

  return Actividad.destroy({
    where: { id }
  })
}

async function getActividadOrRevision (id) {
  const actividad = await Actividad.unscoped().findOne({
    where: { id },
    include: [CatalogoServicio],
    order: [['id', 'asc']]
  })
  if (!actividad) {
    throwException('E00601')
  }
  return actividad
}

module.exports = {
  createActividad,
  getActividad,
  getAllActividad,
  updateActividad,
  deleteActividad,
  getActividadOrRevision
}
