'use strict'

const TipoComentario = require('../models').TipoComentario
const throwException = require('../utils/helpers').throwException

async function createTipoComentario (data) {
  console.log(data)
  await TipoComentario.create(data)
}

async function getTipoComentario (id) {
  const tipoComentario = await TipoComentario.findOne({
    where: { id }
  })
  if (!tipoComentario) {
    throwException('E-TipoComentario-01')
  }
  return tipoComentario
}

async function getAllTipoComentario () {
  return TipoComentario.findAll({
    order: [['id', 'asc']]

  })
}

async function updateTipoComentario (id, data) {
  const tipoComentario = await getTipoComentario(id)
  return tipoComentario.update(data)
}

async function deleteTipoComentario (id) {
  await getTipoComentario(id)
  return TipoComentario.destroy({
    where: { id }
  })
}

module.exports = { createTipoComentario,
  getTipoComentario,
  getAllTipoComentario,
  updateTipoComentario,
  deleteTipoComentario }
