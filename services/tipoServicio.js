'use strict'

const TipoServicio = require('../models').TipoServicio
const throwException = require('../utils/helpers').throwException

async function createTipoServicio (data) {
  console.log(data)
  await TipoServicio.create(data)
}

async function getTipoServicio (id) {
  const tipoServicio = await TipoServicio.findOne({
    where: { id }
  })
  if (!tipoServicio) {
    throwException('E00401')
  }
  return tipoServicio
}

async function getAllTipoServicio () {
  return TipoServicio.findAll({
    order: [['id', 'asc']]

  })
}

async function updateTipoServicio (id, data) {
  const tipoServicio = await getTipoServicio(id)
  return tipoServicio.update(data)
}

async function deleteTipoServicio (id) {
  await getTipoServicio(id)
  return TipoServicio.destroy({
    where: { id }
  })
}

module.exports = { createTipoServicio,
  getTipoServicio,
  getAllTipoServicio,
  updateTipoServicio,
  deleteTipoServicio }
