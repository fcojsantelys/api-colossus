'use strict'

const ModeloEquipo = require('../models').ModeloEquipo
const TipoEquipo = require('../models').TipoEquipo
const Marca = require('../models').Marca
const sTipoEquipo = require('../services/tipoEquipo')
const sMarca = require('../services/marca')
const throwException = require('../utils/helpers').throwException

async function createModeloEquipo (data) {
  // console.log(data)
  if (!data.idTipoEquipo) {
    throwException('E01102')
  }
  await sTipoEquipo.getTipoEquipo(data.idTipoEquipo)

  if (!data.idMarca) {
    throwException('E01103')
  }
  await sMarca.getMarca(data.idMarca)

  await ModeloEquipo.create(data)
}

// Busqueda por id
async function getModeloEquipo (id) {
  const modeloEquipo = await ModeloEquipo.findOne({
    where: { id },
    include: [TipoEquipo, Marca],
    order: [['id', 'asc']]
  })
  if (!modeloEquipo) {
    throwException('E01101')
  }
  return modeloEquipo
}

async function getAllModeloEquipo (data) {
  console.log(data.idTipoEquipo)
  console.log(data.idMarca)

  if (data.idTipoEquipo && data.idMarca) {
    // verify that they exist
    await sTipoEquipo.getTipoEquipo(data.idTipoEquipo)
    await sMarca.getMarca(data.idMarca)
    // add them as query params
    return ModeloEquipo.findAll({
      where: { idTipoEquipo: data.idTipoEquipo, idMarca: data.idMarca },
      include: [TipoEquipo, Marca],
      order: [['id', 'asc']]
    })
  }

  return ModeloEquipo.findAll({
    include: [TipoEquipo, Marca],
    order: [['id', 'asc']]
  })
}

async function updateModeloEquipo (data, id) {
  if (data.idTipoEquipo) {
    await sTipoEquipo.getTipoEquipo(data.idTipoEquipo)
  }
  if (data.idMarca) {
    await sMarca.getMarca(data.idMarca)
  }
  const modeloEquipo = await getModeloEquipo(id)
  if (!modeloEquipo) {
    throwException('E01101')
  }
  return modeloEquipo.update(data)
}

async function deleteModeloEquipo (id) {
  await getModeloEquipo(id)
  return ModeloEquipo.destroy({
    where: { id }
  })
}

module.exports = {
  createModeloEquipo,
  getModeloEquipo,
  getAllModeloEquipo,
  updateModeloEquipo,
  deleteModeloEquipo
}
