'use strict'

module.exports = {
  createNotificacion,
  getAllNotificacion,
  getNotificacionCount,
  markAsRead,
  getNotificacion,
  createBulkPromocionNotification,
  createRespondedComentarioNotification,
  createIncidenceNotification
}
const Op = require('../models').Sequelize.Op
const Rol = require('../models').Rol
const CaracteristicaCliente = require('../models').CaracteristicaCliente

const Notificacion = require('../models').Notificacion
const PlantillaNotificacion = require('../models').PlantillaNotificacion
const Usuario = require('../models').Usuario
const SolicitudServicio = require('../models').SolicitudServicio
const throwException = require('../utils/helpers').throwException
const sequelize = require('../models').sequelize
const sPlantillaNotificacion = require('./plantillaNotificacion')
const sEmpresa = require('./empresa')
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const moment = require('moment')

const sendMobileNotificationToSpecificUsers = require('../utils/notificationHandler').sendMobileNotificationToSpecificUsers
const sendNotificationEmail = require('../utils/mailer').sendNotificationEmail

function renderTemplate (template, data) {
  return template.replace(/%(.*?)%/g, (match, key) => data[key] || match)
  // trying to find a key that is in data, and replace it with the value. Otherwise do not change
}

async function createNotificacion (nombrePlantilla, usuario, idEntidad = undefined, solicitud = undefined) {
  /* Data se trae: nombrePlantilla, idEntidad (a veces), idUsuario(req), idSolicitud (a veces)
  */

  await sequelize.transaction(async t => {
    const plantilla = await sPlantillaNotificacion.getPlantillaNotificacionByName(nombrePlantilla)
    const empresa = await sEmpresa.getEmpresa()
    console.log(usuario.nombre)
    console.log('nombre de usuario ^')

    let replacingData = { usuario: usuario.nombreCompleto, correoUsuario: usuario.correo }
    let idSolicitud

    if (solicitud) {
      replacingData.equipo = `${solicitud.modeloEquipo.marca.nombre} ${solicitud.modeloEquipo.nombre}`
      replacingData.tecnico = solicitud.ordenServicio.usuario.nombreCompleto
      replacingData.cliente = solicitud.usuario.nombreCompleto
      replacingData.servicio = solicitud.catalogoServicio.descripcion
      replacingData.presupuestoBase = solicitud.presupuesto
      idSolicitud = solicitud.id
      console.log(solicitud.revision)
      if (solicitud.revision) {
        console.log(solicitud.revision.estatusEquipo)
        switch (solicitud.revision.estatusEquipo) {
          case 'A':
            replacingData.resultadoRevision = 'aprobada'
            replacingData.descResultadoRevision = 'Debes esperar a que se genere un presupuesto para aprovarlo o rechazarlo.'
            break
          case 'R':
            replacingData.resultadoRevision = 'no aprobada'
            replacingData.descResultadoRevision = 'Tu equipo se determinó no reparable, por lo que no se puede proceder, y debes retirarlo.'
            break
          default:
        }
        replacingData.diagnostico = solicitud.revision.diagnostico
        const actividades = solicitud.ordenServicio.actividades.map(o => o.get('nombre'))
        replacingData.actividades = actividades.join(', ')
      }
      if (solicitud.promocion) {
        replacingData.promocion = solicitud.promocion.nombre
        replacingData.presupuestoTotal = solicitud.presupuesto * (1 - solicitud.promocion.descuento.porcentaje / 100)
        replacingData.descuento = `descuento: ${solicitud.promocion.descuento.porcentaje}%`
      } else {
        replacingData.presupuestoTotal = solicitud.presupuesto
        replacingData.descuento = `sin descuento`
      }
      if (solicitud.motivoRechazo) {
        replacingData.motivoRechazo = solicitud.motivoRechazo.descripcion
        replacingData.motivo = solicitud.motivoRechazo.descripcion
      } else {
        replacingData.motivo = solicitud.ordenServicio.estatus === 'Esperando entrega' ? 'Reparación completada' : 'desconocido'
      }
      if (solicitud.ordenServicio.garantia) {
        console.log(solicitud.ordenServicio.garantia.condiciones)
        const condicionesGarantia = solicitud.ordenServicio.garantia.condiciones.map(o => o.get('descripcion'))
        replacingData.condicionesGarantia = condicionesGarantia.join('; ')
        replacingData.expiracionGarantia = solicitud.ordenServicio.garantia.fechaExpiracion
      }
    } else {
      idSolicitud = undefined
    }

    const desc = renderTemplate(plantilla.descripcion, replacingData)
    const title = renderTemplate(plantilla.titulo, replacingData)
    const notificacionObj = {
      titulo: title,
      descripcion: desc,
      entidad: plantilla.entidad,
      idEntidad,
      idSolicitudServicio: idSolicitud,
      idUsuario: usuario.id,
      urlImagen: plantilla.urlImagen
    }
    await Notificacion.create(notificacionObj, { transaction: t })
    console.log(notificacionObj)

    if (usuario.suscritoPushMobile) { // if enabled push notifications
      if (usuario.mobilePlayerId) { // and has a mobile device to send it to
        await sendMobileNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
      }
    }

    if (usuario.suscritoPushDesktop) { // if enabled push notifications
      if (usuario.desktopPlayerId) { // and has a desktop device to send it to
        // await sendDesktopNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
        // TODO: IMPLEMENT DESKTOP NOTIFICATIONS
      }
    }

    if (usuario.suscritoEmail) { // if subscribed to email alerts
      const baseUrl = 'http://' + mainUrl + ':' + port
      const unsuscribeLink = new URL(`usuario/${usuario.id}/unsubscribeEmail`, baseUrl).href // this URL constructor transforms
      let context = {
        title,
        message: desc,
        empresa: empresa.nombre,
        rif: empresa.rif,
        telefono: empresa.telefono,
        direccion: empresa.direccion,
        instagram: empresa.instagram,
        facebook: empresa.facebook,
        twitter: empresa.twitter,
        fullName: usuario.nombreCompleto,
        unsuscribeLink,
        logoEmpresa: empresa.urlLogo,
        date: moment().format('DD-MM-YYYY')
      }
      const emailObj = {
        nombrePlantilla: 'notificacion',
        recipient: usuario.correo,
        title,
        context
      }
      await sendNotificationEmail(emailObj)
    }
  })
}

async function createBulkPromocionNotification (nombrePlantilla, charList, promocion, alternativeText = undefined) {
  let mobilePlayerIds = await Usuario.findAll({
    where: {
      mobilePlayerId: { [Op.not]: null },
      suscritoPushMobile: true
    },
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }, {
      model: CaracteristicaCliente,
      where: {
        id: { [Op.in]: charList }
      }
    }]
  }).map(us => us.get('mobilePlayerId'))
  console.log(`${mobilePlayerIds.length} usuarios mobiles`)

  mobilePlayerIds = [...new Set(mobilePlayerIds)] // this line is to remove duplicates
  mobilePlayerIds = mobilePlayerIds.filter(m => m !== 'undefined')

  const emails = await Usuario.findAll({
    attributes: ['correo'],
    where: {
      suscritoPushMobile: true
    },
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }, {
      model: CaracteristicaCliente,
      where: {
        id: { [Op.in]: charList }
      }
    }]
  }).map(us => us.get('correo'))
  console.log(`${emails.length} correos`)

  const relevantUsers = await Usuario.findAll({
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }, {
      model: CaracteristicaCliente,
      where: {
        id: { [Op.in]: charList }
      }
    }]
  }).map(us => us.get('id'))

  const cc = await CaracteristicaCliente.findAll({
    where: { id: { [Op.in]: charList } }
  }).map(cc => cc.get('nombre'))

  await sequelize.transaction(async t => {
    const plantilla = await sPlantillaNotificacion.getPlantillaNotificacionByName(nombrePlantilla)
    const empresa = await sEmpresa.getEmpresa()
    // preparing the data
    let replacingData = {
      servicio: promocion.catalogoServicio.descripcion,
      nombrePromocion: promocion.nombre,
      descPromocion: promocion.descripcion,
      inicioPromocion: promocion.fechaInicio,
      finPromocion: promocion.fechaExpiracion,
      descuento: `descuento: ${promocion.descuento.porcentaje}%`,
      nombreDescuento: promocion.descuento.nombre,
      caracteristicas: cc.join(', ')
    }

    let desc
    // Data replacement
    if (alternativeText) {
      desc = renderTemplate(alternativeText, replacingData)
    } else {
      desc = renderTemplate(plantilla.descripcion, replacingData)
    }
    const title = renderTemplate(plantilla.titulo, replacingData)

    const notificacionObj = {
      titulo: title,
      descripcion: desc,
      entidad: plantilla.entidad,
      idEntidad: promocion.id,
      urlImagen: plantilla.urlImagen
    }

    const bulkNotificacionArray = relevantUsers.map(obj => {
      let objeto = notificacionObj
      objeto.idUsuario = obj
      return objeto
    })
    console.log(bulkNotificacionArray)
    await Notificacion.bulkCreate(bulkNotificacionArray, { transaction: t })
    await sendMobileNotificationToSpecificUsers(notificacionObj, mobilePlayerIds)
    // MOBILE PUSH ABOVE
    // EMAIL BELOW
    let context = {
      title,
      message: desc,
      empresa: empresa.nombre,
      rif: empresa.rif,
      telefono: empresa.telefono,
      direccion: empresa.direccion,
      instagram: empresa.instagram,
      facebook: empresa.facebook,
      twitter: empresa.twitter,
      logoEmpresa: empresa.urlLogo,
      date: moment().format('DD-MM-YYYY')
    }
    const emailObj = {
      nombrePlantilla: 'notificacion',
      recipient: emails,
      title,
      context
    }
    await sendNotificationEmail(emailObj)
  })
}

async function createIncidenceNotification (agendaNueva, agendaVieja, usuario, solicitud, idEntidad = undefined) {
  await sequelize.transaction(async t => {
    const plantilla = await sPlantillaNotificacion.getPlantillaNotificacionByName('reagendado')
    const empresa = await sEmpresa.getEmpresa()
    console.log(usuario.nombre)
    console.log('nombre de usuario ^')

    let replacingData = { usuario: usuario.nombreCompleto, correoUsuario: usuario.correo }
    let idSolicitud

    if (solicitud) {
      replacingData.equipo = `${solicitud.modeloEquipo.marca.nombre} ${solicitud.modeloEquipo.nombre}`
      replacingData.tecnico = solicitud.ordenServicio.usuario.nombreCompleto
      replacingData.cliente = solicitud.usuario.nombreCompleto
      replacingData.servicio = solicitud.catalogoServicio.descripcion
      idSolicitud = solicitud.id
      console.log(solicitud.revision)
      if (solicitud.promocion) {
        replacingData.promocion = solicitud.promocion.nombre
        replacingData.presupuestoTotal = solicitud.presupuesto - solicitud.presupuesto * solicitud.promocion.descuento.porcentaje
        replacingData.descuento = `descuento: ${solicitud.promocion.descuento.porcentaje}%`
      } else {
        replacingData.presupuestoTotal = solicitud.presupuesto
        replacingData.descuento = `sin descuento`
      }
      if (solicitud.motivoRechazo) {
        replacingData.motivoRechazo = solicitud.motivoRechazo.descripcion
        replacingData.motivo = solicitud.motivoRechazo.descripcion
      } else {
        replacingData.motivo = solicitud.ordenServicio.estatus === 'Esperando entrega' ? 'Reparación completada' : 'desconocido'
      }
    } else {
      idSolicitud = undefined
    }
    if (agendaVieja) {
      replacingData.fechaVieja = agendaVieja.fechaActividad
      replacingData.horaVieja = agendaVieja.bloqueHorario.descripcion
      replacingData.incidencia = agendaVieja.incidencia.descripcion
      replacingData.tipoIncidencia = agendaVieja.incidencia.tipoIncidencia.nombre
    }
    if (agendaNueva) {
      replacingData.fechaNueva = agendaNueva.fechaActividad
      replacingData.horaNueva = agendaNueva.bloqueHorario.descripcion
    }
    replacingData.actividad = agendaNueva.actividad.nombre

    const desc = renderTemplate(plantilla.descripcion, replacingData)
    const title = renderTemplate(plantilla.titulo, replacingData)
    const notificacionObj = {
      titulo: title,
      descripcion: desc,
      entidad: plantilla.entidad,
      idEntidad,
      idSolicitudServicio: idSolicitud,
      idUsuario: usuario.id,
      urlImagen: plantilla.urlImagen
    }
    await Notificacion.create(notificacionObj, { transaction: t })
    console.log(notificacionObj)

    if (usuario.suscritoPushMobile) { // if enabled push notifications
      if (usuario.mobilePlayerId) { // and has a mobile device to send it to
        await sendMobileNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
      }
    }

    if (usuario.suscritoPushDesktop) { // if enabled push notifications
      if (usuario.desktopPlayerId) { // and has a desktop device to send it to
        // await sendDesktopNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
        // TODO: IMPLEMENT DESKTOP NOTIFICATIONS
      }
    }

    if (usuario.suscritoEmail) { // if subscribed to email alerts
      const baseUrl = 'http://' + mainUrl + ':' + port
      const unsuscribeLink = new URL(`usuario/${usuario.id}/unsubscribeEmail`, baseUrl).href // this URL constructor transforms
      let context = {
        title,
        message: desc,
        empresa: empresa.nombre,
        rif: empresa.rif,
        telefono: empresa.telefono,
        direccion: empresa.direccion,
        instagram: empresa.instagram,
        facebook: empresa.facebook,
        twitter: empresa.twitter,
        logoEmpresa: empresa.urlLogo,
        date: moment().format('DD-MM-YYYY'),
        fullName: usuario.nombreCompleto,
        unsuscribeLink
      }
      const emailObj = {
        nombrePlantilla: 'notificacion',
        recipient: usuario.correo,
        title,
        context
      }
      await sendNotificationEmail(emailObj)
    }
  })
}

async function createRespondedComentarioNotification (comentario, correo, usuario = undefined) {
  /* Data se trae: nombrePlantilla, idEntidad (a veces), idUsuario(req), idSolicitud (a veces)
  */
  console.log(comentario)
  await sequelize.transaction(async t => {
    const plantilla = await sPlantillaNotificacion.getPlantillaNotificacionByName('comentario-respondido')
    const empresa = await sEmpresa.getEmpresa()
    console.log(correo)
    console.log('correo de usuario ^')
    let replacingData
    replacingData = {
      fechaCreacionComentario: comentario.dayCreated,
      tipoComentario: comentario.tipoComentario.nombre,
      comentario: comentario.descripcion,
      respuestaComentario: comentario.respuesta,
      correo: correo
    }

    replacingData.usuario = usuario ? usuario.nombreCompleto : 'Usuario Anónimo'

    const desc = renderTemplate(plantilla.descripcion, replacingData)
    const title = renderTemplate(plantilla.titulo, replacingData)
    let notificacionObj = {
      titulo: title,
      descripcion: desc,
      entidad: plantilla.entidad,
      idEntidad: comentario.id,
      urlImagen: plantilla.urlImagen
    }
    if (usuario) {
      notificacionObj.idUsuario = usuario.id
    }
    await Notificacion.create(notificacionObj, { transaction: t })
    console.log(notificacionObj)

    let context = {
      title,
      message: desc,
      empresa: empresa.nombre,
      rif: empresa.rif,
      telefono: empresa.telefono,
      direccion: empresa.direccion,
      instagram: empresa.instagram,
      facebook: empresa.facebook,
      twitter: empresa.twitter,
      logoEmpresa: empresa.urlLogo,
      date: moment().format('DD-MM-YYYY')
    }
    context.fullName = usuario ? usuario.nombreCompleto : undefined
    const emailObj = {
      nombrePlantilla: 'notificacion',
      recipient: correo,
      title,
      context
    }

    if (usuario) {
      if (usuario.suscritoPushMobile) { // if enabled push notifications
        if (usuario.mobilePlayerId) { // and has a mobile device to send it to
          if (comentario.usuario) { // and the comment was signed by userToken
            await sendMobileNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
          }
        }
      }
      if (usuario.suscritoPushDesktop) { // if enabled push notifications
        if (usuario.desktopPlayerId) { // and has a desktop device to send it to
          // await sendDesktopNotificationToSpecificUsers(notificacionObj, [usuario.mobilePlayerId])
          // TODO: IMPLEMENT DESKTOP NOTIFICATIONS
        }
      }
      if (usuario.suscritoEmail) { // if subscribed to email alerts
        await sendNotificationEmail(emailObj)
      }
    } else {
      await sendNotificationEmail(emailObj)
    }
  })
}

async function getNotificacion (id) {
  const notificacion = await Notificacion.findOne({
    where: { id },
    include: [PlantillaNotificacion, Usuario, SolicitudServicio]
  })
  if (!notificacion) {
    throwException('E03101')
  }
  return notificacion
}

async function getAllNotificacion (currentUser) {
  const notificacion = await Notificacion.findAll({
    include: [PlantillaNotificacion, SolicitudServicio, {
      model: Usuario,
      where: { id: currentUser.id }
    }],
    order: [['createdAt', 'desc']]
  })
  if (!notificacion) {
    throwException('E03102')
  }
  return notificacion
}

async function markAsRead (currentUser) {
  return Notificacion.update({ leida: true }, { where: { leida: false, idUsuario: currentUser.id } })
}

async function getNotificacionCount (currentUser) {
  return Notificacion.count({ where: { leida: false, idUsuario: currentUser.id } })
}
