'use strict'

const Bitacora = require('../models').Bitacora
const SolicitudServicio = require('../models').SolicitudServicio
const sequelize = require('../models').sequelize
const throwException = require('../utils/helpers').throwException

async function createBitacora (data, transaction) {
  data.fechaMovimiento = sequelize.fn('NOW')
  console.log(data)
  await Bitacora.create(data, { transaction })
}

async function getBitacora (idSolicitud) {
  const bitacora = await Bitacora.findAll({
    where: { idSolicitudServicio: idSolicitud },
    include: [SolicitudServicio],
    order: [['fechaMovimiento', 'asc']]
  })
  if (!bitacora) {
    throwException('E-bitacora-01')
  }
  return bitacora
}

async function getAllBitacora () {
  return Bitacora.findAll({
    include: [ SolicitudServicio ],
    order: [['idSolicitudServicio', 'asc'], ['fechaMovimiento', 'asc']]
  })
}

module.exports = {
  createBitacora,
  getBitacora,
  getAllBitacora }
