'use strict'
// entidad 17
const Rol = require('../models').Rol
const Permiso = require('../models').Permiso
const throwException = require('../utils/helpers').throwException
const Op = require('../models').Sequelize.Op
const sequelize = require('../models').sequelize

async function addRol (data) {
  await sequelize.transaction(async t => {
    const rol = await Rol.create(data, { t })
    if (data.permisos) {
      console.log(data.permisos)
      await rol.setPermisos(data.permisos, { t })
    }
  })
}

async function getAllRol (query) {
  let conditions = {
    [Op.not]: {
      tipoRol: ['cliente']
    }
  }
  if (query.includeJefe !== 'true') {
    // if it has not been sent, or if it is anything different than true
    // that means you do not want to show 'Jefe'
    conditions.esJefe = false
  } // if you have includeJefe === 'true', then esJefe = undefined, so it brings both jefes and not jefes

  const findAllRol = await Rol.findAll({
    where: conditions,
    include: [Permiso],
    order: [['id', 'asc']]
  })
  return findAllRol
}

async function getRol (id) {
  const findRol = await Rol.findOne({
    where: {
      id,
      [Op.not]: {
        tipoRol: ['cliente'] }
    },
    include: [Permiso]

  })
  if (!findRol) {
    throwException('E-rol-01')
  }
  return findRol
}

async function notJefeOrClient (id) {
  const findRol = await Rol.findOne({
    where: {
      id,
      [Op.not]: {
        tipoRol: ['cliente']
      },
      esJefe: false
    }
  })
  if (!findRol) {
    throwException('E-rol-02') // no se pueden borrar o modificar estos roles
  }
}

async function updateRol (id, data) {
  await notJefeOrClient(id)
  console.log(data)
  await sequelize.transaction(async t => {
    const rol = await getRol(id, { t })
    if (data.permisos) {
      console.log(data.permisos)
      await rol.setPermisos(data.permisos, { t })
    }
    return rol.update(data, { t })
  })
}

async function deleteRol (id) {
  await notJefeOrClient(id)
  await Rol.destroy({
    where: { id }
  })
}

async function addPermisos (id, data) {
  let rol = await getRol(id)
  if (data.permisos) {
    console.log(data.permisos)
    return rol.addPermisos(data.permisos)
  }
}

async function removePermisos (id, permiso) {
  let rol = await getRol(id)
  return rol.removePermisos(permiso)
}

async function getPermisos (id) {
  await getRol(id)

  return Permiso.findAll({
    include: [{
      model: Rol,
      where: { id: id }
    }],
    order: [['id', 'asc']]
  })
}

async function getUnaddedPermisos (id) {
  let permis = await Permiso.findAll({
    include: [{
      model: Rol,
      where: { id: id }
    }]
  }).map(per => per.get('id'))
  console.log(permis)

  if (permis.length === 0) {
    return Permiso.findAll({
      include: [Rol],
      order: [['id', 'asc']]
    })
  }

  return Permiso.findAll({
    where: {
      id: {
        [Op.notIn]: permis
      }
    },
    include: [Rol],
    order: [['id', 'asc']]
  })
}

module.exports = {
  addRol,
  getAllRol,
  getRol,
  updateRol,
  deleteRol,
  getPermisos,
  getUnaddedPermisos,
  addPermisos,
  removePermisos
}
