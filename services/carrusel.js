'use strict'
const Carrusel = require('../models').Carrusel
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage
// const messages = require('../utils/messages')

async function getCarrusel (id) {
  const carrusel = await Carrusel.findOne({
    where: { id },
    order: [['id', 'asc']]
  })

  if (!carrusel) {
    throwException('E02301')
  }
  return carrusel
}

async function createCarrusel (data, img) {
  console.log(data)
  console.log(img)

  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'carrusel')
    } else {
      data.urlImagen = new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href
    }
    await Carrusel.create(data, { t })
  })
}

async function getAllCarrusel () {
  return Carrusel.findAll({
    order: [['id', 'asc']]
  })
}

async function updateCarrusel (id, data, img) {
  await sequelize.transaction(async t => {
    const carrusel = await getCarrusel(id)
    console.log(img)
    console.log(data)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'carrusel')
    }

    return carrusel.update(data, { t })
  })
}

async function deleteCarrusel (id) {
  await getCarrusel(id)
  // using this in order to throw an exception if
  // it does not find the object that's been requested to be deleted
  return Carrusel.destroy({
    where: {
      id: id
    }
  })
}

module.exports = {
  getCarrusel,
  createCarrusel,
  getAllCarrusel,
  updateCarrusel,
  deleteCarrusel
}
