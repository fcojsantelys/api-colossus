'use strict'

const Valor = require('../models').Valor
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

async function createValor (data, img) {
  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'valor')
    } else {
      data.urlImagen = new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href
    }
    await Valor.create(data, { t })
  })
}

async function getValor (id) {
  const valor = await Valor.findOne({
    where: { id }
  })
  if (!valor) {
    throwException()
  }
  return valor
}

async function getAllValor () {
  const valor = await Valor.findAll({
    order: [['id', 'asc']]
  })
  if (!valor) {
    throwException()
  }
  return valor
}
async function updateValor (id, data, img) {
  await sequelize.transaction(async t => {
    const valor = await getValor(id)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'valor')
    }

    return valor.update(data, { t })
  })
}

async function deleteValor (id) {
  await getValor(id)
  return Valor.destroy({
    where: { id }
  })
}

module.exports = {
  createValor,
  getValor,
  getAllValor,
  updateValor,
  deleteValor
}
