'use strict'
const TecnicoExhibicion = require('../models').TecnicoExhibicion
const Usuario = require('../models').Usuario
const Rol = require('../models').Rol

const sUsuario = require('./usuario')

async function getAllTecnicoExhibicion () {
  return TecnicoExhibicion.findAll({
    include: [{
      model: Usuario,
      include: [Rol]
    }],
    order: [['id', 'asc']]
  })
}

async function updateTecnicoExhibicion (id, data) {
  await sUsuario.getEmpleado(data.idUsuario)
  const tecnicoExhibicion = await TecnicoExhibicion.findOne({
    where: { id }
  })
  return tecnicoExhibicion.update(data)
}

module.exports = { getAllTecnicoExhibicion,
  updateTecnicoExhibicion }
