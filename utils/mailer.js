const nodemailer = require('nodemailer')
const nodemailerConfig = require('../config/env').nodemailer
const hbs = require('nodemailer-express-handlebars')
const sender = `Colossus <${nodemailerConfig.auth.user}>`
// const throwException = require('./helpers').throwException
const transporter = nodemailer.createTransport(nodemailerConfig)
transporter.use('compile', hbs({
  viewEngine: {
    partialsDir: '../templates/email',
    layoutsDir: './templates/email',
    defaultLayout: false
  },
  viewPath: './templates/email'
}))

async function sendResetPasswordEmail (recipient, context) {
  console.log(`sending mail reset password token to ${recipient}`)
  const mail = {
    from: sender,
    to: recipient, // list of receivers
    subject: 'Reinicio de contrasena', // Subject line
    template: 'resetPassword',
    context
  }

  return transporter.sendMail(mail)
}

async function sendNewPasswordEmail (recipient, context) {
  console.log(`sending new password mail to ${recipient}`)

  const mail = {
    from: sender, // sender address
    to: recipient, // list of receivers
    subject: 'Nueva contrasena', // Subject line
    template: 'newPassword',
    context // variables to use on the template
  }

  return transporter.sendMail(mail)
}

async function sendWelcomeEmail (recipient, context) {
  console.log(`sending welcome mail to ${recipient}`)

  const mail = {
    from: sender, // sender address
    to: recipient, // list of receivers
    subject: `Bienvenido a ${context.empresa}`, // Subject line
    template: 'welcome',
    context // variables to use on the template
  }

  return transporter.sendMail(mail)
}

async function sendWelcomeEmailEmployee (recipient, context) {
  console.log(`sending welcome mail to ${recipient}`)

  const mail = {
    from: sender, // sender address
    to: recipient, // list of receivers
    subject: `Bienvenido a ${context.empresa}`, // Subject line
    template: 'welcomeEmployee',
    context // variables to use on the template
  }

  return transporter.sendMail(mail)
}

async function sendNotificationEmail (data) {
  const { nombrePlantilla, recipient, title, context } = data
  console.log(`sending ${nombrePlantilla} mail to ${recipient}`)
  const mail = {
    from: sender, // sender address
    to: recipient, // list of receivers
    subject: title, // Subject line
    template: nombrePlantilla,
    context // variables to use on the template
  }
  return transporter.sendMail(mail)
}

module.exports = {
  sendResetPasswordEmail,
  sendNewPasswordEmail,
  sendWelcomeEmail,
  sendWelcomeEmailEmployee,
  sendNotificationEmail
}
