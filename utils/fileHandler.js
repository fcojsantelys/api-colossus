'use strict'
const fs = require('fs')
const path = require('path')
const pathList = require('../config/path.json')
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port

function handleFile (tempPath, entity) {
  const pathForEntity = pathList[entity]
  const rootStaticPath = pathList['staticRoot']
  const rootAbsolutePath = path.resolve('.') // path of the parent
  const uniqueFileName = path.basename(tempPath) // gets the uniquely generated name from formidable and keeps the extension
  const relativePath = path.join(rootStaticPath, pathForEntity, uniqueFileName)
  // e.g. public\\img\\avatar\\upload_123123123123.png
  console.log('relativo: ' + relativePath)
  const newAbsolutePath = path.join(rootAbsolutePath, relativePath) // just the full path
  fs.renameSync(tempPath, newAbsolutePath) // moving the file to the public folder

  // enabling the newly moved file to be publicly accesibly
  const baseUrl = 'http://' + mainUrl + ':' + port
  const publicUrl = new URL(relativePath, baseUrl).href // this URL constructor transforms
  // the slashes so it is an actual URL instead of windows awkward filepath
  console.log('publicUrl:' + publicUrl)
  // return the publicly accesible path
  return publicUrl
}

module.exports = { handleFile }
