const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `
    ALTER TABLE "plantillaNotificacion" ALTER COLUMN descripcion TYPE character varying(2000);

    ALTER TABLE "notificacion" ALTER COLUMN descripcion TYPE character varying(2000);

    ALTER TABLE "notificacion" rename "idsolicitudServicio" to "idSolicitudServicio";

 
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('difundir-promocion', 'No te pierdas nuestra promocion: %nombrePromocion% para %servicio%', 
    'Desde el %inicioPromocion% hasta %finPromocion% podrás disfrutar de %nombrePromocion%: %descPromocion%, la cual ofrece un %nombreDescuento% para los servicios de %servicio%. Fuiste elegido porque perteneces o tienes interes por las caracteristicas siguientes: %caracteristicas%'
    ,'promocion', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    `
  )
}
