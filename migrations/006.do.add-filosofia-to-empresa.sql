
ALTER TABLE ONLY public.empresa 
    ADD COLUMN "cargada" boolean DEFAULT FALSE,
    ADD COLUMN "mision" character varying(2000),
    ADD COLUMN "vision" character varying(2000),
    ADD COLUMN "objetivoGeneral" character varying(2000),
    ADD COLUMN "urlImagenMision" character varying(2000),
    ADD COLUMN "urlImagenVision" character varying(2000),
    ADD COLUMN "urlImagenObjetivoGeneral" character varying(2000);