ALTER TABLE ONLY public.empresa
  ADD COLUMN "twitter" character varying(255),
  ADD COLUMN "instagram" character varying(255),
  ADD COLUMN "facebook" character varying(255);