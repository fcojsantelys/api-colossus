create table "reclamoServicio"(
  id serial PRIMARY KEY,
  descripcion character varying(255),
  "fechaRespuesta" date,
  estatus character varying(255),
  "estatusRevision" character varying(255),
  "diagnostico" character varying(500),
  "idOrdenServicio" integer REFERENCES "ordenServicio"(id),
  "idTipoReclamo" integer REFERENCES "tipoReclamo"(id),
  "idMotivoRechazo"  integer REFERENCES "motivoRechazo"(id),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone
);

create table public."tareaReclamo"(
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "actividadId" integer REFERENCES public.actividad(id) ON UPDATE CASCADE ON DELETE CASCADE,
    "reclamoServicioId" integer REFERENCES public."reclamoServicio"(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY("actividadId", "reclamoServicioId")
);