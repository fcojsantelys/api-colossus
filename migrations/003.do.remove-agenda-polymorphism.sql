
ALTER TABLE ONLY public.agenda 
    DROP COLUMN IF EXISTS idOrigen;

ALTER TABLE ONLY public.agenda 
    DROP COLUMN IF EXISTS origen;

ALTER TABLE ONLY public.agenda 
    ADD COLUMN "idActividad" integer;

ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT "agenda_idActividad_fkey" FOREIGN KEY ("idActividad") 
    REFERENCES public."actividad"(id) ON UPDATE CASCADE ON DELETE SET NULL;