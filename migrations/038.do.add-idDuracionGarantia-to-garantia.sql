ALTER TABLE ONLY public.garantia
  ADD COLUMN "idDuracionGarantia" integer;
  ALTER TABLE ONLY public.garantia
    ADD CONSTRAINT "garantia_idDuracionGarantia_fkey" FOREIGN KEY ("idDuracionGarantia") REFERENCES public."duracionGarantia"(id) ON UPDATE CASCADE ON DELETE SET NULL;
