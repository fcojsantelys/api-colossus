INSERT INTO "tipoCaracteristicaCliente" (nombre, "createdAt", "updatedAt")
values ('autoperfilado', NOW(), NOW());

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer menor de 15', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre menor de 15', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer entre 15 y 20', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre entre 15 y 20', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer entre 20 y 30', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre entre 20 y 30', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer entre 30 y 40', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre entre 30 y 40', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer entre 40 y 50', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre entre 40 y 50', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer entre 50 y 60', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre entre 50 y 60', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;

INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Mujer mayor de 60', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
INSERT INTO "caracteristicaCliente" (nombre, "idTipoCaracteristicaCliente", "createdAt", "updatedAt")
  select 'Hombre mayor de 60', t.id, NOW(), NOW() from "tipoCaracteristicaCliente" t where nombre='autoperfilado'  ON CONFLICT (id) DO NOTHING;
