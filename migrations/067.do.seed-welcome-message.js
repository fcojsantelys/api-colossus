const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    ` 
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('registro', 'Bienvenido(a)%', 
    'Bienvenid@ a nuestra empresa, estamos felices de que quieras utilizar nuestros servicios'
    ,'usuario', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    `
  )
}
