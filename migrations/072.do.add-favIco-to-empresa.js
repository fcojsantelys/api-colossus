const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `ALTER TABLE ONLY public.empresa
    ADD COLUMN "urlFavIco" character varying(2000)
    ;
    
    UPDATE public.empresa SET 
    "urlFavIco"='${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}';
    `
  )
}
