CREATE TABLE PUBLIC."plantillaNotificacion" (
  id SERIAL PRIMARY KEY,
  descripcion character varying(255),
  titulo character varying(255),
  nombre character varying(255),
  entidad character varying(255),
  "urlImagen" character varying(2000),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone
);

CREATE TABLE PUBLIC."notificacion" (
  id SERIAL PRIMARY KEY,
  entidad character varying(255),
  descripcion character varying(255),
  titulo character varying(255),
  "idEntidad" integer,
  "urlImagen" character varying(2000),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone,
  "idsolicitudServicio" integer references public."solicitudServicio"(id),
  "idUsuario" integer references public.usuario(id),
  "idPlantillaNotificacion" integer references public."plantillaNotificacion"(id)
);
