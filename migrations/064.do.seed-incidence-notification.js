const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    ` 
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('reagendado', 'Ocurrió una incidencia durante la reparacion de tu %equipo%', 
    'La actividad %actividad% que estaba agendada para el %fechaVieja% en el bloque horario %horaVieja%, no se pudo completar debido a: (%tipoIncidencia%) %incidencia%. La nueva fecha y hora de la actividad serán: %fechaNueva%, %horaNueva%'
    ,'agenda', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('garantia-entregada', 'Garantía del equipo %equipo% por %servicio%', 
    'Se creó una garantía por la entrega de tu %equipo% por %servicio%, válida hasta %expiracionGarantia%. Las condiciones son: %condicionesGarantia%.'
    ,'garantia', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    `
  )
}
