CREATE TABLE public.comentario (
    id integer NOT NULL,
    descripcion character varying(255),
    respuesta character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idUsuario" integer,
    "idTipoComentario" integer
);
CREATE SEQUENCE public.comentario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.comentario_id_seq OWNED BY public.comentario.id;
ALTER TABLE ONLY public.comentario ALTER COLUMN id SET DEFAULT nextval('public.comentario_id_seq'::regclass);
ALTER TABLE ONLY public.comentario
    ADD CONSTRAINT comentario_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.comentario
    ADD CONSTRAINT "comentario_idUsuario_fkey" FOREIGN KEY ("idUsuario") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.comentario
    ADD CONSTRAINT "comentario_idTipoComentario_fkey" FOREIGN KEY ("idTipoComentario") REFERENCES public."tipoComentario"(id) ON UPDATE CASCADE ON DELETE SET NULL;