
module.exports.generateSql = function () {
  if (process.env.COLOSSUS_SEED !== 'true') { return ('') } // do nothing

  return (
    `
    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión', 'revision',
      'Revisión de Reparación Smartphones', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;

    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión por reclamo', 'revisionReclamo',
      'Revisión por reclamo de Reparación Smartphones', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;


    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión', 'revision',
      'Revisión de Reparación Laptops', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Laptops' ON CONFLICT (id) DO NOTHING;
    
    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión por reclamo', 'revisionReclamo',
      'Revisión por reclamo de Reparación Laptops', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Laptops' ON CONFLICT (id) DO NOTHING;


    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión', 'revision',
      'Revisión de Reparación Consolas', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Consolas' ON CONFLICT (id) DO NOTHING;

    INSERT INTO "actividad"  
      (nombre, tipo, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Revisión por reclamo', 'revisionReclamo',
      'Revisión por reclmao de Reparación Consolas', 0, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Consolas' ON CONFLICT (id) DO NOTHING;
    `
  )
}
