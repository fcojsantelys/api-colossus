CREATE TABLE public."tecnicoExhibicion"(
  id integer NOT NULL,
  descripcion character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone,
  "idUsuario" integer
);
CREATE SEQUENCE public."tecnicoExhibicion_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tecnicoExhibicion_id_seq" OWNED BY public."tecnicoExhibicion".id;
ALTER TABLE ONLY public."tecnicoExhibicion" ALTER COLUMN id SET DEFAULT nextval('public."tecnicoExhibicion_id_seq"'::regclass);
ALTER TABLE ONLY public."tecnicoExhibicion"
    ADD CONSTRAINT "tecnicoExhibicion_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."tecnicoExhibicion"
    ADD CONSTRAINT "tecnicoExhibicion_idUsuario_fkey" FOREIGN KEY ("idUsuario") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;
    
INSERT INTO "tecnicoExhibicion" (descripcion, "createdAt", "updatedAt") values('En espera de actualizacion de la exhibicion 1', NOW(), NOW());
INSERT INTO "tecnicoExhibicion" (descripcion, "createdAt", "updatedAt") values('En espera de actualizacion de la exhibicion 2', NOW(), NOW());
INSERT INTO "tecnicoExhibicion" (descripcion, "createdAt", "updatedAt") values('En espera de actualizacion de la exhibicion 3', NOW(), NOW());
INSERT INTO "tecnicoExhibicion" (descripcion, "createdAt", "updatedAt") values('En espera de actualizacion de la exhibicion 4', NOW(), NOW());