const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('solicitud-aprobada', 'Solicitud Aprobada', 
    'La solicitud de reparación ha sido aprobada. Recuerda que debes llevar tu equipo antes de la fecha y hora de revision.'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());
    `
  )
}
