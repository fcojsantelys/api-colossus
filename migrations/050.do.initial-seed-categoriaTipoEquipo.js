
module.exports.generateSql = function () {
  if (process.env.COLOSSUS_SEED !== 'true') { return ('') } // do nothing

  return (
    `
    INSERT INTO "categoriaTipoEquipo"  
    (nombre, "createdAt", "updatedAt")
    values ('Telefonía', NOW(), NOW()) ON CONFLICT (id) DO NOTHING;

    INSERT INTO "categoriaTipoEquipo"  
    (nombre, "createdAt", "updatedAt")
    values ('Computación', NOW(), NOW()) ON CONFLICT (id) DO NOTHING;

    INSERT INTO "categoriaTipoEquipo"  
    (nombre, "createdAt", "updatedAt")
    values ('Videojuegos', NOW(), NOW()) ON CONFLICT (id) DO NOTHING;
    `
  )
}
