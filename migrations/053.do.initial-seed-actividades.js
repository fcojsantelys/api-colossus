
module.exports.generateSql = function () {
  if (process.env.COLOSSUS_SEED !== 'true') { return ('') } // do nothing

  return (
    `
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Reparación Pin de Carga',
      'Este servicio se realiza en el caso de que el teléfono presente inconvenientes a la hora de cargar la batería, el cambio de la pieza se hará en un periodo máximo de dos horas.', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Cambio de Mica',
      'Este Servicio se realiza en el caso de que la mica se encuentre partida o mal estado, comúnmente producidos por las caídas de los teléfonos afín de mejorar la visibilidad de la pantalla, el cambio de esta pieza se hará en un periodo de 2 días hábiles', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución de los botones de volumen',
      'Este servicio se realiza en caso de que los botones de volumen se encuentren en mal estado esto debido al uso excesivo y deterioro a través del tiempo, este servicio se realiza en un periodo menos a las 5 horas', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución de Parlantes',
      'Este servicio se realiza en caso de que el dispositivo no reproduce ningún sonido cuando recibes una llamada o llega alguna notificación, el cambio de esta pieza se realizara en un periodo de 2 días hábiles.', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Smartphones' ON CONFLICT (id) DO NOTHING;

    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución de la tarjeta madre','Este servicio se realizara en caso de que la tarjeta madre se encuentre defectuosa ocasionando reinicio constante del equipo.', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Laptops' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución de la Memoria RAM','Este servicio de realiza al dispositivo en caso de manifestarse una ralentización del funcionamiento del ordenador', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Laptops' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución del disco duro','Este servicio de realiza al dispositivo en caso de presentarse desaparición de la información (carpetas y archivos), aparición de “pantallazos azules”.', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Laptops' ON CONFLICT (id) DO NOTHING;

    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Reparacion de placas base','Reparacion de placas base ', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Consolas' ON CONFLICT (id) DO NOTHING;
    INSERT INTO "actividad"  
      (nombre, descripcion, costo, "idCatalogoServicio", "createdAt", "updatedAt")
      select 'Sustitución de chip grafico','Sustitución de chip grafico ', 60000, cs.id, NOW(), NOW()
      from "catalogoServicio" cs where descripcion='Reparación Consolas' ON CONFLICT (id) DO NOTHING;

    `
  )
}
