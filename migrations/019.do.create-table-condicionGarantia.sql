create table public."condicionGarantia"(
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "condicionId" integer REFERENCES public.condicion(id) ON UPDATE CASCADE ON DELETE CASCADE,
    "garantiaId" integer REFERENCES public.garantia(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY("condicionId", "garantiaId")
);