CREATE TABLE PUBLIC."mensaje" (
  id SERIAL PRIMARY KEY,
  nombre character varying(255),
  descripcion character varying(2000),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone
);

INSERT INTO mensaje (nombre, descripcion, "createdAt", "updatedAt") values ('primera-solicitud','Antes de realizar tu primera solicitud, por favor indicanos tus preferencias', NOW(), NOW());
INSERT INTO mensaje (nombre, descripcion, "createdAt", "updatedAt") values ('post-calificacion','Gracias por tu calificacion, tu opinión es muy importante para nosotros', NOW(), NOW());