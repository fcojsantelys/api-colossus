const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `ALTER TABLE ONLY public.promocion
    ADD COLUMN "urlImagen" character varying(2000)
    ;
    
    UPDATE public.promocion SET 
    "urlImagen"='${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}';
    `
  )
}
