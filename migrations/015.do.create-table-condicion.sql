CREATE TABLE public.condicion (
    id integer NOT NULL,
    descripcion character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public.condicion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.condicion_id_seq OWNED BY public.condicion.id;
ALTER TABLE ONLY public.condicion ALTER COLUMN id SET DEFAULT nextval('public.condicion_id_seq'::regclass);
ALTER TABLE ONLY public.condicion
    ADD CONSTRAINT condicion_pkey PRIMARY KEY (id);