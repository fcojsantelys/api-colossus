CREATE TABLE public."horarioTrabajo" (
    id SERIAL PRIMARY KEY,
    "horaInicio" time without time zone,
    "horaInicioFeriado" time without time zone,
    "horaFinal" time without time zone,
    "horaFinalFeriado" time without time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);

INSERT INTO "horarioTrabajo" ("createdAt","updatedAt") VALUES (NOW(), NOW());