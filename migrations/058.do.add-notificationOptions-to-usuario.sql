ALTER TABLE "usuario" 
  ADD COLUMN "suscritoEmail" boolean default true,
  ADD COLUMN "suscritoPushMobile" boolean default true,
  ADD COLUMN "suscritoPushDesktop" boolean default true;