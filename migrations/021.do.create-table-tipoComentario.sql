CREATE TABLE public."tipoComentario" (
    id integer NOT NULL,
        nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
    );
CREATE SEQUENCE public."tipoComentario_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoComentario_id_seq" OWNED BY public."tipoComentario".id;
ALTER TABLE ONLY public."tipoComentario" ALTER COLUMN id SET DEFAULT nextval('public."tipoComentario_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoComentario"
    ADD CONSTRAINT "tipoComentario_pkey" PRIMARY KEY (id);
