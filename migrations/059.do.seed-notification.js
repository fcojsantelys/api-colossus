const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('solicitud-rechazada', 'Solicitud de reparación de %equipo% Rechazada', 
    'La solicitud de reparación de tu %equipo% ha sido rechazada. El motivo que se indicó es: %motivoRechazo%'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('revision-realizada', 'Revisión %resultadoRevision%', 
    'La revisión de tu %equipo% se llevó acabo y resultó %resultadoRevision%. %descResultadoRevision%'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('presupuesto-generado', 'Presupuesto Generado', 
    'Se generó el presupuesto para la reparación de tu %equipo% para un total de: %presupuestoTotal% (%descuento% aplicado). Ingresa para aceptar o rechazar.'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('reclamo-aprobado', 'Reclamo de %equipo% Aprobado', 
    'La solicitud de reparación por reclamo de tu %equipo% ha sido aprobada. Recuerda que debes llevar tu equipo antes de la fecha y hora de revision.'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('retirar-equipo', '%equipo% listo', 
    'Tu equipo %equipo% está listo para que lo retires. Motivo del retiro: %motivo%'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('reclamo-rechazado', 'Reclamo de %equipo% rechazado', 
    'La solicitud de reparación por reclamo de tu %equipo% ha sido rechazada. El motivo que se indicó es: %motivoRechazo%'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('revision-reclamo-realizada', 'Revisión por reclamo %resultadoRevision%', 
    'La revisión por reclamo de tu %equipo% se llevó acabo y resultó %resultadoRevision%. %descResultadoRevision%'
    ,'solicitudServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('servicio-asignado', 'Servicio %servicio% asignado', 
    'Se te asignó un(a) %servicio% del equipo %equipo% del cliente %cliente%'
    ,'ordenServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('servicio-reclamo-asignado', 'Servicio %servicio% (por reclamo) asignado', 
    'Se te asignó un(a) %servicio% por reclamo del equipo %equipo% del cliente %cliente%'
    ,'ordenServicio', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    `
  )
}
