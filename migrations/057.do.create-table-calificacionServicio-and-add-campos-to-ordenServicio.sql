ALTER TABLE ONLY public."ordenServicio"
  ADD COLUMN "puntaje" integer,
  ADD COLUMN "comentario" character varying(255); 

create table public."calificacionServicio"(
    respuesta integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idOrdenServicio" integer REFERENCES public."ordenServicio"(id) ON UPDATE CASCADE ON DELETE CASCADE,
    "idPreguntaCalificacion" integer REFERENCES public."preguntaCalificacion"(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY("idOrdenServicio", "idPreguntaCalificacion")
);