const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    ` 
    INSERT INTO "plantillaNotificacion"  
    (nombre, titulo, descripcion, entidad, "urlImagen", "createdAt", "updatedAt") 
    values ('comentario-respondido', 'Respondimos tu %tipoComentario%', 
    'El %fechaCreacionComentario% nos enviaste un(a) %tipoComentario%: %comentario%. Y nuestra respuesta es: %respuestaComentario%'
    ,'comentario', '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', NOW(), NOW());

    `
  )
}
