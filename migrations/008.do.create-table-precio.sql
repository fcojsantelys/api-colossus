CREATE TABLE public.precio (
    id SERIAL PRIMARY KEY,
    "precioMinimoRevision" float,
    "precioMaximoRevision" float,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);