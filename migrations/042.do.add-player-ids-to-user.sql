ALTER TABLE ONLY public.usuario
  ADD COLUMN "mobilePlayerId" character varying(255),
  ADD COLUMN "desktopPlayerId" character varying(255);