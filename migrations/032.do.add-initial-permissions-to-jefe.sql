
INSERT INTO "permisoRol" ("createdAt", "updatedAt","permisoId", "rolId") select NOW(), NOW(), p."id", r.id
  from permiso p, rol r 
  where p."id" NOT IN 
    (select "permisoId" FROM "permisoRol" where "rolId" in 
      (select r.id from rol r where r."esJefe"=TRUE)) 
  and r."esJefe" = true;

