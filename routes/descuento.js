var router = require('express').Router()
var cDescuento = require('../controllers/descuento')

router.post('/', cDescuento.createDescuento)
router.get('/', cDescuento.getAllDescuento)
router.get('/:id', cDescuento.getDescuento)
router.put('/:id', cDescuento.updateDescuento)
router.delete('/:id', cDescuento.deleteDescuento)

module.exports = router
