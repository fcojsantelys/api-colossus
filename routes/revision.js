var router = require('express').Router()
var cRevision = require('../controllers/revision')

router.get('/:id', cRevision.getRevision)
router.get('/', cRevision.getAllRevision)
// router.post('/:id/revisar', cRevision.updateRevision)
// router.get('/revision/solicitudServicio/:id', cRevision.getRevisionSolicitud)

module.exports = router
