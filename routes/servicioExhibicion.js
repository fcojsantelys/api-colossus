var router = require('express').Router()
var cServicioExhibicion = require('../controllers/servicioExhibicion')

router.get('/', cServicioExhibicion.getAllServicioExhibicion)
router.put('/:id', cServicioExhibicion.updateServicioExhibicion)

module.exports = router
