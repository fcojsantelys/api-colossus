const router = require('express').Router()
const cGarantia = require('../controllers/garantia')

// router.post('/', cGarantia.createGarantia)
router.get('/', cGarantia.getAllGarantia)
router.get('/:id', cGarantia.getGarantia)

module.exports = router
