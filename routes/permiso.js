var router = require('express').Router()
var cPermiso = require('../controllers/permiso')

router.get('/', cPermiso.getAllPermiso)

module.exports = router
