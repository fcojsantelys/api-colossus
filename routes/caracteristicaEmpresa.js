var router = require('express').Router()
var cCaracteristicaEmpresa = require('../controllers/caracteristicaEmpresa')

router.post('/', cCaracteristicaEmpresa.createCaracteristicaEmpresa)
router.get('/:id', cCaracteristicaEmpresa.getCaracteristicaEmpresa)
router.get('/', cCaracteristicaEmpresa.getAllCaracteristicaEmpresa)
router.put('/:id', cCaracteristicaEmpresa.updateCaracteristicaEmpresa)
module.exports = router
