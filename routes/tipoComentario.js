var router = require('express').Router()
var cTipoComentario = require('../controllers/tipoComentario')

router.post('/', cTipoComentario.createTipoComentario)
router.get('/', cTipoComentario.getAllTipoComentario)
router.get('/:id', cTipoComentario.getTipoComentario)
router.put('/:id', cTipoComentario.updateTipoComentario)
router.delete('/:id', cTipoComentario.deleteTipoComentario)

module.exports = router
