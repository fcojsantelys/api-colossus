var router = require('express').Router()
var cRol = require('../controllers/rol')

router.post('/', cRol.postRol)
router.get('/:id', cRol.getRol)
router.get('/', cRol.getAllRol)
router.put('/:id', cRol.updateRol)
router.delete('/:id', cRol.destroyRol)
router.post('/:id/permisos', cRol.addPermisos)
router.delete('/:id/permisos/:permiso', cRol.removePermisos)
router.get('/:id/permisos', cRol.getPermisos)
router.get('/:id/permisosNoAgregados', cRol.getUnaddedPermisos)

module.exports = router
