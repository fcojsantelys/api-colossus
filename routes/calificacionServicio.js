const router = require('express').Router()
const cCalificacionServicio = require('../controllers/calificacionServicio')

router.post('/:id', cCalificacionServicio.createCalificacionServicio)
router.get('/', cCalificacionServicio.getAllCalificacionServicio)
router.get('/:id', cCalificacionServicio.getCalificacionServicio)

module.exports = router
