var router = require('express').Router()
var cPromocion = require('../controllers/promocion')

router.post('/', cPromocion.createPromocion)
router.get('/', cPromocion.getAllPromocion)
router.get('/vigente', cPromocion.getAllPromocionVigente)
router.get('/:id', cPromocion.getPromocion)
router.post('/:id/difundir', cPromocion.difundirPromocion)

router.get('/servicio/:id', cPromocion.getPromocionByServicio)
router.put('/:id', cPromocion.updatePromocion)
router.delete('/:id', cPromocion.deletePromocion)

module.exports = router
