var router = require('express').Router()
var cPreguntaFrecuente = require('../controllers/preguntaFrecuente')

router.post('/', cPreguntaFrecuente.createPreguntaFrecuente)
router.get('/:id', cPreguntaFrecuente.getPreguntaFrecuente)
router.get('/', cPreguntaFrecuente.getAllPreguntaFrecuente)
router.put('/:id', cPreguntaFrecuente.updatePreguntaFrecuente)
router.delete('/:id', cPreguntaFrecuente.deletePreguntaFrecuente)

module.exports = router
