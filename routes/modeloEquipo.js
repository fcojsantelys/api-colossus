var router = require('express').Router()
var cModeloEquipo = require('../controllers/modeloEquipo')

router.post('/', cModeloEquipo.createModeloEquipo)
router.get('/:id', cModeloEquipo.getModeloEquipo)
router.get('/', cModeloEquipo.getAllModeloEquipo)
router.put('/:id', cModeloEquipo.updateModeloEquipo)
router.delete('/:id', cModeloEquipo.deleteModeloEquipo)

module.exports = router
