var router = require('express').Router()
var cDescargaApp = require('../controllers/descargaApp')

router.post('/', cDescargaApp.createDescargaApp)
router.get('/:id', cDescargaApp.getDescargaApp)
router.get('/', cDescargaApp.getAllDescargaApp)
router.put('/:id', cDescargaApp.updateDescargaApp)
module.exports = router
