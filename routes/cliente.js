var router = require('express').Router()
var cUsuario = require('../controllers/usuario')

router.post('/', cUsuario.createCliente)
router.get('/:id', cUsuario.getCliente)
router.get('/', cUsuario.getAllCliente)
router.put('/:id', cUsuario.updateCliente)
router.delete('/:id', cUsuario.deleteCliente)
router.post('/:id/caracteristicasCliente', cUsuario.addCaracteristicasCliente)
router.post('/:id/setCaracteristicasCliente', cUsuario.setCaracteristicasCliente)
router.delete('/:id/caracteristicasCliente/:caracteristicaCliente', cUsuario.removeCaracteristicasCliente)
router.get('/:id/caracteristicasCliente', cUsuario.getCaracteristicasCliente)
router.get('/:id/caracteristicasClienteNoAgregadas', cUsuario.getUnaddedCaracteristicasCliente)

module.exports = router
