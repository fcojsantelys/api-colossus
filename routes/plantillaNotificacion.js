var router = require('express').Router()
var cPlantillaNotificacion = require('../controllers/plantillaNotificacion')

router.get('/:id', cPlantillaNotificacion.getPlantillaNotificacion)
router.put('/:id', cPlantillaNotificacion.updatePlantillaNotificacion)
router.get('/', cPlantillaNotificacion.getAllPlantillaNotificacion)
module.exports = router
