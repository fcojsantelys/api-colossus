var router = require('express').Router()
var cBloqueHorario = require('../controllers/bloqueHorario')

router.post('/', cBloqueHorario.createBloqueHorario)
router.get('/:id', cBloqueHorario.getBloqueHorario)
router.get('/', cBloqueHorario.getAllBloqueHorario)
router.put('/:id', cBloqueHorario.updateBloqueHorario)
router.delete('/:id', cBloqueHorario.deleteBloqueHorario)

module.exports = router
