var router = require('express').Router()
var cTecnicoExhibicion = require('../controllers/tecnicoExhibicion')

router.get('/', cTecnicoExhibicion.getAllTecnicoExhibicion)
router.put('/:id', cTecnicoExhibicion.updateTecnicoExhibicion)

module.exports = router
