var express = require('express')

var cCatalogoServicio = require('../controllers/catalogoServicio')

var router = express.Router()

router.get('/:id', cCatalogoServicio.getCatalogoServicio)
router.post('/', cCatalogoServicio.createCatalogoServicio)
router.get('/', cCatalogoServicio.getAllCatalogoServicio)
router.put('/:id', cCatalogoServicio.updateCatalogoServicio)
router.delete('/:id', cCatalogoServicio.deleteCatalogoServicio)
router.get('/:id/revision', cCatalogoServicio.getRevisionActivity)
module.exports = router
