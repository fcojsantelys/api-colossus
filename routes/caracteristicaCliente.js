var express = require('express')

var cCaracteristicaCliente = require('../controllers/caracteristicaCliente')

var router = express.Router()

router.get('/:id', cCaracteristicaCliente.getCaracteristicaCliente)
router.get('/:id/clientes', cCaracteristicaCliente.getUsuariosCaracteristicaCliente)
router.post('/', cCaracteristicaCliente.createCaracteristicaCliente)
router.get('/', cCaracteristicaCliente.getAllCaracteristicaCliente)
router.put('/:id', cCaracteristicaCliente.updateCaracteristicaCliente)
router.delete('/:id', cCaracteristicaCliente.deleteCaracteristicaCliente)

module.exports = router
