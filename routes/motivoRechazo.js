var router = require('express').Router()
var cMotivoRechazo = require('../controllers/motivoRechazo')

router.post('/', cMotivoRechazo.createMotivoRechazo)
router.get('/', cMotivoRechazo.getAllMotivoRechazo)
router.get('/:id', cMotivoRechazo.getOneMotivoRechazo)
router.get('/tiporechazo/:tipo', cMotivoRechazo.getByNameMotivoRechazo)
router.put('/:id', cMotivoRechazo.updateMotivoRechazo)
router.delete('/:id', cMotivoRechazo.deleteMotivoRechazo)
router.get('/solicitud/:id', cMotivoRechazo.getSolicitudByMotivo)

module.exports = router
