var router = require('express').Router()
var cSliderMovil = require('../controllers/sliderMovil')

router.post('/', cSliderMovil.createSliderMovil)
router.get('/:id', cSliderMovil.getSliderMovil)
router.get('/', cSliderMovil.getAllSliderMovil)
router.put('/:id', cSliderMovil.updateSliderMovil)
router.delete('/:id', cSliderMovil.deleteSliderMovil)

module.exports = router
