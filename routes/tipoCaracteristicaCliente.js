var router = require('express').Router()
var cTipoCaracteristicaCliente = require('../controllers/tipoCaracteristicaCliente')

router.post('/', cTipoCaracteristicaCliente.createTipoCaracteristicaCliente)
router.get('/:id', cTipoCaracteristicaCliente.getTipoCaracteristicaCliente)
router.get('/', cTipoCaracteristicaCliente.getAllTipoCaracteristicaCliente)
router.put('/:id', cTipoCaracteristicaCliente.updateTipoCaracteristicaCliente)
router.delete('/:id', cTipoCaracteristicaCliente.deleteTipoCaracteristicaCliente)

module.exports = router
