var router = require('express').Router()
var cDuracionGarantia = require('../controllers/duracionGarantia')

router.post('/', cDuracionGarantia.createDuracionGarantia)
router.get('/:id', cDuracionGarantia.getDuracionGarantia)
router.get('/', cDuracionGarantia.getAllDuracionGarantia)
router.put('/:id', cDuracionGarantia.updateDuracionGarantia)
router.delete('/:id', cDuracionGarantia.deleteDuracionGarantia)

module.exports = router
