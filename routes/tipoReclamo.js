var router = require('express').Router()
var cTipoReclamo = require('../controllers/tipoReclamo')

router.post('/', cTipoReclamo.createTipoReclamo)
router.get('/:id', cTipoReclamo.getTipoReclamo)
router.get('/', cTipoReclamo.getAllTipoReclamo)
router.put('/:id', cTipoReclamo.updateTipoReclamo)
router.delete('/:id', cTipoReclamo.deleteTipoReclamo)

module.exports = router
