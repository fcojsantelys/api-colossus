var router = require('express').Router()
var cHorarioTrabajo = require('../controllers/horarioTrabajo')

router.get('/', cHorarioTrabajo.getHorarioTrabajo)
router.put('/', cHorarioTrabajo.updateHorarioTrabajo)

module.exports = router
