var router = require('express').Router()
var cNotificacion = require('../controllers/notificacion')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.get('/', ejwt, checkUser, cNotificacion.getAllNotificacion)
router.get('/noLeidas', ejwt, checkUser, cNotificacion.getNotificacionCount)
router.post('/leer', ejwt, checkUser, cNotificacion.markAsRead)
router.get('/:id', cNotificacion.getNotificacion)

module.exports = router
