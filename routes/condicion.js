var router = require('express').Router()
const cCondicion = require('../controllers/condicion')

router.post('/', cCondicion.createCondicion)
router.get('/', cCondicion.getAllCondicion)
router.get('/:id', cCondicion.getCondicion)
router.put('/:id', cCondicion.updateCondicion)
router.delete('/:id', cCondicion.deleteCondicion)

module.exports = router
