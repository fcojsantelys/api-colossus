var router = require('express').Router()
var cReclamoServicio = require('../controllers/reclamoServicio')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.post('/', cReclamoServicio.createReclamoServicio)
router.get('/', ejwt, checkUser, cReclamoServicio.getAllReclamoServicio)
router.get('/:id', cReclamoServicio.getReclamoServicio)
router.post('/:id/aceptar', cReclamoServicio.acceptReclamoServicio)
router.post('/:id/rechazar', cReclamoServicio.rejectReclamoServicio)
router.post('/:id/reparable', cReclamoServicio.acceptRevisionByReclamoServicio)
router.post('/:id/terminarReparación', cReclamoServicio.completeReclamoRepair)
router.post('/:id/entregarEquipo', cReclamoServicio.deliverEquipo)
router.post('/:id/noReparable', cReclamoServicio.rejectRevisionByReclamoServicio)
router.get('/:id/actividadesPorAgendar', cReclamoServicio.getSchedulableReclamoTasks)
router.get('/:id/agendasPorCompletar', ejwt, checkUser, cReclamoServicio.getUncompletedAgendas)

module.exports = router
