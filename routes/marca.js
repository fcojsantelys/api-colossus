var express = require('express')

var cMarca = require('../controllers/marca')

var router = express.Router()

router.get('/:id', cMarca.getMarca)
router.post('/', cMarca.createMarca)
router.get('/', cMarca.getAllMarca)
router.put('/:id', cMarca.updateMarca)
router.delete('/:id', cMarca.deleteMarca)

module.exports = router
